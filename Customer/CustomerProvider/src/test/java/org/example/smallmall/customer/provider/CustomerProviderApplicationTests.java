package org.example.smallmall.customer.provider;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.smallmall.customer.provider.dao.CustomerBalanceDAO;
import org.example.smallmall.customer.provider.dao.CustomerOrderDAO;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;
import org.example.smallmall.customer.provider.entity.CustomerOrderEntity;
import org.example.smallmall.customer.provider.mapper.CustomerBalanceMapper;
import org.example.smallmall.customer.provider.mapper.CustomerOrderMapper;
import org.example.smallmall.customer.provider.service.CustomerBalanceService;
import org.example.smallmall.customer.provider.service.CustomerHistoryService;
import org.example.smallmall.customer.provider.service.CustomerOrderService;
import org.example.smallmall.customer.provider.service.impl.CustomerBalanceServiceImpl;
import org.example.smallmall.customer.provider.service.impl.CustomerHistoryServiceImpl;
import org.example.smallmall.customer.provider.service.impl.CustomerOrderServiceImpl;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.SimpleTransactionStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerProviderApplicationTests {
    @MockBean
    private CustomerOrderMapper customerOrderMapper;

    @MockBean
    private CustomerBalanceMapper customerBalanceMapper;

    @MockBean
    private CustomerHistoryService customerHistoryService;

    @MockBean
    private CustomerBalanceService customerBalanceService;

    @MockBean
    private CustomerOrderService customerOrderService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testIndex() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(status().isOk());
    }

    @Test
    public void testGetOrder() throws Exception {
        CustomerOrderServiceImpl customerOrderServiceImpl = new CustomerOrderServiceImpl();
        CustomerOrderService customerOrderServiceMock = Mockito.spy(customerOrderServiceImpl);

        Mockito.doReturn(null).when(customerOrderServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        Map<String, Object> orderMap = customerOrderServiceMock.getOrder(1, 1, 1);
        Assertions.assertNull(orderMap);

        CustomerOrderEntity customerOrder = new CustomerOrderEntity();
        List<CustomerOrderEntity> orderList = new ArrayList<>();
        Page<CustomerOrderEntity> orderPage = new Page<>();

        orderList.add(customerOrder);
        orderPage.setCurrent(1);
        orderPage.setPages(1);
        orderPage.setSize(1);
        orderPage.setTotal(1);
        orderPage.setRecords(orderList);

        Mockito.doReturn(orderPage).when(customerOrderServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        orderMap = customerOrderServiceMock.getOrder(1, 1, 1);
        Assertions.assertNotNull(orderMap);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/0")).andExpect(status().isBadRequest());

        Mockito.when(customerOrderService.getOrder(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/1")).andExpect(status().isOk());

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);
        Mockito.when(customerOrderService.getOrder(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/1")).andExpect(status().isOk());
    }

    @Test
    public void testPurchaseOrder() throws Exception {
        CustomerOrderDAO customerOrderDAO = new CustomerOrderDAO();
        ReflectionTestUtils.setField(customerOrderDAO, "customerOrderMapper", customerOrderMapper);
        CustomerOrderDAO customerOrderDAOMock = Mockito.spy(customerOrderDAO);
        CustomerOrderServiceImpl customerOrderServiceImpl = new CustomerOrderServiceImpl();
        ReflectionTestUtils.setField(customerOrderServiceImpl, "customerOrderDAO", customerOrderDAOMock);
        ReflectionTestUtils.setField(customerOrderServiceImpl, "customerBalanceService", customerBalanceService);
        ReflectionTestUtils.setField(customerOrderServiceImpl, "customerHistoryService", customerHistoryService);
        CustomerOrderService customerOrderServiceMock = Mockito.spy(customerOrderServiceImpl);
        SimpleTransactionStatus simpleTransactionStatus = new SimpleTransactionStatus();
        SimpleTransactionStatus simpleTransactionStatusMock = Mockito.spy(simpleTransactionStatus);
        MockedStatic<TransactionAspectSupport> transactionAspectSupportMockedStatic = Mockito.mockStatic(TransactionAspectSupport.class);

        Mockito.doNothing().when(simpleTransactionStatusMock).setRollbackOnly();
        transactionAspectSupportMockedStatic.when(() -> {
            TransactionAspectSupport.currentTransactionStatus();
        }).thenReturn(simpleTransactionStatusMock);

        Mockito.doReturn(null).when(customerOrderMapper).customerPurchaseOrder(Mockito.anyInt(), Mockito.any(CustomerOrderEntity.class));
        Mockito.doReturn(null).when(customerBalanceService).getCustomerBalance(Mockito.anyInt());
        Mockito.doReturn(true).when(customerHistoryService).save(Mockito.any(CustomerHistoryEntity.class));

        CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO = new CustomerPurchaseOrderRequestVO();

        customerPurchaseOrderRequestVO.setCustomerId(1);
        customerPurchaseOrderRequestVO.setMerchantId(1);
        customerPurchaseOrderRequestVO.setGoodsDescription("test");
        customerPurchaseOrderRequestVO.setGoodsId(1);
        customerPurchaseOrderRequestVO.setGoodsImage("test");
        customerPurchaseOrderRequestVO.setGoodsName("Test");
        customerPurchaseOrderRequestVO.setGoodsPrice(new BigDecimal("3.14"));
        customerPurchaseOrderRequestVO.setGoodsQuantity(1);
        customerPurchaseOrderRequestVO.setGoodsSku("test");
        customerPurchaseOrderRequestVO.setRemark("test");

        boolean purchased = customerOrderServiceMock.purchaseOrder(customerPurchaseOrderRequestVO);
        Assertions.assertFalse(purchased);

        CustomerBalanceEntity customerBalance = new CustomerBalanceEntity();

        customerBalance.setCustomerId(1);
        customerBalance.setId(1);
        customerBalance.setBalance(new BigDecimal("2.28"));
        customerBalance.setCreateTime(LocalDateTime.now());

        Mockito.doReturn(customerBalance).when(customerBalanceService).getCustomerBalance(Mockito.anyInt());
        purchased = customerOrderServiceMock.purchaseOrder(customerPurchaseOrderRequestVO);
        Assertions.assertFalse(purchased);

        customerBalance.setBalance(new BigDecimal("6.28"));
        customerBalance.setUpdateTime(LocalDateTime.now());
        Mockito.doReturn(customerBalance).when(customerBalanceService).getCustomerBalance(Mockito.anyInt());
        purchased = customerOrderServiceMock.purchaseOrder(customerPurchaseOrderRequestVO);
        Assertions.assertFalse(purchased);

        Mockito.doReturn(1).when(customerOrderMapper).customerPurchaseOrder(Mockito.anyInt(), Mockito.any(CustomerOrderEntity.class));
        purchased = customerOrderServiceMock.purchaseOrder(customerPurchaseOrderRequestVO);
        Assertions.assertTrue(purchased);

        Mockito.doThrow(new RuntimeException("Test")).when(customerOrderMapper).customerPurchaseOrder(Mockito.anyInt(), Mockito.any(CustomerOrderEntity.class));
        purchased = customerOrderServiceMock.purchaseOrder(customerPurchaseOrderRequestVO);
        Assertions.assertFalse(purchased);

        Mockito.when(customerOrderService.purchaseOrder(Mockito.any(CustomerPurchaseOrderRequestVO.class))).thenReturn(false);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/purchase")).andExpect(status().isBadRequest());

        customerPurchaseOrderRequestVO.setGoodsPrice(new BigDecimal("0.001"));
        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(customerPurchaseOrderRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/purchase").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        customerPurchaseOrderRequestVO.setGoodsPrice(new BigDecimal("3.14"));
        requestJson = objectMapper.writeValueAsString(customerPurchaseOrderRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/purchase").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(customerOrderService.purchaseOrder(Mockito.any(CustomerPurchaseOrderRequestVO.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/purchase").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetOperationHistory() throws Exception {
        CustomerHistoryServiceImpl customerHistoryServiceImpl = new CustomerHistoryServiceImpl();
        CustomerHistoryService customerHistoryServiceMock = Mockito.spy(customerHistoryServiceImpl);

        Mockito.doReturn(null).when(customerHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        Map<String, Object> customerHistoryMap = customerHistoryServiceMock.getCustomerHistory(1, 1, 1);
        Assertions.assertNull(customerHistoryMap);

        CustomerHistoryEntity customerHistory = new CustomerHistoryEntity();
        List<CustomerHistoryEntity> historyList = new ArrayList<>();
        Page<CustomerHistoryEntity> historyPage = new Page<>();

        historyList.add(customerHistory);
        historyPage.setCurrent(1);
        historyPage.setPages(1);
        historyPage.setSize(1);
        historyPage.setTotal(1);
        historyPage.setRecords(historyList);

        Mockito.doReturn(historyPage).when(customerHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        customerHistoryMap = customerHistoryServiceMock.getCustomerHistory(1, 1, 1);
        Assertions.assertNotNull(customerHistoryMap);
        Assertions.assertFalse(customerHistoryMap.isEmpty());

        Mockito.when(customerHistoryService.getCustomerHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/user/0")).andExpect(status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/user/1")).andExpect(status().isOk());

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);

        Mockito.when(customerHistoryService.getCustomerHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/user/1")).andExpect(status().isOk());
    }

    @Test
    public void testAddCredit() throws Exception {
        CustomerBalanceDAO customerBalanceDAO = new CustomerBalanceDAO();
        ReflectionTestUtils.setField(customerBalanceDAO, "customerBalanceMapper", customerBalanceMapper);
        CustomerBalanceDAO customerBalanceDAOMock = Mockito.spy(customerBalanceDAO);
        CustomerBalanceServiceImpl customerBalanceServiceImpl = new CustomerBalanceServiceImpl();
        ReflectionTestUtils.setField(customerBalanceServiceImpl, "customerBalanceDAO", customerBalanceDAOMock);
        ReflectionTestUtils.setField(customerBalanceServiceImpl, "customerHistoryService", customerHistoryService);
        CustomerBalanceService customerBalanceServiceMock = Mockito.spy(customerBalanceServiceImpl);

        Mockito.doReturn(true).when(customerHistoryService).save(Mockito.any(CustomerHistoryEntity.class));
        Mockito.doReturn(null).when(customerBalanceMapper).createCustomerCredit(Mockito.any(CustomerBalanceEntity.class));

        boolean success = customerBalanceServiceMock.createCustomerCredit(1, new BigDecimal("3.14"));
        Assertions.assertFalse(success);

        Mockito.doReturn(1).when(customerBalanceMapper).createCustomerCredit(Mockito.any(CustomerBalanceEntity.class));
        success = customerBalanceServiceMock.createCustomerCredit(1, new BigDecimal("3.14"));
        Assertions.assertTrue(success);

        Mockito.doReturn(null).when(customerBalanceMapper).updateCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class), Mockito.any(LocalDateTime.class));
        Mockito.doReturn(null).when(customerBalanceServiceMock).getCustomerBalance(Mockito.anyInt());

        success = customerBalanceServiceMock.updateCustomerCredit(1, new BigDecimal("3.14"));
        Assertions.assertFalse(success);

        CustomerBalanceEntity customerBalance = new CustomerBalanceEntity();

        customerBalance.setId(1);
        customerBalance.setCustomerId(1);
        customerBalance.setBalance(new BigDecimal("13.09"));
        customerBalance.setCreateTime(LocalDateTime.now());

        Mockito.doReturn(1).when(customerBalanceMapper).updateCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class), Mockito.any(LocalDateTime.class));
        Mockito.doReturn(customerBalance).when(customerBalanceServiceMock).getCustomerBalance(Mockito.anyInt());

        success = customerBalanceServiceMock.updateCustomerCredit(1, new BigDecimal("3.14"));
        Assertions.assertTrue(success);

        Mockito.when(customerBalanceService.getCustomerBalance(Mockito.anyInt())).thenReturn(null);
        Mockito.when(customerBalanceService.createCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class))).thenReturn(false);
        Mockito.when(customerBalanceService.updateCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class))).thenReturn(false);

        CustomerAddCreditRequestVO customerAddCreditRequestVO = new CustomerAddCreditRequestVO();
        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        customerAddCreditRequestVO.setCredit(new BigDecimal("78.23"));
        customerAddCreditRequestVO.setCustomerId(0);
        requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        customerAddCreditRequestVO.setCustomerId(1);
        requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(customerBalanceService.createCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(customerBalanceService.getCustomerBalance(Mockito.anyInt())).thenReturn(customerBalance);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(customerBalanceService.updateCustomerCredit(Mockito.anyInt(), Mockito.any(BigDecimal.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetBalance() throws Exception {
        CustomerBalanceServiceImpl CustomerBalanceServiceImpl = new CustomerBalanceServiceImpl();
        CustomerBalanceService CustomerBalanceServiceMock = Mockito.spy(CustomerBalanceServiceImpl);

        Mockito.doReturn(null).when(CustomerBalanceServiceMock).getOne(Mockito.any(LambdaQueryWrapper.class));

        CustomerBalanceEntity customerBalance = CustomerBalanceServiceMock.getCustomerBalance(1);
        Assertions.assertNull(customerBalance);

        customerBalance = new CustomerBalanceEntity();
        Mockito.doReturn(customerBalance).when(CustomerBalanceServiceMock).getOne(Mockito.any(LambdaQueryWrapper.class));

        CustomerBalanceEntity customerBalanceTest = CustomerBalanceServiceMock.getCustomerBalance(1);
        Assertions.assertNotNull(customerBalanceTest);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/credit/user/0")).andExpect(status().isBadRequest());

        Mockito.when(customerBalanceService.getCustomerBalance(Mockito.anyInt())).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/credit/user/1")).andExpect(status().isOk());

        customerBalance.setId(1);
        customerBalance.setCustomerId(1);
        customerBalance.setBalance(new BigDecimal("0.17"));
        customerBalance.setCreateTime(LocalDateTime.now());

        Mockito.when(customerBalanceService.getCustomerBalance(Mockito.anyInt())).thenReturn(customerBalance);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/credit/user/1")).andExpect(status().isOk());
    }
}