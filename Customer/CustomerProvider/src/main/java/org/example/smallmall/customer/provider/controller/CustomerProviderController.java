package org.example.smallmall.customer.provider.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;
import org.example.smallmall.customer.provider.service.CustomerBalanceService;
import org.example.smallmall.customer.provider.service.CustomerHistoryService;
import org.example.smallmall.customer.provider.service.CustomerOrderService;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CustomerProviderController {
    @Value("${query-config.paging.default.number}")
    private Integer defaultPageNumber;

    @Value("${query-config.paging.default.size}")
    private Integer defaultPageSize;

    @Autowired
    private CustomerHistoryService customerHistoryService;

    @Autowired
    private CustomerBalanceService customerBalanceService;

    @Autowired
    private CustomerOrderService customerOrderService;

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity<String> index() {
        String hello = "<html><head><title>Customer Service Provider</title></head><body><h1>Customer Service Provider</h1></body></html>";

        return new ResponseEntity<>(hello, HttpStatus.OK);
    }

    @GetMapping(value = "/order/user/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getOrder(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("customerId") Integer customerId,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> customerGoodsMap = customerOrderService.getOrder(customerId, pageNumber, pageSize);

        if (customerGoodsMap == null || customerGoodsMap.isEmpty()) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(customerGoodsMap);

        return responseVO;
    }

    @PostMapping(value = "/order/purchase", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO purchaseOrder(@Valid @NotNull @RequestBody CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO) {
        ResponseVO responseVO = new ResponseVO();
        boolean purchased = customerOrderService.purchaseOrder(customerPurchaseOrderRequestVO);

        if (purchased) {
            Map<String, Object> resultMap = this.getBalanceResultMap(customerPurchaseOrderRequestVO.getCustomerId());

            responseVO.setStatus(HttpStatus.OK.value());
            responseVO.setMessage(HttpStatus.OK.toString());
            responseVO.setData(resultMap);
        } else {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Purchase goods failed!");
        }

        return responseVO;
    }

    @GetMapping(value = "/history/user/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getOperationHistory(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("customerId") Integer customerId,
                                          @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                          @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> customerGoodsMap = customerHistoryService.getCustomerHistory(customerId, pageNumber, pageSize);

        if (customerGoodsMap == null || customerGoodsMap.isEmpty()) {
            responseVO.setMessage("No query result found!");
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());

            return responseVO;
        }

        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(customerGoodsMap);
        responseVO.setStatus(HttpStatus.OK.value());

        return responseVO;
    }

    @PostMapping(value = "/credit", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO addCredit(@Valid @NotNull @RequestBody CustomerAddCreditRequestVO customerAddCreditRequestVO) {
        ResponseVO responseVO = new ResponseVO();
        Integer customerId = customerAddCreditRequestVO.getCustomerId();
        BigDecimal credit = customerAddCreditRequestVO.getCredit();
        Map<String, Object> resultMap;
        CustomerBalanceEntity customerBalance = customerBalanceService.getCustomerBalance(customerId);

        if (customerBalance == null) {
            boolean created = customerBalanceService.createCustomerCredit(customerId, credit);

            if (created) {
                resultMap = this.getBalanceResultMap(customerId);
                responseVO.setStatus(HttpStatus.OK.value());
                responseVO.setMessage(HttpStatus.OK.toString());
                responseVO.setData(resultMap);
            } else {
                responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                responseVO.setMessage("Increase balance failed!");
            }

            return responseVO;
        }

        boolean updated = customerBalanceService.updateCustomerCredit(customerId, credit);

        if (updated) {
            resultMap = this.getBalanceResultMap(customerId);
            responseVO.setStatus(HttpStatus.OK.value());
            responseVO.setMessage(HttpStatus.OK.toString());
            responseVO.setData(resultMap);
        } else {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Increase balance failed!");
        }

        return responseVO;
    }

    @GetMapping(value = "/credit/user/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getBalance(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("customerId") Integer customerId) {
        ResponseVO responseVO = new ResponseVO();
        Map<String, Object> resultMap = this.getBalanceResultMap(customerId);

        if (resultMap == null || resultMap.isEmpty()) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage(HttpStatus.NOT_FOUND.toString());
        } else {
            responseVO.setData(resultMap);
            responseVO.setStatus(HttpStatus.OK.value());
            responseVO.setMessage(HttpStatus.OK.toString());
        }

        return responseVO;
    }

    private Map<String, Object> getBalanceResultMap(Integer customerId) {
        Map<String, Object> resultMap = new HashMap<>();
        CustomerBalanceEntity balanceEntity = customerBalanceService.getCustomerBalance(customerId);

        if (balanceEntity != null && balanceEntity.getBalance() != null) {
            resultMap.put("balance", balanceEntity.getBalance());

            if (balanceEntity.getUpdateTime() != null) {
                resultMap.put("updateTime", balanceEntity.getUpdateTime());
            } else {
                resultMap.put("updateTime", balanceEntity.getCreateTime());
            }
        }

        return resultMap;
    }
}