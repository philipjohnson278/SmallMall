package org.example.smallmall.customer.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;

@Mapper
public interface CustomerHistoryMapper extends BaseMapper<CustomerHistoryEntity> {
}