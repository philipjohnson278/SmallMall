package org.example.smallmall.customer.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_customer_order")
@Data
public class CustomerOrderEntity implements Serializable {
    private static final long serialVersionUID = 3041241336616206788L;

    private int id;
    private int customerId;
    private int goodsId;
    private String goodsName;
    private String goodsSku;
    private String goodsImage;
    private String goodsDescription;
    private Integer goodsQuantity;
    private BigDecimal goodsPrice;
    private BigDecimal goodsAmount;
    private byte state;
    private String remark;
    private LocalDateTime createTime;
}