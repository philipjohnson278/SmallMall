package org.example.smallmall.customer.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.smallmall.customer.provider.entity.CustomerOrderEntity;

@Mapper
public interface CustomerOrderMapper extends BaseMapper<CustomerOrderEntity> {
    public Integer customerPurchaseOrder(@Param("merchantId") Integer merchantId, @Param("order") CustomerOrderEntity customerOrder);
}