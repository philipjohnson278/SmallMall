package org.example.smallmall.customer.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;
import org.example.smallmall.customer.provider.mapper.CustomerHistoryMapper;
import org.example.smallmall.customer.provider.service.CustomerHistoryService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CustomerHistoryServiceImpl extends ServiceImpl<CustomerHistoryMapper, CustomerHistoryEntity> implements CustomerHistoryService {
    @Override
    public Map<String, Object> getCustomerHistory(Integer customerId, Integer pageNumber, Integer pageSize) {
        LambdaQueryWrapper<CustomerHistoryEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(CustomerHistoryEntity.class)
                                                                               .eq(CustomerHistoryEntity::getCustomerId, customerId)
                                                                               .orderByDesc(CustomerHistoryEntity::getCreateTime);
        Page<CustomerHistoryEntity> customerHistoryPage = this.page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);

        if (customerHistoryPage == null || customerHistoryPage.getRecords() == null) {
            log.error("No customer history found for query argument: customer ID: {}, pageNumber: {}, pageSize: {}", customerId, pageNumber, pageSize);

            return null;
        }

        List<CustomerHistoryEntity> historyList = customerHistoryPage.getRecords();
        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("list", historyList);
        resultMap.put("totalPages", customerHistoryPage.getPages() + 1);
        resultMap.put("currentPage", customerHistoryPage.getCurrent());
        resultMap.put("pageSize", historyList.size());

        return resultMap;
    }
}