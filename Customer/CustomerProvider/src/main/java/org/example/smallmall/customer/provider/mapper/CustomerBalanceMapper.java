package org.example.smallmall.customer.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Mapper
public interface CustomerBalanceMapper extends BaseMapper<CustomerBalanceEntity> {
    public Integer createCustomerCredit(CustomerBalanceEntity customerBalance);
    public Integer updateCustomerCredit(@Param("customerId") Integer customerId, @Param("credit") BigDecimal credit, @Param("updateTime") LocalDateTime updateTime);
}