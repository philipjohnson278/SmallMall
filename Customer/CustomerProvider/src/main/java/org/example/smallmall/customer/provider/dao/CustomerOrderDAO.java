package org.example.smallmall.customer.provider.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.provider.entity.CustomerOrderEntity;
import org.example.smallmall.customer.provider.mapper.CustomerOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Slf4j
@Component
public class CustomerOrderDAO {
    @Autowired
    private CustomerOrderMapper customerOrderMapper;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
    public boolean customerPurchaseOrder(Integer merchantId, CustomerOrderEntity customerOrder) {
        Integer orderId = 0;
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();

        try {
            orderId = customerOrderMapper.customerPurchaseOrder(merchantId, customerOrder);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            try {
                log.error("Customer purchase order failed by exception, rollback transaction now, merchant ID: {}, order: {}, exception message: {}", merchantId, objectMapper.writeValueAsString(customerOrder), e.getMessage());
            } catch (JsonProcessingException ex) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }
        }

        try {
            log.info("Customer purchase order ID: {}, merchant ID: {}, order: {}", orderId, merchantId, objectMapper.writeValueAsString(customerOrder));
        } catch (JsonProcessingException e) {
            log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
        }

        if (orderId == null || orderId <= 0) {
            return false;
        }

        customerOrder.setId(orderId);

        return true;
    }
}