package org.example.smallmall.customer.provider.service.impl;

import com.alibaba.nacos.common.utils.JacksonUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.provider.dao.CustomerOrderDAO;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;
import org.example.smallmall.customer.provider.entity.CustomerOrderEntity;
import org.example.smallmall.customer.provider.mapper.CustomerOrderMapper;
import org.example.smallmall.customer.provider.service.CustomerBalanceService;
import org.example.smallmall.customer.provider.service.CustomerHistoryService;
import org.example.smallmall.customer.provider.service.CustomerOrderService;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CustomerOrderServiceImpl extends ServiceImpl<CustomerOrderMapper, CustomerOrderEntity> implements CustomerOrderService {
    @Autowired
    private CustomerOrderDAO customerOrderDAO;

    @Autowired
    private CustomerBalanceService customerBalanceService;

    @Autowired
    private CustomerHistoryService customerHistoryService;

    @Override
    public Map<String, Object> getOrder(Integer customerId, Integer pageNumber, Integer pageSize) {
        LambdaQueryWrapper<CustomerOrderEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(CustomerOrderEntity.class)
                                                                             .eq(CustomerOrderEntity::getCustomerId, customerId)
                                                                             .orderByDesc(CustomerOrderEntity::getCreateTime);
        Page<CustomerOrderEntity> customerGoodsPage = this.page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);

        if (customerGoodsPage == null || customerGoodsPage.getRecords() == null) {
            log.error("No customer goods found for query argument: customer ID: {}, pageNumber: {}, pageSize: {}", customerId, pageNumber, pageSize);

            return null;
        }

        Map<String, Object> resultMap = new HashMap<>();
        List<CustomerOrderEntity> orderList = customerGoodsPage.getRecords();

        resultMap.put("list", orderList);
        resultMap.put("totalPages", customerGoodsPage.getPages() + 1);
        resultMap.put("currentPage", customerGoodsPage.getCurrent());
        resultMap.put("pageSize", orderList.size());

        return resultMap;
    }

    @Override
    public boolean purchaseOrder(CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO) {
        Integer merchantId = customerPurchaseOrderRequestVO.getMerchantId();
        CustomerOrderEntity customerOrder = null;
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();

        try {
            customerOrder = objectMapper.convertValue(customerPurchaseOrderRequestVO, CustomerOrderEntity.class);
        } catch (Exception e) {
            Map<String, Object> customerOrderMap = objectMapper.convertValue(customerPurchaseOrderRequestVO, Map.class);

            customerOrderMap.remove("merchantId");
            customerOrder = objectMapper.convertValue(customerOrderMap, CustomerOrderEntity.class);
        }

        if (customerOrder == null) {
            Map<String, Object> requestParameterMap = objectMapper.convertValue(customerPurchaseOrderRequestVO, Map.class);
            log.error("Can not convert CustomerPurchaseGoodsRequestVO to CustomerGoodsEntity: {}", JacksonUtils.toJson(requestParameterMap));

            return false;
        }

        Integer goodsQuantity = customerOrder.getGoodsQuantity();
        BigDecimal goodsPrice = customerOrder.getGoodsPrice();
        BigDecimal goodsAmount = goodsPrice.multiply(new BigDecimal(goodsQuantity));
        CustomerBalanceEntity customerBalance = customerBalanceService.getCustomerBalance(customerOrder.getCustomerId());

        if (customerBalance == null || customerBalance.getBalance() == null) {
            log.error("Can not find balance info of customer ID: {}", customerOrder.getCustomerId());

            return false;
        }

        BigDecimal balance = customerBalance.getBalance();

        if (balance.compareTo(goodsAmount) < 0) {
            log.error("Balance of customer ID {} not enough to purchase goods, current balance: {}, expected balance: {}, goods: {}", customerOrder.getCustomerId(), balance, goodsAmount, JacksonUtils.toJson(customerOrder));

            return false;
        }

        customerOrder.setState((byte) 1);
        customerOrder.setCreateTime(LocalDateTime.now());

        boolean purchased = customerOrderDAO.customerPurchaseOrder(merchantId, customerOrder);

        if (purchased) {
            try {
                log.info("Customer purchase goods success, merchant ID: {}, goods: {}", merchantId, objectMapper.writeValueAsString(customerOrder));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            CustomerHistoryEntity customerHistory = new CustomerHistoryEntity();

            customerHistory.setBalance(customerBalance.getBalance());
            customerHistory.setCustomerId(customerBalance.getCustomerId());
            customerHistory.setTargetId(customerOrder.getId());
            customerHistory.setOperation("PURCHASE");
            customerHistory.setCreateTime(LocalDateTime.now());
            customerHistoryService.save(customerHistory);

            return true;
        } else {
            try {
                log.error("Customer purchase goods failed, merchant ID: {}, goods: {}", merchantId, objectMapper.writeValueAsString(customerOrder));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }
        }

        return false;
    }
}