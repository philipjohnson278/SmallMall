package org.example.smallmall.customer.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_customer_history")
@Data
public class CustomerHistoryEntity implements Serializable {
    private static final long serialVersionUID = 5916246692640808155L;

    private int id;
    private int customerId;
    private String operation;
    private int targetId;
    private BigDecimal credit;
    private BigDecimal balance;
    private LocalDateTime createTime;
}