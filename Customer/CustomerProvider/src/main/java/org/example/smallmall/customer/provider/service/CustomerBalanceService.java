package org.example.smallmall.customer.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;

import java.math.BigDecimal;

public interface CustomerBalanceService extends IService<CustomerBalanceEntity> {
    public boolean createCustomerCredit(Integer customerId, BigDecimal credit);
    public boolean updateCustomerCredit(Integer customerId, BigDecimal credit);
    public CustomerBalanceEntity getCustomerBalance(int customerId);
}