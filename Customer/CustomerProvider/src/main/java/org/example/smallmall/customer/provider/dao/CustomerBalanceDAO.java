package org.example.smallmall.customer.provider.dao;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;
import org.example.smallmall.customer.provider.mapper.CustomerBalanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@Component
public class CustomerBalanceDAO {
    @Autowired
    private CustomerBalanceMapper customerBalanceMapper;

    public boolean createCustomerBalance(CustomerBalanceEntity customerBalance) {
        Integer affectedRow = customerBalanceMapper.createCustomerCredit(customerBalance);

        if (affectedRow == null || affectedRow <= 0) {
            log.error("Insert customer balance record failed, customer ID: {}, credit: {}", customerBalance.getCustomerId(), customerBalance.getBalance());

            return false;
        }

        log.info("Update credit of customer, customer ID: {}, credit: {}, affected row: {}", customerBalance.getCustomerId(), customerBalance.getBalance(), affectedRow);

        return true;
    }

    public boolean updateCustomerBalance(Integer customerId, BigDecimal credit, LocalDateTime updateTime) {
        Integer updatedRow = customerBalanceMapper.updateCustomerCredit(customerId, credit, updateTime);

        if (updatedRow == null || updatedRow <= 0) {
            log.error("Update customer credit failed, customer ID: {}, credit: {}", customerId, credit);

            return false;
        }

        log.info("Update credit of customer, customer ID: {}, credit: {}, affected row: {}", customerId, credit, updatedRow);

        return true;
    }
}