package org.example.smallmall.customer.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.provider.dao.CustomerBalanceDAO;
import org.example.smallmall.customer.provider.entity.CustomerBalanceEntity;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;
import org.example.smallmall.customer.provider.mapper.CustomerBalanceMapper;
import org.example.smallmall.customer.provider.service.CustomerBalanceService;
import org.example.smallmall.customer.provider.service.CustomerHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@Service
public class CustomerBalanceServiceImpl extends ServiceImpl<CustomerBalanceMapper, CustomerBalanceEntity> implements CustomerBalanceService {
    @Autowired
    private CustomerBalanceDAO customerBalanceDAO;

    @Autowired
    private CustomerHistoryService customerHistoryService;

    @Override
    public boolean createCustomerCredit(Integer customerId, BigDecimal credit) {
        CustomerBalanceEntity customerBalance = new CustomerBalanceEntity();

        customerBalance.setBalance(credit);
        customerBalance.setCustomerId(customerId);
        customerBalance.setCreateTime(LocalDateTime.now());

        boolean inserted = customerBalanceDAO.createCustomerBalance(customerBalance);

        log.info("Insert balance record of customer with customer ID: {}, credit: {}, result: {}", customerId, credit, inserted);

        if (inserted) {
            CustomerHistoryEntity customerHistory = new CustomerHistoryEntity();

            customerHistory.setBalance(BigDecimal.ZERO);
            customerHistory.setCredit(credit);
            customerHistory.setCustomerId(customerId);
            customerHistory.setTargetId(customerBalance.getId());
            customerHistory.setOperation("CREATE");
            customerHistory.setCreateTime(LocalDateTime.now());
            customerHistoryService.save(customerHistory);
        }

        return inserted;
    }

    public boolean updateCustomerCredit(Integer customerId, BigDecimal credit) {
        CustomerBalanceEntity customerBalance = this.getCustomerBalance(customerId);
        boolean updated = customerBalanceDAO.updateCustomerBalance(customerId, credit, LocalDateTime.now());

        if (updated) {
            CustomerHistoryEntity customerHistory = new CustomerHistoryEntity();

            customerHistory.setBalance(customerBalance.getBalance());
            customerHistory.setCredit(credit);
            customerHistory.setCustomerId(customerId);
            customerHistory.setTargetId(customerBalance.getId());
            customerHistory.setOperation("UPDATE");
            customerHistory.setCreateTime(LocalDateTime.now());
            customerHistoryService.save(customerHistory);
        }

        log.info("Update credit of customer, customer ID: {}, credit: {}, result: {}", customerId, credit, updated);

        return updated;
    }

    @Override
    public CustomerBalanceEntity getCustomerBalance(int customerId) {
        LambdaQueryWrapper<CustomerBalanceEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(CustomerBalanceEntity.class).eq(CustomerBalanceEntity::getCustomerId, customerId);
        CustomerBalanceEntity balanceEntity = this.getOne(lambdaQueryWrapper);

        return balanceEntity;
    }
}