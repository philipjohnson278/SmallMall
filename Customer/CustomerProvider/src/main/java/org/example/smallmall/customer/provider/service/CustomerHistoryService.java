package org.example.smallmall.customer.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.customer.provider.entity.CustomerHistoryEntity;

import java.util.Map;

public interface CustomerHistoryService extends IService<CustomerHistoryEntity> {
    public Map<String, Object> getCustomerHistory(Integer customerId, Integer pageNumber, Integer pageSize);
}