package org.example.smallmall.customer.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class CustomerProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerProviderApplication.class, args);
    }
}