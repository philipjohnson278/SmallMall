package org.example.smallmall.customer.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_customer_balance")
@Data
public class CustomerBalanceEntity implements Serializable {
    private static final long serialVersionUID = 6984630346707740695L;

    private int id;
    private int customerId;
    private BigDecimal balance;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}