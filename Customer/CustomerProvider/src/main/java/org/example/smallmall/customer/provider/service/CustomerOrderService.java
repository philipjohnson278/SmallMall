package org.example.smallmall.customer.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.customer.provider.entity.CustomerOrderEntity;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;

import java.util.Map;

public interface CustomerOrderService extends IService<CustomerOrderEntity> {
    public Map<String, Object> getOrder(Integer customerId, Integer pageNumber, Integer pageSize);
    public boolean purchaseOrder(CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO);
}