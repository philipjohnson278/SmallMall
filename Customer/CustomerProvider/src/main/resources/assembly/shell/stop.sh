#!/bin/bash

cd `dirname $0`

BIN_DIR=`pwd`

cd ..

DEPLOY_DIR=`pwd`

PIDS=`ps -ef | grep java | grep "$DEPLOY_DIR" | awk '{print $2}'`

echo  "Stop applicaation ..."

if [ -z "${PIDS}" ]; then
	echo "No PID found!"
	exit 0;
else
	kill -TERM "${PIDS}"
	
	sleep 3
	
	PIDS=`ps -ef | grep java | grep "$DEPLOY_DIR" | awk '{print $2}'`
	
	if [ -z "${PIDS}" ]; then
		echo "OK!"
	else
		kill -9 ${PIDS}
		echo "OK, stop application with kill -9"
	fi
fi