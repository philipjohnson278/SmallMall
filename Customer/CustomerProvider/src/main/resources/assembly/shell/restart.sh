#!/bin/bash

cd `dirname $0`

BIN_DIR=`pwd`

cd ..

DEPLOY_DIR=`pwd`

sh ${DEPLOY_DIR}/bin/stop.sh
sh ${DEPLOY_DIR}/bin/start.sh "${1}"