package org.example.smallmall.customer.authenticate.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.customer.authenticate.mapper.CustomerProfileMapper;
import org.example.smallmall.customer.authenticate.service.CustomerProfileService;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CustomerProfileServiceImpl extends ServiceImpl<CustomerProfileMapper, CustomerProfileEntity> implements CustomerProfileService {
    @Override
    public CustomerProfileEntity getCustomer(String customerName) {
        LambdaQueryWrapper<CustomerProfileEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(CustomerProfileEntity.class).eq(CustomerProfileEntity::getName, customerName);
        CustomerProfileEntity customerProfile = this.getOne(lambdaQueryWrapper);

        return customerProfile;
    }

    @Override
    public boolean customerRegistry(CommonLoginRequestVO commonLoginRequestVO) {
        CustomerProfileEntity customerProfile = this.getCustomer(commonLoginRequestVO.getName());

        if (customerProfile != null) {
            log.error("Can not registry a new customer, already exists customer name: {}", commonLoginRequestVO.getName());

            return false;
        }

        customerProfile = new CustomerProfileEntity();
        customerProfile.setName(commonLoginRequestVO.getName());
        customerProfile.setSecret(commonLoginRequestVO.getSecret());
        customerProfile.setState((byte) 1);
        customerProfile.setCreateTime(commonLoginRequestVO.getRequestTime());

        try {
            boolean saved = this.save(customerProfile);

            log.info("Registry a new customer, name: {}, request time: {}, result: {}.", commonLoginRequestVO.getName(), commonLoginRequestVO.getRequestTime(), saved);

            return saved;
        } catch (Exception e) {
            log.error("Registry a new customer failed by exception, name: {}, request time: {}, exception: {}", commonLoginRequestVO.getName(), commonLoginRequestVO.getRequestTime(), e.getMessage());
        }

        return false;
    }
}