package org.example.smallmall.customer.authenticate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;

public interface CustomerProfileService extends IService<CustomerProfileEntity> {
    public CustomerProfileEntity getCustomer(String customerName);
    public boolean customerRegistry(CommonLoginRequestVO commonLoginRequestVO);
}