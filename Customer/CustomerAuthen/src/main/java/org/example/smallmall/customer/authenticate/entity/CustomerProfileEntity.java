package org.example.smallmall.customer.authenticate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("tbl_customer_profile")
@Data
public class CustomerProfileEntity implements Serializable {
    private static final long serialVersionUID = -4813446757051944254L;

    private int id;
    private String name;
    private String secret;
    private byte state;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}