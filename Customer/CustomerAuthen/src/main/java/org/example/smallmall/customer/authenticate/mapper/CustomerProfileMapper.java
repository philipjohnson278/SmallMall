package org.example.smallmall.customer.authenticate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;

@Mapper
public interface CustomerProfileMapper extends BaseMapper<CustomerProfileEntity> {
}