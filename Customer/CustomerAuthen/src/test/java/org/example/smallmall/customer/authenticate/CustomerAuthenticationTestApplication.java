package org.example.smallmall.customer.authenticate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerAuthenticationTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerAuthenticationTestApplication.class, args);
    }
}