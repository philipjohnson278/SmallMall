package org.example.smallmall.customer.authenticate;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.customer.authenticate.service.CustomerProfileService;
import org.example.smallmall.customer.authenticate.service.Impl.CustomerProfileServiceImpl;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

@SpringBootTest(classes = {CustomerAuthenticationTestApplication.class})
@ActiveProfiles(profiles = {"common"})
public class CustomerProfileServiceTests {
    private CustomerProfileService customerProfileService;

    @BeforeEach
    public void setup() {
        CustomerProfileServiceImpl customerProfileServiceImpl = new CustomerProfileServiceImpl();
        customerProfileService = Mockito.spy(customerProfileServiceImpl);
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testGetCustomer() {
        CustomerProfileEntity customerProfile = new CustomerProfileEntity();
        Mockito.doReturn(customerProfile).when(customerProfileService).getOne(Mockito.any(LambdaQueryWrapper.class));
        CustomerProfileEntity customerProfileTest = customerProfileService.getCustomer("test");

        Assertions.assertNotNull(customerProfileTest);
        Assertions.assertNull(customerProfileTest.getName());
        Assertions.assertNull(customerProfileTest.getSecret());
        Assertions.assertNull(customerProfileTest.getCreateTime());
        Assertions.assertNull(customerProfileTest.getUpdateTime());
        Assertions.assertTrue(customerProfileTest.getId() == 0);
        Assertions.assertTrue(customerProfileTest.getState() == 0);
    }

    @Test
    public void testCustomerRegistry() {
        Mockito.doReturn(null).when(customerProfileService).getCustomer(Mockito.anyString());
        Mockito.doReturn(false).when(customerProfileService).save(Mockito.any(CustomerProfileEntity.class));

        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName("test");
        commonLoginRequestVO.setSecret("test");
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        boolean success = customerProfileService.customerRegistry(commonLoginRequestVO);
        Assertions.assertFalse(success);

        Mockito.doReturn(true).when(customerProfileService).save(Mockito.any(CustomerProfileEntity.class));
        success = customerProfileService.customerRegistry(commonLoginRequestVO);
        Assertions.assertTrue(success);

        CustomerProfileEntity customerProfile = new CustomerProfileEntity();

        customerProfile.setName("test");
        customerProfile.setSecret("test");

        Mockito.doReturn(customerProfile).when(customerProfileService).getCustomer(Mockito.anyString());
        success = customerProfileService.customerRegistry(commonLoginRequestVO);
        Assertions.assertFalse(success);
    }
}