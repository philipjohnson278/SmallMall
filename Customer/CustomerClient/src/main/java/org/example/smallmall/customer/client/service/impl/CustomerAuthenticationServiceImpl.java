package org.example.smallmall.customer.client.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.customer.authenticate.service.CustomerProfileService;
import org.example.smallmall.customer.client.service.CustomerAuthenticationService;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@Service
public class CustomerAuthenticationServiceImpl implements CustomerAuthenticationService {
    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public CommonLoginResponseVO createAuthenticationInfo(CommonLoginRequestVO loginRequestVO) {
        CustomerProfileEntity customerProfile = customerProfileService.getCustomer(loginRequestVO.getName());

        if (customerProfile == null) {
            return null;
        }

        String authentication = loginRequestVO.getName() + ":" + loginRequestVO.getSecret();
        CommonLoginResponseVO loginResponseVO = new CommonLoginResponseVO();

        loginResponseVO.setId(customerProfile.getId());
        loginResponseVO.setName(customerProfile.getName());
        loginResponseVO.setState(customerProfile.getState());
        loginResponseVO.setAuthen(Base64.getEncoder().encodeToString(authentication.getBytes(StandardCharsets.UTF_8)));

        return loginResponseVO;
    }

    @Override
    public boolean register(CommonLoginRequestVO loginRequestVO) {
        String password = loginRequestVO.getSecret();
        String secret = passwordEncoder.encode(password);

        loginRequestVO.setSecret(secret);

        boolean registered = customerProfileService.customerRegistry(loginRequestVO);

        return registered;
    }
}