package org.example.smallmall.customer.client.feign.fallback;

import org.example.smallmall.customer.client.feign.CustomerOpenFeignClient;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;

public class CustomerOpenFeignClientFallback implements CustomerOpenFeignClient {
    @Override
    public String listOrder(Integer userId, Integer pageNumber, Integer pageSize) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String purchaseOrder(CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Purchase goods failed!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String getCredit(Integer userId) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String increaseCredit(CustomerAddCreditRequestVO customerAddCreditRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Increase balance failed!\", \"data\": null}";

        return fallbackJson;
    }
}