package org.example.smallmall.customer.client.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.customer.client.feign.CustomerOpenFeignClient;
import org.example.smallmall.customer.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.customer.client.service.CustomerAuthenticationService;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CustomerClientController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private SecurityContextRepository securityContextRepository;

    @Autowired
    private SecurityContextHolderStrategy securityContextHolderStrategy;

    @Autowired
    private CustomerAuthenticationService authenticationService;

    @Autowired
    private CustomerOpenFeignClient customerOpenFeignClient;

    @Autowired
    private MerchantOpenFeignClient merchantOpenFeignClient;

    @GetMapping(value = {"/", "/page", "/pages"}, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity<String> index() {
        String hello = "<html><head><title>Customer Application</title></head><body><h1>Customer Application</h1></body></html>";

        return new ResponseEntity<>(hello, HttpStatus.OK);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "login", blockHandler = "loginBlockHelper")
    public ResponseVO login(@NotBlank @RequestParam("name") String userName, @NotBlank @RequestParam("secret") String password,
                            HttpServletRequest request, HttpServletResponse response) {
        ResponseVO responseVO = new ResponseVO();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName(userName);
        commonLoginRequestVO.setSecret(password);
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        context.setAuthentication(authentication);
        securityContextHolderStrategy.setContext(context);
        securityContextRepository.saveContext(context, request, response);

        CommonLoginResponseVO authenticateVO = authenticationService.createAuthenticationInfo(commonLoginRequestVO);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> resultMap = objectMapper.convertValue(authenticateVO, Map.class);

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage("Login success!");
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "register", blockHandler = "registerBlockHelper")
    public ResponseVO register(@NotBlank @RequestParam("name") String userName, @NotBlank @RequestParam("secret") String password) {
        ResponseVO responseVO = new ResponseVO();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName(userName);
        commonLoginRequestVO.setSecret(password);
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        boolean registered = authenticationService.register(commonLoginRequestVO);

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.name());

        if (registered) {
            Map<String, Object> resultMap = new HashMap<>();

            resultMap.put("result", registered);
            responseVO.setData(resultMap);
        }

        return responseVO;
    }

    @GetMapping(value = "/order/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getUserOrder", blockHandler = "getUserOrderBlockHelper")
    public String getUserOrder(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("userId") Integer userId,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer pageNumber,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer pageSize) {
        String orderJson = customerOpenFeignClient.listOrder(userId, pageNumber, pageSize);

        return orderJson;
    }

    @PostMapping(value = "/order", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "purchaseOrder", blockHandler = "purchaseOrderBlockHelper")
    public String purchaseOrder(@Valid @NotNull @RequestBody CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO) {
        String resultJson = customerOpenFeignClient.purchaseOrder(customerPurchaseOrderRequestVO);

        return resultJson;
    }

    @GetMapping(value = "/credit/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getUserCredit", blockHandler = "getUserCreditBlockHelper")
    public String getUserCredit(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("userId") Integer userId) {
        String creditJson = customerOpenFeignClient.getCredit(userId);

        return creditJson;
    }

    @PostMapping(value = "/credit", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "increaseUserCredit", blockHandler = "increaseUserCreditBlockHelper")
    public String increaseUserCredit(@Valid @NotNull @RequestBody CustomerAddCreditRequestVO customerAddCreditRequestVO) {
        String resultJson = customerOpenFeignClient.increaseCredit(customerAddCreditRequestVO);

        return resultJson;
    }

    @GetMapping(value = "/goods", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getGoodsList", blockHandler = "getGoodsListBlockHelper")
    public String getGoodsList(@Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer pageNumber,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer pageSize) {
        String productJson = merchantOpenFeignClient.getProduct(pageNumber, pageSize);

        return productJson;
    }

    @GetMapping(value = "/goods/{goodsId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getGoodsById", blockHandler = "getGoodsByIdBlockHelper")
    public String getGoodsById(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("goodsId") Integer goodsId) {
        String productJson = merchantOpenFeignClient.getProductbyId(goodsId);

        return productJson;
    }

    public ResponseVO loginBlockHelper(String userName, String password, HttpServletRequest request, HttpServletResponse response, BlockException exception) {
        ResponseVO responseVO = new ResponseVO();

        responseVO.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
        responseVO.setMessage("Login failed!");

        return responseVO;
    }

    public ResponseVO registerBlockHelper(String userName, String password, BlockException exception) {
        ResponseVO responseVO = new ResponseVO();

        responseVO.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
        responseVO.setMessage("Registry failed!");

        return responseVO;
    }

    public String getUserOrderBlockHelper(Integer userId, Integer pageNumber, Integer pageSize, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String purchaseOrderBlockHelper(CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getUserCreditBlockHelper(Integer userId, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String increaseUserCreditBlockHelper(CustomerAddCreditRequestVO customerAddCreditRequestVO, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getGoodsListBlockHelper(Integer pageNumber, Integer pageSize, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getGoodsByIdBlockHelper(Integer goodsId, BlockException exception) {
        return requestBlockHelper(exception);
    }

    private String requestBlockHelper(BlockException exception) {
        String blockHelperJson;

        if (exception instanceof DegradeException) {
            blockHelperJson = "{\"status\": 503, \"message\": \"Service down graded!\", \"data\": null}";
        } else {
            blockHelperJson = "{\"status\": 404, \"message\": \"Service out of range!\", \"data\": null}";
        }

        return blockHelperJson;
    }
}