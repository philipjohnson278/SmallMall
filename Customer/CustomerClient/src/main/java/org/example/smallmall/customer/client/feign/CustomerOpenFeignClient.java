package org.example.smallmall.customer.client.feign;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.customer.client.config.OpenFeignConfiguration;
import org.example.smallmall.customer.client.feign.fallback.CustomerOpenFeignClientFallback;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service-provider.service-name.customer}", path = "${service-provider.context-path.customer}", fallback = CustomerOpenFeignClientFallback.class, configuration = OpenFeignConfiguration.class)
public interface CustomerOpenFeignClient {
    @GetMapping("/order/user/{userId}")
    String listOrder(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("userId") Integer userId,
                     @RequestParam(name = "page", required = false) Integer pageNumber,
                     @RequestParam(name = "size", required = false) Integer pageSize);

    @PostMapping("/order/purchase")
    String purchaseOrder(@NotNull @RequestBody CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO);

    @GetMapping("/credit/user/{userId}")
    String getCredit(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("userId") Integer userId);

    @PostMapping("/credit")
    String increaseCredit(@NotNull @RequestBody CustomerAddCreditRequestVO customerAddCreditRequestVO);
}