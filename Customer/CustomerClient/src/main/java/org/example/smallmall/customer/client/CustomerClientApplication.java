package org.example.smallmall.customer.client;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient(autoRegister = false)
@LoadBalancerClients
@EnableFeignClients
@ComponentScan(basePackages = {"org.example.smallmall.customer"})
@MapperScan(basePackages = {"org.example.smallmall.customer.authenticate.mapper"})
public class CustomerClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerClientApplication.class, args);
    }
}