package org.example.smallmall.customer.client.feign.fallback;

import org.example.smallmall.customer.client.feign.MerchantOpenFeignClient;

public class MerchantOpenFeignClientFallback implements MerchantOpenFeignClient {
    @Override
    public String getProduct(Integer page, Integer size) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String getProductbyId(Integer productId) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }
}
