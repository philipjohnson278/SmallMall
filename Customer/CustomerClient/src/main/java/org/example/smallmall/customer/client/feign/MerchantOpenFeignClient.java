package org.example.smallmall.customer.client.feign;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.customer.client.config.OpenFeignConfiguration;
import org.example.smallmall.customer.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service-provider.service-name.merchant}", path = "${service-provider.context-path.merchant}", fallback = MerchantOpenFeignClientFallback.class, configuration = OpenFeignConfiguration.class)
public interface MerchantOpenFeignClient {
    @GetMapping("/product")
    public String getProduct(@Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                             @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size);

    @GetMapping("/product/{productId}")
    public String getProductbyId(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("productId") Integer productId);
}