package org.example.smallmall.customer.client.config;

import org.example.smallmall.customer.client.feign.CustomerOpenFeignClient;
import org.example.smallmall.customer.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.customer.client.feign.fallback.CustomerOpenFeignClientFallback;
import org.example.smallmall.customer.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.springframework.context.annotation.Bean;

public class OpenFeignConfiguration {
    @Bean
    public CustomerOpenFeignClient customerFeignClientFallback() {
        return new CustomerOpenFeignClientFallback();
    }

    @Bean
    public MerchantOpenFeignClient merchantFeignClientFallback() {
        return new MerchantOpenFeignClientFallback();
    }
}