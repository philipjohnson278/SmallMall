package org.example.smallmall.customer.client.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.customer.authenticate.service.CustomerProfileService;
import org.example.smallmall.customer.client.service.CustomerDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class CustomerDetailsServiceImpl implements CustomerDetailsService {
    @Autowired
    private CustomerProfileService customerProfileService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CustomerProfileEntity customerProfile = customerProfileService.getCustomer(username);

        if (customerProfile == null) {
            log.error("Can not find customer: {}", username);

            throw new UsernameNotFoundException("Customer " + username + " not found!");
        }

        UserDetails userDetails = new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList("ROLE_USER");

                return authorityList;
            }

            @Override
            public String getPassword() {
                return customerProfile.getSecret();
            }

            @Override
            public String getUsername() {
                return customerProfile.getName();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                if (customerProfile.getState() == 1) {
                    return true;
                }

                return false;
            }
        };

        return userDetails;
    }
}