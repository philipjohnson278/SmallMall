package org.example.smallmall.customer.client.loadbalancer;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.stereotype.Component;

@Component
@LoadBalancerClient(value = "customer-provider", configuration = CustomerProviderLoadBalancer.class)
public class CustomerLoadBalancerClient {
}