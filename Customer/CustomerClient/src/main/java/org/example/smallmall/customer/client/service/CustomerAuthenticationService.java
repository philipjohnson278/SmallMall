package org.example.smallmall.customer.client.service;

import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;

public interface CustomerAuthenticationService {
    public CommonLoginResponseVO createAuthenticationInfo(CommonLoginRequestVO loginRequestVO);
    public boolean register(CommonLoginRequestVO loginRequestVO);
}