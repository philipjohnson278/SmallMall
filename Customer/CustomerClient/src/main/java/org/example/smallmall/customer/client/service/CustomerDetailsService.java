package org.example.smallmall.customer.client.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomerDetailsService extends UserDetailsService {
}