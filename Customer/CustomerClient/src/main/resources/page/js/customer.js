var Customer;

async function httpRequestGet(url = "") {
    const response = await fetch(url, {
        method: "GET",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
    });
    return response.json();
}

async function httpRequestPost(url = "", data = {}) {
    const response = await fetch(url, {
        method: "POST",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });
    return response.json();
}

async function httpRequestPatch(url = "", data = {}) {
    const response = await fetch(url, {
        method: "PATCH",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });
    return response.json();
}

function purchaseGoods(goodsInfo) {
    httpRequestPost("/customer/order", goodsInfo).then(data => {
        if (!data || !data.status || data.status != 200 || !data.data) {
            alert("Check Out Failed, Please Retry Later!");

            return;
        }

        document.getElementById("sidebar-menu-click-goods").click();
        hidePurchaseGoodsDialog();

        alert("Check Out Success!");
    });
}

function hidePurchaseGoodsDialog() {
    let purchaseGoodsDialog = document.getElementById("product-add-modal");

    purchaseGoodsDialog.innerHTML = "";
    purchaseGoodsDialog.className = "product-modal modal-hidden";
}

function showPurchaseGoodsDialog(merchantId, goodsId) {
    let goodsNode = document.getElementById("purchase-goodsId-" + goodsId).parentNode.parentNode;
    let goodsName = goodsNode.querySelector("td[goodsName]").getAttribute("goodsName");
    let goodsSKU = goodsNode.querySelector("td[goodsSKU]").getAttribute("goodsSKU");
    let goodsImage = goodsNode.querySelector("td[goodsImage]").getAttribute("goodsImage");
    let goodsDescription = goodsNode.querySelector("td[goodsDescription]").getAttribute("goodsDescription");
    let goodsPrice = goodsNode.querySelector("td[goodsPrice]").getAttribute("goodsPrice");
    let goodsQuantity = goodsNode.querySelector("td[goodsQuantity]").getAttribute("goodsQuantity");
    let purchaseGoodsDialog = document.getElementById("product-add-modal");

    if (parseInt(goodsQuantity) <= 0) {
        alert("Goods Out Of Stock, Please Retry Later!");

        return;
    }

    let purchaseDialogHtml = "<div class=\"product-modal-dialog\">\n" +
        "                <div class=\"product-modal-dialog-content\">\n" +
        "                    <div class=\"product-modal-dialog-header\">\n" +
        "                        <h4 class=\"product-modal-dialog-title\">Purchase Goods</h4>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Goods Name: </div>\n" +
        "                            <div class=\"product-dialog-item-content\">\n" +
        "                                <div class=\"product-dialog-item-info\">"  + goodsName + "</div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Goods SKU: </div>\n" +
        "                            <div class=\"product-dialog-item-content\">\n" +
        "                                <div class=\"product-dialog-item-info\">"  + goodsSKU + "</div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Goods Price</div>\n" +
        "                            <div class=\"product-dialog-item-content\">\n" +
        "                                <div class=\"product-dialog-item-info\">"  + goodsPrice + "</div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Product Quantity: </div>\n" +
        "                            <div class=\"product-dialog-item-content\">\n" +
        "                                <input type=\"number\" class=\"product-dialog-item-content-input\" name=\"goods-quantity\" placeholder=\"Product Quantity\" value=\"1\" min=\"1\" max=\"" + goodsQuantity +"\" required>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Goods Amount: </div>\n" +
        "                            <div class=\"product-dialog-item-content\">\n" +
        "                                <div id=\"purchase-dialog-goods-quantity\" class=\"product-dialog-item-info\">"  + goodsPrice + "</div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <div class=\"product-modal-dialog-body product-modal-dialog-body-last\">\n" +
        "                        <div class=\"product-dialog-item\">\n" +
        "                            <div class=\"product-dialog-item-name\">Remark: </div>\n" +
        "                            <div class=\"product-dialog-item-content product-dialog-item-content-remark\">\n" +
        "                                <textarea class=\"product-dialog-item-content-text\" name=\"purchase-remark\" placeholder=\"Remark\" maxlength=\"256\" required></textarea>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "\n" +
        "                    <hr>\n" +
        "\n" +
        "                    <div class=\"product-dialog-button\">\n" +
        "                        <div class=\"product-dialog-button-list\">\n" +
        "                            <button id=\"purchase-goods-dialog-button-ok\" class=\"product-dialog-button-ok\">Purchase</button>\n" +
        "                            <button id=\"purchase-goods-dialog-button-cancel\" class=\"product-dialog-button-cancel\">Cancel</button>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>";

    purchaseGoodsDialog.innerHTML = "";
    purchaseGoodsDialog.insertAdjacentHTML("beforeend", purchaseDialogHtml);

    let quantityBox = purchaseGoodsDialog.querySelector("input[name='goods-quantity']");
    let purchaseButton = purchaseGoodsDialog.querySelector("#purchase-goods-dialog-button-ok");
    let cancelButton = purchaseGoodsDialog.querySelector("#purchase-goods-dialog-button-cancel");

    quantityBox.addEventListener("change", (event) => {
        event.preventDefault();

        let purchaseQuantity = purchaseGoodsDialog.querySelector("input[name='goods-quantity']").value;
        let priceAmount = purchaseGoodsDialog.querySelector("#purchase-dialog-goods-quantity");

        goodsPrice = parseFloat(goodsPrice);
        goodsPrice = goodsPrice.toFixed(2);
        goodsPrice = parseFloat(goodsPrice);
        purchaseQuantity = parseInt(purchaseQuantity);
        priceAmount.innerHTML = (purchaseQuantity * goodsPrice).toFixed(2);
    });

    purchaseButton.addEventListener("click", (event) => {
        event.preventDefault();

        let purchaseQuantity = purchaseGoodsDialog.querySelector("input[name='goods-quantity']").value;
        let purchaseRemark = purchaseGoodsDialog.querySelector("textarea[name='purchase-remark']").value;

        if (purchaseRemark && purchaseRemark.trim().length != 0) {
            purchaseRemark = purchaseRemark.trim();
        } else {
            purchaseRemark = null;
        }

        purchaseQuantity = parseInt(purchaseQuantity);
        goodsPrice = parseFloat(goodsPrice);
        goodsPrice.toFixed(2);

        let goodsInfo = {
            "customerId": Customer.id,
            "merchantId": merchantId,
            "goodsId": goodsId,
            "goodsName": goodsName,
            "goodsSku": goodsSKU,
            "goodsImage": goodsImage,
            "goodsDescription": goodsDescription,
            "goodsQuantity": purchaseQuantity,
            "goodsPrice": goodsPrice,
            "remark": purchaseRemark,
        };

        purchaseGoods(goodsInfo);
    });

    cancelButton.addEventListener("click", (event) => {
        event.preventDefault();
        hidePurchaseGoodsDialog();
    });

    purchaseGoodsDialog.className = "product-modal";
}

function getGoodsList(pageNumber, pageSize) {
    httpRequestGet("/customer/goods?page=" + pageNumber + "&size=" + pageSize).then(data => {
        let goodsTableBody = document.querySelector("table#product-table > tbody")

        goodsTableBody.innerHTML = "";

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.list || data.data.list.length === 0) {
            return;
        }

        let goodsList = data.data.list;

        for (let i = 0; i < goodsList.length; i++) {
            let goods = goodsList[i];

            if (!goods) {
                continue;
            }

            let merchantId = goods.merchantId;
            let merchantName = goods.merchantName;
            let goodsId = goods.id;
            let goodsName = goods.productName;
            let goodsSKU = goods.productSku;
            let goodsImage = goods.productImage;
            let goodsDescription = goods.productDescription;
            let goodsQuantity = goods.productQuantity;
            let goodsPrice = goods.productPrice;
            let createTime = goods.createTime;
            let updateTime = goods.updateTime;

            goodsPrice = parseFloat(goodsPrice);
            goodsPrice = goodsPrice.toFixed(2);

            if (!updateTime) {
                updateTime = createTime;
            }

            let goodsInfo = "<tr>\n" +
                "    <td>" + merchantName + "</td>\n" +
                "    <td goodsName=\"" + goodsName + "\">" + goodsName + "</td>\n" +
                "    <td goodsSKU=\"" + goodsSKU + "\">" + goodsSKU + "</td>\n" +
                "    <td goodsImage=\"" + goodsImage + "\">\n" +
                "        <img height=\"150\" src=\"" + goodsImage + "\">\n" +
                "    </td>\n" +
                "    <td goodsDescription=\"" + goodsDescription + "\">" + goodsDescription + "</td>\n" +
                "    <td goodsPrice=\"" + goodsPrice + "\">" + goodsPrice + "</td>\n" +
                "    <td goodsQuantity=\"" + goodsQuantity + "\">" + goodsQuantity + "</td>\n" +
                "    <td>" + updateTime + "</td>\n" +
                "    <td>\n" +
                "        <button id=\"purchase-goodsId-" + goodsId + "\" class=\"purchase-product-button\">Purchase</button>\n" +
                "    </td>\n" +
                "</tr>";

            goodsTableBody.insertAdjacentHTML("beforeend", goodsInfo);

            let purchaseGoodsButton = goodsTableBody.querySelector("td > button#purchase-goodsId-" + goodsId);

            purchaseGoodsButton.addEventListener("click", (event) => {
                event.preventDefault();
                showPurchaseGoodsDialog(merchantId, goodsId);
            });
        }
    });
}

function getOrderList(pageNumber, pageSize) {
    httpRequestGet("/customer/order/user/" + Customer.id + "?page=" + pageNumber + "&size=" + pageSize).then(data => {
        let orderTableBody = document.querySelector("table#order-table > tbody")

        orderTableBody.innerHTML = "";

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.list || data.data.list.length === 0) {
            return;
        }

        let orderList = data.data.list;

        for (let i = 0; i < orderList.length; i++) {
            let order = orderList[i];

            if (!order) {
                continue;
            }

            let orderId = order.id;
            let goodsName = order.goodsName;
            let goodsSKU = order.goodsSku;
            let goodsImage = order.goodsImage;
            let goodsDescription = order.goodsDescription;
            let goodsQuantity = order.goodsQuantity;
            let goodsPrice = order.goodsPrice;
            let goodsAmount = order.goodsAmount;
            let remark = order.remark;
            let createTime = order.createTime;

            if (!remark) {
                remark = "";
            }

            goodsPrice = parseFloat(goodsPrice);
            goodsPrice = goodsPrice.toFixed(2);
            goodsAmount = parseFloat(goodsAmount);
            goodsAmount = goodsAmount.toFixed(2);

            let orderInfo = "<tr>\n" +
                "    <td>" + orderId + "</td>\n" +
                "    <td>" + goodsName + "</td>\n" +
                "    <td>" + goodsSKU + "</td>\n" +
                "    <td>\n" +
                "        <img height=\"150\" src=\"" + goodsImage + "\">\n" +
                "    </td>\n" +
                "    <td>" + goodsDescription + "</td>\n" +
                "    <td>" + goodsPrice + "</td>\n" +
                "    <td>" + goodsQuantity + "</td>\n" +
                "    <td>" + goodsAmount + "</td>\n" +
                "    <td>" + remark + "</td>\n" +
                "    <td>" + createTime + "</td>\n" +
                "</tr>";

            orderTableBody.insertAdjacentHTML("beforeend", orderInfo);
        }
    });
}

function getBalanceInfo() {
    httpRequestGet("/customer/credit/user/" + Customer.id).then(data => {
        if (!data || !data.status || data.status != 200) {
            if (data.status == 404) {
                document.querySelector("#account-balance-total").innerHTML = "0.00";
                document.querySelector("#account-balance-update-time").innerHTML = "";
            }

            return;
        }

        if (!data.data) {
            document.querySelector("#account-balance-total").innerHTML = "0.00";
            document.querySelector("#account-balance-update-time").innerHTML = "";
        } else {
            document.querySelector("#account-balance-total").innerHTML = data.data.balance.toFixed(2);
            document.querySelector("#account-balance-update-time").innerHTML = data.data.updateTime;
        }
    });
}

function updateAccountCredit() {
    let credit = document.querySelector("input#update-account-credit-value").value;

    credit = parseFloat(credit);
    credit = credit.toFixed(2);

    let creditInfo = {
        "customerId": Customer.id,
        "credit": credit
    };

    httpRequestPost("/customer/credit", creditInfo).then(data => {
        if (!data || !data.status || data.status != 200 || !data.data) {
            alert("Increase Credit Failed, Please Retry Failed!");

            return;
        }

        document.querySelector("#account-balance-total").innerHTML = data.data.balance.toFixed(2);
        document.querySelector("#account-balance-update-time").innerHTML = data.data.updateTime;

        alert("Increase Credit Success!");
    });
}

function logout() {
    const XHR = new XMLHttpRequest();

    XHR.addEventListener("load", (event) => {
        if (XHR.status == 200) {
            sessionStorage.removeItem("Customer");
            window.location.href = "signin.html";
        } else {
            alert("Logout Failed, Please Retry Failed!");

            return;
        }
    });

    XHR.addEventListener("error", (event) => {
        alert("Logout Failed, Please Retry Failed!");
    });

    XHR.open("POST", "/customer/logout");
    XHR.send();
}

function handleLeftMenuItemClick(current) {
    if (current == null) {
        return;
    }

    let nodes = document.querySelectorAll("ul.sidebar-menu-table > li");
    let areas = document.querySelectorAll("#right-content > .content-area-display");
    let targetAreaId = current.getAttribute("targetId");

    if (nodes == null || nodes.length === 0) {
        return;
    }

    for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        let area = areas[i];

        if (node == null) {
            continue;
        }

        node.className = "sidebar-menu-click";

        if (area) {
            area.className = "content-area-display content-area-hidden";
        }
    }

    current.className = "sidebar-menu-click active";

    if (targetAreaId && targetAreaId.trim().length > 0) {
        document.getElementById(targetAreaId).className = "content-area-display";
    }
}

function handleMerchantMenuClick() {
    let className = document.getElementById("customer-dropdown-menu-list").className;

    if (className.indexOf("customer-menu-dropdown-list-open") >= 0) {
        document.getElementById("customer-dropdown-menu-list").className = "customer-menu-dropdown-list";
    } else {
        document.getElementById("customer-dropdown-menu-list").className = "customer-menu-dropdown-list customer-menu-dropdown-list-open";
    }
}

window.addEventListener("load", () => {
    let customer = sessionStorage.getItem("Customer");

    if (customer != null && typeof customer === "string" && customer.trim().length > 0) {
        try {
            let json = JSON.parse(customer);

            if (json != null && typeof json === "object"
                && json.authen != null && typeof json.authen === "string" && json.authen.length > 0) {
                Customer = json;

                document.getElementById("customer-menu-display").innerHTML = "Welcome " + Customer.name;
            } else {
                alert("Please Signin / Signup, Then Retry!");

                window.location.href = "signin.html";
            }
        } catch (e) {
            alert("Please Signin / Signup, Then Retry!");

            window.location.href = "signin.html";
        }
    } else {
        alert("Please Signin / Signup, Then Retry!");

        window.location.href = "signin.html";
    }

    const productMenu = document.getElementById("sidebar-menu-click-goods");
    const salesMenu = document.getElementById("sidebar-menu-click-order");
    const settlementMenu = document.getElementById("sidebar-menu-click-balance");
    const updateCreditButton = document.getElementById("update-account-credit-button");
    const showCustomerMenuButton = document.getElementById("customer-menu-dropdown-button");
    const logoutButton = document.getElementById("customer-menu-dropdown-list-item-logout");

    productMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(productMenu.parentNode);
        getGoodsList(1, 1000);
    });

    salesMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(salesMenu.parentNode);
        getOrderList(1, 1000);
    });

    settlementMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(settlementMenu.parentNode);
        getBalanceInfo();
    });

    updateCreditButton.addEventListener("click", (event) => {
        event.preventDefault();
        updateAccountCredit();
    });

    showCustomerMenuButton.addEventListener("click", (event) => {
        event.preventDefault();
        handleMerchantMenuClick();
    });

    logoutButton.addEventListener("click", (event) => {
        event.preventDefault();
        logout();
    });

    document.addEventListener("click", (event) => {
        let node = event.target;
        let subNode = node.querySelector(".customer-menu-dropdown-title");

        if (!subNode) {
            let className = node.className;

            if (className == "customer-menu-dropdown-title-text" || className == "customer-menu-text" || className == "customer-menu-dropdown-caret") {
                return;
            } else {
                document.getElementById("customer-dropdown-menu-list").className = "customer-menu-dropdown-list";
            }
        }  else {
            let className = subNode.className;

            if (className == "customer-menu-dropdown-title-text" || className == "customer-menu-text" || className == "customer-menu-dropdown-caret") {
                return;
            } else {
                document.getElementById("customer-dropdown-menu-list").className = "customer-menu-dropdown-list";
            }
        }
    });

    productMenu.click();
});