window.addEventListener("load", () => {
    const signinForm = document.getElementById("form-signin");

    if (signinForm == null) {
        return;
    }

    function sendSigninData() {
        const XHR = new XMLHttpRequest();
        const FD = new FormData(signinForm);

        XHR.addEventListener("load", (event) => {
            let response = XHR.response;

            if (XHR.status == 200 && response != null && typeof response === "string" && response.length > 0) {
                try {
                    var responseJson = JSON.parse(response);

                    if (responseJson != null && typeof responseJson === "object"
                        && responseJson.status != null && typeof responseJson.status === "number" && responseJson.status == 200
                        && responseJson.data != null && typeof responseJson.data === "object") {
                        sessionStorage.setItem("Customer", JSON.stringify(responseJson.data));
                        window.location.href = "main.html";
                    } else {
                        alert("Signin Failed, Please Retry Later!");
                    }
                }
                catch (e) {
                    alert("Signin Failed, Please Retry Later!");
                }
            } else {
                alert("Signin Failed, Please Retry Later!");
            }
        });

        XHR.addEventListener("error", (event) => {
            alert("Signin Failed, Please Retry Later!");
        });

        XHR.open("POST", signinForm.action);
        XHR.send(FD);
    }

    signinForm.addEventListener("submit", (event) => {
        event.preventDefault();
        sendSigninData();
    });
});