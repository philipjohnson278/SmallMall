package org.example.smallmall.customer.client;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.smallmall.customer.client.controller.CustomerClientController;
import org.example.smallmall.customer.client.feign.CustomerOpenFeignClient;
import org.example.smallmall.customer.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.customer.client.feign.fallback.CustomerOpenFeignClientFallback;
import org.example.smallmall.customer.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.customer.request.CustomerAddCreditRequestVO;
import org.example.smallmall.vo.customer.request.CustomerPurchaseOrderRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

@SpringBootTest
@AutoConfigureMockMvc
public class CustomerClientControllerTests {
    private static final String responseJson = "{\"test\": \"Test\"}";

    @MockBean
    private CustomerOpenFeignClient customerOpenFeignClient;

    @MockBean
    private MerchantOpenFeignClient merchantOpenFeignClient;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetUserOrder() throws Exception {
        Mockito.when(customerOpenFeignClient.listOrder(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/1").param("page", "1").param("size", "0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/order/user/1").param("page", "1").param("size", "1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testPurchaseOrder() throws Exception {
        Mockito.when(customerOpenFeignClient.purchaseOrder(Mockito.any(CustomerPurchaseOrderRequestVO.class))).thenReturn(responseJson);

        ObjectMapper objectMapper = new ObjectMapper();
        CustomerPurchaseOrderRequestVO customerPurchaseOrderRequestVO =  new CustomerPurchaseOrderRequestVO();
        String requestJson = objectMapper.writeValueAsString(customerPurchaseOrderRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/order").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        customerPurchaseOrderRequestVO.setCustomerId(1);
        customerPurchaseOrderRequestVO.setMerchantId(1);
        customerPurchaseOrderRequestVO.setGoodsDescription("test");
        customerPurchaseOrderRequestVO.setGoodsId(1);
        customerPurchaseOrderRequestVO.setGoodsImage("test");
        customerPurchaseOrderRequestVO.setGoodsName("Test");
        customerPurchaseOrderRequestVO.setGoodsPrice(new BigDecimal("3.14"));
        customerPurchaseOrderRequestVO.setGoodsQuantity(0);
        customerPurchaseOrderRequestVO.setGoodsSku("test");
        customerPurchaseOrderRequestVO.setRemark("test");
        requestJson = objectMapper.writeValueAsString(customerPurchaseOrderRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        customerPurchaseOrderRequestVO.setGoodsQuantity(1);
        requestJson = objectMapper.writeValueAsString(customerPurchaseOrderRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetUserCredit() throws Exception {
        Mockito.when(customerOpenFeignClient.getCredit(Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/credit/user/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/credit/user/1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testIncreaseUserCredit() throws Exception {
        Mockito.when(customerOpenFeignClient.increaseCredit(Mockito.any(CustomerAddCreditRequestVO.class))).thenReturn(responseJson);

        ObjectMapper objectMapper = new ObjectMapper();
        CustomerAddCreditRequestVO customerAddCreditRequestVO = new CustomerAddCreditRequestVO();
        String requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        customerAddCreditRequestVO.setCredit(new BigDecimal("8.23"));
        customerAddCreditRequestVO.setCustomerId(0);
        requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        customerAddCreditRequestVO.setCustomerId(1);
        requestJson = objectMapper.writeValueAsString(customerAddCreditRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/credit").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetGoodsList() throws Exception {
        Mockito.when(merchantOpenFeignClient.getProduct(Mockito.anyInt(), Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/goods").param("page", "9999999999999999").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/goods").param("page", "1").param("size", "10000").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetGoodsById() throws Exception {
        Mockito.when(merchantOpenFeignClient.getProductbyId(Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/goods/9999999999999999").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/goods/1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testSentinelBlockHandler() {
        CustomerClientController customerClientController = new CustomerClientController();
        CustomerClientController customerClientControllerMock = Mockito.spy(customerClientController);
        BlockException blockException = new DegradeException("Test");

        ResponseVO responseVO = customerClientControllerMock.loginBlockHelper(null, null, null, null, blockException);
        Assertions.assertNotNull(responseVO);

        responseVO = customerClientControllerMock.registerBlockHelper(null, null, blockException);
        Assertions.assertNotNull(responseVO);

        String blockJson = customerClientControllerMock.getUserOrderBlockHelper(null, null, null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockJson = customerClientControllerMock.purchaseOrderBlockHelper(null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockJson = customerClientControllerMock.getUserCreditBlockHelper(null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockJson = customerClientControllerMock.increaseUserCreditBlockHelper(null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockJson = customerClientControllerMock.getGoodsListBlockHelper(null, null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockJson = customerClientControllerMock.getGoodsByIdBlockHelper(null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));

        blockException = new FlowException("Test");
        blockJson = customerClientControllerMock.getGoodsByIdBlockHelper(null, blockException);
        Assertions.assertTrue(StringUtils.isNotBlank(blockJson));
    }

    @Test
    public void testOpenFeignFallback() {
        CustomerOpenFeignClientFallback customerOpenFeignClientFallback = new CustomerOpenFeignClientFallback();
        MerchantOpenFeignClientFallback merchantOpenFeignClientFallback = new MerchantOpenFeignClientFallback();
        CustomerOpenFeignClient customerOpenFeignClientMock = Mockito.spy(customerOpenFeignClientFallback);
        MerchantOpenFeignClient merchantOpenFeignClientMock = Mockito.spy(merchantOpenFeignClientFallback);

        String fallbackJson = customerOpenFeignClientMock.listOrder(null, null, null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));

        fallbackJson = customerOpenFeignClientMock.purchaseOrder(null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));

        fallbackJson = customerOpenFeignClientMock.getCredit(null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));

        fallbackJson = customerOpenFeignClientMock.increaseCredit(null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));

        fallbackJson = merchantOpenFeignClientMock.getProduct(null, null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));

        fallbackJson = merchantOpenFeignClientMock.getProductbyId(null);
        Assertions.assertTrue(StringUtils.isNotBlank(fallbackJson));
    }
}