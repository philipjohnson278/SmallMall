package org.example.smallmall.customer.client;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.smallmall.customer.authenticate.entity.CustomerProfileEntity;
import org.example.smallmall.customer.authenticate.service.CustomerProfileService;
import org.example.smallmall.customer.client.service.CustomerAuthenticationService;
import org.example.smallmall.customer.client.service.CustomerDetailsService;
import org.example.smallmall.customer.client.service.impl.CustomerAuthenticationServiceImpl;
import org.example.smallmall.customer.client.service.impl.CustomerDetailsServiceImpl;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerClientAuthenticationTests {
    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private SecurityContextRepository securityContextRepository;

    @MockBean
    private SecurityContextHolderStrategy securityContextHolderStrategy;

    @MockBean
    private CustomerAuthenticationService customerAuthenticationService;

    @MockBean
    private CustomerProfileService customerProfileService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private SecurityContext securityContext = new SecurityContextImpl();

    @BeforeEach
    public void setup() {
        Mockito.doNothing().when(securityContextHolderStrategy).setContext(Mockito.any(SecurityContext.class));
        Mockito.doReturn(securityContext).when(securityContextHolderStrategy).getContext();
        Mockito.doReturn(securityContext).when(securityContextHolderStrategy).createEmptyContext();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testLogin() throws Exception {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken("test", "test");
        MockedStatic<SecurityContextHolder> securityContextHolderMockedStatic = Mockito.mockStatic(SecurityContextHolder.class);

        securityContextHolderMockedStatic.when(() -> {
            SecurityContextHolder.createEmptyContext();
        }).thenReturn(securityContext);
        securityContextHolderMockedStatic.when(() -> {
            SecurityContextHolder.getContext();
        }).thenReturn(securityContext);
        Mockito.doReturn(authenticationToken).when(authenticationManager).authenticate(authenticationToken);
        Mockito.doNothing().when(securityContextRepository).saveContext(Mockito.any(SecurityContext.class), Mockito.any(HttpServletRequest.class), Mockito.any(HttpServletResponse.class));

        CustomerProfileEntity customerProfile = new CustomerProfileEntity();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        customerProfile.setName("test");
        customerProfile.setSecret("test");
        commonLoginRequestVO.setName("test");
        commonLoginRequestVO.setSecret("test");
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        CustomerAuthenticationServiceImpl customerAuthenticationServiceImpl = new CustomerAuthenticationServiceImpl();
        ReflectionTestUtils.setField(customerAuthenticationServiceImpl, "customerProfileService", customerProfileService);
        CustomerAuthenticationService customerAuthenticationServiceMock = Mockito.spy(customerAuthenticationServiceImpl);

        Mockito.when(customerProfileService.getCustomer(Mockito.anyString())).thenReturn(null);
        CommonLoginResponseVO authenticationInfo = customerAuthenticationServiceMock.createAuthenticationInfo(commonLoginRequestVO);
        Assertions.assertNull(authenticationInfo);

        Mockito.when(customerProfileService.getCustomer(Mockito.anyString())).thenReturn(customerProfile);
        authenticationInfo = customerAuthenticationServiceMock.createAuthenticationInfo(commonLoginRequestVO);
        Assertions.assertNotNull(authenticationInfo);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/login").param("name", "test").param("secret", "")).andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.when(customerAuthenticationService.createAuthenticationInfo(Mockito.any(CommonLoginRequestVO.class))).thenReturn(authenticationInfo);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/login").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());

        securityContextHolderMockedStatic.clearInvocations();
        securityContextHolderMockedStatic.reset();
        securityContextHolderMockedStatic.close();
    }

    @Test
    public void testRegister() throws Exception {
        CustomerProfileEntity customerProfile = new CustomerProfileEntity();
        CommonLoginRequestVO loginRequestVO = new CommonLoginRequestVO();

        customerProfile.setName("test");
        customerProfile.setSecret("test");
        loginRequestVO.setName("test");
        loginRequestVO.setSecret("test");
        loginRequestVO.setRequestTime(LocalDateTime.now());

        Mockito.when(customerProfileService.getCustomer(Mockito.anyString())).thenReturn(customerProfile);
        Mockito.when(customerProfileService.customerRegistry(Mockito.any(CommonLoginRequestVO.class))).thenReturn(false);
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn("test");

        CustomerAuthenticationServiceImpl customerAuthenticationServiceImpl = new CustomerAuthenticationServiceImpl();
        ReflectionTestUtils.setField(customerAuthenticationServiceImpl, "customerProfileService", customerProfileService);
        ReflectionTestUtils.setField(customerAuthenticationServiceImpl, "passwordEncoder", passwordEncoder);
        CustomerAuthenticationService customerAuthenticationServiceMock = Mockito.spy(customerAuthenticationServiceImpl);

        CustomerDetailsServiceImpl customerDetailsServiceImpl = new CustomerDetailsServiceImpl();
        ReflectionTestUtils.setField(customerDetailsServiceImpl, "customerProfileService", customerProfileService);
        CustomerDetailsService customerDetailsServiceMock = Mockito.spy(customerDetailsServiceImpl);

        boolean registered = customerAuthenticationServiceMock.register(loginRequestVO);
        Assertions.assertFalse(registered);

        Mockito.when(customerProfileService.customerRegistry(Mockito.any(CommonLoginRequestVO.class))).thenReturn(true);
        registered = customerAuthenticationServiceMock.register(loginRequestVO);
        Assertions.assertTrue(registered);

        Mockito.when(customerProfileService.getCustomer(Mockito.anyString())).thenReturn(customerProfile);
        UserDetails userDetails = customerDetailsServiceMock.loadUserByUsername("test");
        Assertions.assertNotNull(userDetails);
        Assertions.assertNotNull(userDetails.getAuthorities());
        Assertions.assertNotNull(userDetails.getPassword());
        Assertions.assertNotNull(userDetails.getPassword());
        Assertions.assertFalse(userDetails.isEnabled());
        Assertions.assertTrue(userDetails.isAccountNonExpired());
        Assertions.assertTrue(userDetails.isAccountNonLocked());
        Assertions.assertTrue(userDetails.isCredentialsNonExpired());

        customerProfile.setState((byte) 1);
        Mockito.when(customerProfileService.getCustomer(Mockito.anyString())).thenReturn(customerProfile);
        userDetails = customerDetailsServiceMock.loadUserByUsername("test");
        Assertions.assertTrue(userDetails.isEnabled());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "")).andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.when(customerAuthenticationService.register(Mockito.any(CommonLoginRequestVO.class))).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.when(customerAuthenticationService.register(Mockito.any(CommonLoginRequestVO.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}