# SmallMall

## Description
A Small Mall, microservice style Spring Boot application.  
This project considerate as PoC for Spring Boot microservice, it includes two modules: merchant and customer, merchant module contains two services, provider and consumer, same as customer module.  
The tech stack:
1. GraalVM 17
2. MySQL
3. Mybatis Plus
4. Redis
5. Spring Boot
6. Spring Security
7. Spring Session
8. Spring Cloud Alibaba Nacos
9. Spring Cloud Alibaba Sentinel
10. Spring Cloud LoadBalancer
11. Spring Cloud OpenFeign
12. XXL Job

## Project Structure
Merchant:  
```
|-----------------------------------------------------------|         |-------------------------------------------|
| Client                                                    |         | Provider                                  |
|                                                           |         |                                           |
|    ------------              -------------                |         |       -------------    ----------------   |
|    | Security |               | OpenFeign |---------------|-------->|       | Scheduler |    | Mybatis Plus |   |
|    ------------               -------------               |         |       ------------     ----------------   |
|         |                          |                      |         |             |                  |          |
|    -----------     ------------    |    ----------------  |         |-------------|------------------|----------|
|    | Session |     | Sentinel |    |    | LoadBalancer |  |             |         |                  |
|    -----------     ------------    |    ----------------  |             |    -----------         ---------
|---------|----------------|---------|---------|------------|             |    | XXL Job |         | MySQL |
          |                |         |         |                          |    -----------         ---------
     ------------          |---------|---------|                          |
     |  Redis   |                ---------                                |
     ------------                | Nacos |--------------------------------|
                                 ---------
```
Customer:
```
|-----------------------------------------------------------|         |-------------------------|
| Client                                                    |         | Provider                |
|                                                           |         |                         |
|    ------------              -------------                |         |      ----------------   |
|    | Security |              | OpenFeign |----------------|-------->|      | Mybatis Plus |   |
|    ------------              -------------                |         |      ----------------   |
|         |                          |                      |         |              |          |
|    -----------     ------------    |    ----------------  |         |--------------|----------|
|    | Session |     | Sentinel |    |    | LoadBalancer |  |              |         |
|    -----------     ------------    |    ----------------  |              |      ---------
|---------|----------------|---------|--------|-------------|              |      | MySQL |
          |                |         |        |                            |      ---------
     ------------          |-------- |--------|                            |
     |  Redis   |                ---------                                 |
     ------------                | Nacos |---------------------------------|
                                 ---------
```

## Deployment
Pre-requirement:
1. JDK 17+, tested with GraalVM 17.0.10
2. A working MySQL server (tested with version 8.2.0), select an exists database (or create one) of MySQL, then using follow SQL script file to create tables:
```
Common/src/main/resources/mysql/SmallMall_MySQL_Modeling.sql
```
3. A working Nacos server (tested with version 2.3.0), import JSON configuration to Nacos Configuration Management from
```
Customer/CustomerClient/src/main/resources/lbrule/degraderule.json  --> data-id: customer-client-flow-rule.json
Customer/CustomerClient/src/main/resources/lbrule/flowrule.json     --> data-id: customer-client-degrade-rule.json
Merchant/MerchantClient/src/main/resources/lbrule/degraderule.json  --> data-id: merchant-client-flow-rule.json
Merchant/MerchantClient/src/main/resources/lbrule/flowrule.json     --> data-id: merchant-client-degrade-rule.json
```
4. A working Sentinel dashboard server (tested with version 1.8.7)

5. A working XXL Job server (tested with version 2.4.0), create an Executor with AppName *merchant-scheduler*, Title *Merchant*, then write down the executor's *groupId*
6. A working Redis server, tested with version 7.2.4

Now start deployment, first get the code and build Spring Boot application for deployment:

```
git clone https://gitlab.com/philipjohnson278/SmallMall.git
cd SmallMall
git ckeckout release
mvn clean package -Dmaven.test.skip=true
```

Next, copy follow *.tar.gz* packages to some directory:
```
Customer/CustomerClient/target/customer-client-0.0.1-SNAPSHOT-deploy.tar.gz
Customer/CustomerProvider/target/customer-provider-0.0.1-SNAPSHOT-deploy.tar.gz
Merchant/MerchantClient/target/merchant-client-0.0.1-SNAPSHOT-deploy.tar.gz
Merchant/MerchantProvider/target/merchant-provider-0.0.1-SNAPSHOT-deploy.tar.gz
```

Then unpack the *.tar.gz* packages, the follow commands will unpack a single application for deployment:
```
tar xf merchant-provider-0.0.1-SNAPSHOT-deploy.tar.gz
cd merchant-provider-0.0.1-SNAPSHOT
```

For additional configuration, such as MySQL connection settings, Redis connection settings, Nacos server settings, Sentinel server settings, XXL Job server settings (don't forget the *groupId* from Pre-requirement step 4), to do so, by editing Spring Boot's application configuration YAML file witch located in *config* directory under deploy directory. For example, here is an application directory structure:
```
merchant-provider-0.0.1-SNAPSHOT
|-- bin
|    |-- restart.sh
|    |-- start.sh
|    |-- stop.sh
|-- boot
|    |-- merchant-provider-0.0.1-SNAPSHOT.jar
|-- config
|    |-- application-common.yml
|    |-- application-merchant-scheduler.yml
|    |-- application.yml
|    |-- mapper
|         ...
|-- lib
|    ...
```

Finally, start the Spring Boot applications one by one, the follow steps for starting a single Spring Boot application:
```
sh bin/start.sh boot/merchant-provider-0.0.1-SNAPSHOT.jar
```

To stop the application:
```
sh bin/stop.sh
```

To restart the application:
```
sh bin/restart.sh boot/merchant-provider-0.0.1-SNAPSHOT.jar
```

## Usage
For merchant, visit *http://localhost:8084/merchant/page/signin.html* for sign in / sign up. \
For customer, visit *http://localhost:8084/customer/page/signin.html* for sign in / sign up.

## License
BSD-3-Clause license.
