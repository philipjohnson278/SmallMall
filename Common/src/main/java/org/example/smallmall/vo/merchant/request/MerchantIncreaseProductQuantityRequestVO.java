package org.example.smallmall.vo.merchant.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class MerchantIncreaseProductQuantityRequestVO implements Serializable {
    private static final long serialVersionUID = 316180947873961429L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer merchantId;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer productId;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer count;
}