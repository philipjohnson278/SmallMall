package org.example.smallmall.vo.customer.request;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CustomerPurchaseOrderRequestVO implements Serializable {
    private static final long serialVersionUID = 5861436039416412196L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer customerId;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer merchantId;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer goodsId;

    @NotBlank
    @Length(min = 1, max = 64)
    private String goodsName;

    @NotBlank
    @Length(min = 1, max = 16)
    private String goodsSku;

    @NotBlank
    @Length(min = 1, max = 2048)
    private String goodsImage;

    @NotBlank
    @Length(min = 1, max = 256)
    private String goodsDescription;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer goodsQuantity;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    @DecimalMin("0.01")
    @DecimalMax("99999999.99")
    private BigDecimal goodsPrice;

    @Length(min = 1, max = 200)
    private String remark;
}