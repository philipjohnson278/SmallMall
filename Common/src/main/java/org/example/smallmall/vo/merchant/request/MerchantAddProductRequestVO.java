package org.example.smallmall.vo.merchant.request;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MerchantAddProductRequestVO implements Serializable {
    private static final long serialVersionUID = -4433623904145083411L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer merchantId;

    @NotBlank
    @Length(min = 1, max = 64)
    private String productName;

    @NotBlank
    @Length(min = 1, max = 16)
    private String productSku;

    @NotBlank
    @Length(min = 1, max = 2048)
    private String productImage;

    @NotBlank
    @Length(min = 1, max = 256)
    private String productDescription;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer productQuantity;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    @DecimalMin("0.01")
    @DecimalMax("99999999.99")
    private BigDecimal productPrice;
}