package org.example.smallmall.vo.common.response;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

@Data
public class CommonLoginResponseVO implements Serializable {
    private static final long serialVersionUID = -3477551018479853073L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer id;

    @NotBlank
    @Length(min = 1, max = 16)
    private String name;

    @NotNull
    @Min(1)
    @Max(Byte.MAX_VALUE)
    private Byte state;

    @NotBlank
    private String authen;
}