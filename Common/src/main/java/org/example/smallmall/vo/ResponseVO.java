package org.example.smallmall.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class ResponseVO implements Serializable {
    private static final long serialVersionUID = 7548205938405676801L;

    private int status;
    private String message;
    private Map<String, Object> data;
}