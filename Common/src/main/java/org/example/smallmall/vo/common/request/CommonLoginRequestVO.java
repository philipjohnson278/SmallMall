package org.example.smallmall.vo.common.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CommonLoginRequestVO implements Serializable {
    private static final long serialVersionUID = -1724824651644856758L;

    @NotBlank
    @Length(min = 1, max = 16)
    private String name;

    @NotBlank
    @Length(min = 1, max = 128)
    private String secret;

    @NotNull
    private LocalDateTime requestTime;
}