package org.example.smallmall.vo.customer.request;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CustomerAddCreditRequestVO implements Serializable {
    private static final long serialVersionUID = -3007607902461317743L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer customerId;

    @NotNull
    @Digits(integer = 8, fraction = 2)
    @DecimalMin("0.01")
    @DecimalMax("99999999.99")
    private BigDecimal credit;
}