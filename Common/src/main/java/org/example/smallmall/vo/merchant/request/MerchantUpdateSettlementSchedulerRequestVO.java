package org.example.smallmall.vo.merchant.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class MerchantUpdateSettlementSchedulerRequestVO implements Serializable {
    private static final long serialVersionUID = -6180251567335341045L;

    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    Integer merchantId;

    @NotNull
    @Min(0)
    @Max(23)
    Integer hourOfDay;
}