CREATE TABLE `tbl_customer_balance`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int UNSIGNED NOT NULL,
  `balance` decimal(10, 2) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_customer_id`(`customer_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_customer_history`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int UNSIGNED NOT NULL,
  `operation` varchar(16) NOT NULL,
  `target_id` int UNSIGNED NOT NULL,
  `credit` decimal(10, 2) UNSIGNED NULL,
  `balance` decimal(10, 2) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_customer_id`(`customer_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_customer_order`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int UNSIGNED NOT NULL,
  `goods_id` int UNSIGNED NOT NULL,
  `goods_name` varchar(64) NOT NULL,
  `goods_sku` varchar(16) NOT NULL,
  `goods_image` varchar(2048) NOT NULL,
  `goods_description` varchar(256) NOT NULL,
  `goods_quantity` int UNSIGNED NOT NULL,
  `goods_price` decimal(10, 2) UNSIGNED NOT NULL,
  `goods_amount` decimal(10, 2) UNSIGNED NOT NULL,
  `state` tinyint UNSIGNED NOT NULL DEFAULT 1,
  `remark` varchar(255) NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_customer_id`(`customer_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_customer_profile`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `secret` varchar(128) NOT NULL,
  `state` tinyint UNSIGNED NOT NULL DEFAULT 1,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_name`(`name`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_merchant_balance`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int UNSIGNED NOT NULL,
  `balance` decimal(10, 2) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_merchant_id`(`merchant_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_merchant_history`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int UNSIGNED NOT NULL,
  `operation` varchar(16) NOT NULL,
  `product_id` int UNSIGNED NOT NULL,
  `product_name` varchar(64) NOT NULL,
  `product_sku` varchar(16) NOT NULL,
  `product_image` varchar(2048) NOT NULL,
  `product_description` varchar(256) NOT NULL,
  `product_quantity` int UNSIGNED NOT NULL,
  `product_price` decimal(10, 2) NOT NULL,
  `remark` varchar(255) NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_merchant_id`(`merchant_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_merchant_product`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int UNSIGNED NOT NULL,
  `product_name` varchar(64) NOT NULL,
  `product_sku` varchar(16) NOT NULL,
  `product_image` varchar(2048) NOT NULL,
  `product_description` varchar(256) NOT NULL,
  `product_quantity` int UNSIGNED NOT NULL,
  `product_price` decimal(10, 2) UNSIGNED NOT NULL,
  `state` tinyint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_merchant_sku`(`merchant_id`, `product_sku`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_merchant_profile`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `secret` varchar(128) NOT NULL,
  `state` tinyint UNSIGNED NOT NULL DEFAULT 1,
  `create_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_name`(`name`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_merchant_sales`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int UNSIGNED NOT NULL,
  `customer_id` int UNSIGNED NOT NULL,
  `product_id` int UNSIGNED NOT NULL,
  `product_name` varchar(64) NOT NULL,
  `product_sku` varchar(16) NOT NULL,
  `product_image` varchar(2048) NOT NULL,
  `product_description` varchar(256) NOT NULL,
  `product_quantity` int UNSIGNED NOT NULL,
  `product_price` decimal(10, 2) UNSIGNED NOT NULL,
  `product_amount` decimal(10, 2) UNSIGNED NOT NULL,
  `state` tinyint UNSIGNED NOT NULL DEFAULT 1,
  `remark` varchar(255) NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_merchant_id`(`merchant_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `tbl_scheduler_history`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int UNSIGNED NOT NULL,
  `balance` decimal(10, 2) UNSIGNED NOT NULL,
  `sales_amount` decimal(10, 2) UNSIGNED NOT NULL,
  `matched` bit NOT NULL DEFAULT 0,
  `create_time` datetime(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_merchant_id`(`merchant_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs;

DROP PROCEDURE IF EXISTS procedure_customer_purchase_order
;

CREATE PROCEDURE procedure_customer_purchase_order(
    IN merchantId INT,
    IN customerId INT,
    IN goodsId INT,
    IN goodsName VARCHAR(64),
    IN goodsSku VARCHAR(16),
    IN goodsImage VARCHAR(2048),
    IN goodsDescription VARCHAR(256),
    IN goodsQuantity INT,
    IN goodsPrice DECIMAL(10, 2),
    IN remark VARCHAR(256),
    IN createTime DATETIME,
    OUT orderId INT
)
BEGIN
    DECLARE t_error INTEGER DEFAULT 0
    ;

    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET t_error = 1
    ;

    START TRANSACTION
    ;

        UPDATE tbl_customer_balance
            LEFT JOIN tbl_customer_profile
            ON tbl_customer_balance.customer_id = tbl_customer_profile.id
        SET tbl_customer_balance.balance = tbl_customer_balance.balance - (goodsQuantity * goodsPrice),
            tbl_customer_balance.update_time = createTime
        WHERE tbl_customer_balance.customer_id = customerId
          AND tbl_customer_balance.balance - (goodsQuantity * goodsPrice) >= 0
          AND tbl_customer_profile.state = 1
          AND (SELECT tbl_merchant_product.product_quantity FROM tbl_merchant_product WHERE tbl_merchant_product.id = goodsId) >= goodsQuantity
        ;

        IF ROW_COUNT() = 1 AND t_error = 0 THEN
            UPDATE tbl_merchant_product
            SET tbl_merchant_product.product_quantity = tbl_merchant_product.product_quantity - goodsQuantity,
                tbl_merchant_product.update_time = createTime
            WHERE tbl_merchant_product.id = goodsId
              AND tbl_merchant_product.product_quantity >= goodsQuantity
            ;

            IF ROW_COUNT() = 1 AND t_error = 0 THEN
                SET @found = (SELECT tbl_merchant_balance.merchant_id FROM tbl_merchant_balance WHERE tbl_merchant_balance.merchant_id = merchantId)
                ;

                IF @found > 0 THEN
                    UPDATE tbl_merchant_balance
                        LEFT JOIN tbl_merchant_profile
                        ON tbl_merchant_balance.merchant_id = tbl_merchant_profile.id
                    SET tbl_merchant_balance.balance = tbl_merchant_balance.balance + (goodsQuantity * goodsPrice),
                        tbl_merchant_balance.update_time = createTime
                    WHERE tbl_merchant_balance.merchant_id = merchantId
                      AND tbl_merchant_profile.state = 1
                    ;
                ELSE
                    INSERT INTO tbl_merchant_balance (
                        merchant_id,
                        balance,
                        create_time
                    )
                    VALUES (
                        merchantId,
                        (goodsQuantity * goodsPrice),
                        createTime
                    )
                    ;
                END IF
                ;

                IF ROW_COUNT() = 1 AND t_error = 0 THEN
                    INSERT INTO tbl_merchant_sales (
                        merchant_id,
                        customer_id,
                        product_id,
                        product_name,
                        product_sku,
                        product_image,
                        product_description,
                        product_quantity,
                        product_price,
                        product_amount,
                        state,
                        remark,
                        create_time
                    )
                    VALUES (
                        merchantId,
                        customerId,
                        goodsId,
                        goodsName,
                        goodsSku,
                        goodsImage,
                        goodsDescription,
                        goodsQuantity,
                        goodsPrice,
                        (goodsQuantity * goodsPrice),
                        1,
                        remark,
                        createTime
                    )
                    ;

                    IF ROW_COUNT() = 1 AND t_error = 0 THEN
                        INSERT INTO  tbl_customer_order (
                            customer_id,
                            goods_id,
                            goods_name,
                            goods_sku,
                            goods_image,
                            goods_description,
                            goods_quantity,
                            goods_price,
                            goods_amount,
                            state,
                            remark,
                            create_time
                        )
                        VALUES (
                            customerId,
                            goodsId,
                            goodsName,
                            goodsSku,
                            goodsImage,
                            goodsDescription,
                            goodsQuantity,
                            goodsPrice,
                            (goodsQuantity * goodsPrice),
                            1,
                            remark,
                            createTime
                        )
                        ;

                        IF ROW_COUNT() = 0 OR t_error = 1 THEN
                            ROLLBACK
                            ;
                        ELSE
                            SET orderId = (SELECT LAST_INSERT_ID())
                            ;
                        END IF
                        ;
                    ELSE
                        ROLLBACK
                        ;
                    END IF
                    ;
                ELSE
                    ROLLBACK
                    ;
                END IF
                ;
            ELSE
                ROLLBACK
                ;
            END IF
            ;
        ELSE
            ROLLBACK
            ;
        END IF
        ;

    COMMIT
    ;

END
;