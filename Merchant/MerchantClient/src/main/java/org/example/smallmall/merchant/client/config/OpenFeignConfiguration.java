package org.example.smallmall.merchant.client.config;

import org.example.smallmall.merchant.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.merchant.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.springframework.context.annotation.Bean;

public class OpenFeignConfiguration {
    @Bean
    public MerchantOpenFeignClient merchantOpenClientFallback() {
        return new MerchantOpenFeignClientFallback();
    }
}