package org.example.smallmall.merchant.client.feign;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.merchant.client.config.OpenFeignConfiguration;
import org.example.smallmall.merchant.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service-provider.service-name.merchant}", path = "${service-provider.context-path.merchant}", fallback = MerchantOpenFeignClientFallback.class, configuration = OpenFeignConfiguration.class)
public interface MerchantOpenFeignClient {
    @GetMapping("/sales/merchant/{merchantId}")
    public String getSales(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                           @RequestParam(name = "page", required = false) Integer pageNumber,
                           @RequestParam(name = "size", required = false) Integer pageSize);

    @GetMapping("/balance/merchant/{merchantId}")
    public String getBalance(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId);

    @GetMapping("/product/merchant/{merchantId}")
    public String getProduct(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                             @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                             @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size);

    @PatchMapping("/product/{productId}")
    public String increaseQuantity(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("productId") Integer productId,
                                   @NotNull @RequestBody MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO);

    @PostMapping("/product")
    public String addProduct(@NotNull @RequestBody MerchantAddProductRequestVO addProductRequestVO);

    @GetMapping("/scheduler/settlement/{merchantId}")
    public String getSettlement(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                                @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size);

    @PostMapping("/scheduler/settlement/{merchantId}")
    public String runSettlementSchedule(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId);

    @PatchMapping("/scheduler/settlement")
    public String updateSettlementSchedule(@NotNull @RequestBody MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO);

    @PostMapping("/scheduler/settlement")
    public String createSettlementSchedule(@NotNull @RequestBody MerchantUpdateSettlementSchedulerRequestVO createSettlementSchedulerRequestVO);
}