package org.example.smallmall.merchant.client.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.authenticate.service.MerchantProfileService;
import org.example.smallmall.merchant.client.service.MerchantDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class MerchantDetailsServiceImpl implements MerchantDetailsService {
    @Autowired
    private MerchantProfileService merchantProfileService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MerchantProfileEntity merchantProfile = this.getMerchantByName(username);

        if (merchantProfile == null) {
            log.error("Can not find merchant: {}", username);

            throw new UsernameNotFoundException("Merchant " + username + " not found!");
        }

        UserDetails userDetails = new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList("ROLE_MERCHANT");

                return authorityList;
            }

            @Override
            public String getPassword() {
                return merchantProfile.getSecret();
            }

            @Override
            public String getUsername() {
                return merchantProfile.getName();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                if (merchantProfile.getState() == 1) {
                    return true;
                }

                return false;
            }
        };

        return userDetails;
    }

    @Override
    public MerchantProfileEntity getMerchantByName(String name) {
        MerchantProfileEntity merchantProfile = merchantProfileService.getMerchant(name);

        if (merchantProfile == null) {
            log.error("No merchant found for name: {}", name);
        }

        return merchantProfile;
    }
}