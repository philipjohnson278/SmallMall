package org.example.smallmall.merchant.client.loadbalancer;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.stereotype.Component;

@Component
@LoadBalancerClient(value = "merchant-provider", configuration = MerchantProviderLoadBalancer.class)
public class MerchantLoadBalancerClient {
}