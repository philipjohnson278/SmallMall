package org.example.smallmall.merchant.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.HeaderWriterLogoutHandler;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.header.writers.ClearSiteDataHeaderWriter;

import static org.springframework.security.web.header.writers.ClearSiteDataHeaderWriter.Directive.COOKIES;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = false)
public class WebMVCSecurityConfiguration {
    private static final String[] URL_WHITELIST = {"/login", "/logout", "/register", "/actuator", "/actuator/**", "/page/**", "/favicon.ico"};

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityContextRepository securityContextRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf(csrf -> {csrf.disable();})
                .authorizeHttpRequests(resp -> {
                    resp.requestMatchers(URL_WHITELIST)
                            .permitAll()
                            .anyRequest()
                            .authenticated();
                })
                .formLogin(form -> {
                    form.permitAll();
                    form.loginPage("/page/signin.html");
                    form.usernameParameter("name");
                    form.passwordParameter("secret");
                })
                .logout(logout -> {
                    logout.clearAuthentication(true);
                    logout.invalidateHttpSession(true);
                    logout.logoutSuccessUrl("/page/signin.html");
                    logout.addLogoutHandler(new HeaderWriterLogoutHandler(new ClearSiteDataHeaderWriter(COOKIES)));
                    logout.deleteCookies("JSESSIONID");
                })
                .sessionManagement(session -> {
                    session.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
                    session.maximumSessions(1)
                            .expiredUrl("/page/signin.html")
                            .maxSessionsPreventsLogin(true);
                    session.sessionFixation().migrateSession();
                    session.invalidSessionUrl("/page/signin.html");
                })
                .securityContext(context -> {
                    context.securityContextRepository(securityContextRepository);
                })
                .userDetailsService(userDetailsService);

        DefaultSecurityFilterChain webSecurityFilterChain = http.build();

        return webSecurityFilterChain;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();

        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        return authenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}