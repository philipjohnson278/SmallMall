package org.example.smallmall.merchant.client.feign.fallback;

import org.example.smallmall.merchant.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;

public class MerchantOpenFeignClientFallback implements MerchantOpenFeignClient {
    @Override
    public String getSales(Integer merchantId, Integer pageNumber, Integer pageSize) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String getBalance(Integer merchantId) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String getProduct(Integer merchantId, Integer page, Integer size) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String increaseQuantity(Integer productId, MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Update product detail failed!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String addProduct(MerchantAddProductRequestVO addProductRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Add a new product failed!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String getSettlement(Integer merchantId, Integer page, Integer size) {
        String fallbackJson = "{\"status\": 404, \"message\": \"No query result found!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String runSettlementSchedule(Integer merchantId) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Run settlement job failed!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String updateSettlementSchedule(MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Update settlement job failed!\", \"data\": null}";

        return fallbackJson;
    }

    @Override
    public String createSettlementSchedule(MerchantUpdateSettlementSchedulerRequestVO createSettlementSchedulerRequestVO) {
        String fallbackJson = "{\"status\": 503, \"message\": \"Create settlement job for merchant failed!\", \"data\": null}";

        return fallbackJson;
    }
}