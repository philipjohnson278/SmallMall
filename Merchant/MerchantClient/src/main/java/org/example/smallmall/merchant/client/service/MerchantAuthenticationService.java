package org.example.smallmall.merchant.client.service;

import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;

public interface MerchantAuthenticationService {
    public CommonLoginResponseVO createAuthenticationInfo(CommonLoginRequestVO loginRequestVO);
    public boolean register(CommonLoginRequestVO loginRequestVO);
}