package org.example.smallmall.merchant.client.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.merchant.client.service.MerchantAuthenticationService;
import org.example.smallmall.merchant.client.service.MerchantDetailsService;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
public class MerchantClientController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private SecurityContextRepository securityContextRepository;

    @Autowired
    private SecurityContextHolderStrategy securityContextHolderStrategy;

    @Autowired
    private MerchantAuthenticationService merchantAuthenticationService;

    @Autowired
    private MerchantDetailsService merchantDetailsService;

    @Autowired
    private MerchantOpenFeignClient merchantOpenFeignClient;

    @GetMapping(value = {"/", "/page", "/page/"}, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity<String> index() {
        String hello = "<html><head><title>Merchant Application</title></head><body><h1>Merchant Application</h1></body></html>";

        return new ResponseEntity<>(hello, HttpStatus.OK);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "login", blockHandler = "loginBlockHelper")
    public ResponseVO login(@NotBlank @RequestParam("name") String userName, @NotBlank @RequestParam("secret") String password,
                            HttpServletRequest request, HttpServletResponse response) {
        ResponseVO responseVO = new ResponseVO();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName(userName);
        commonLoginRequestVO.setSecret(password);
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        context.setAuthentication(authentication);
        securityContextHolderStrategy.setContext(context);
        securityContextRepository.saveContext(context, request, response);

        CommonLoginResponseVO commonLoginResponseVO = merchantAuthenticationService.createAuthenticationInfo(commonLoginRequestVO);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> resultMap = objectMapper.convertValue(commonLoginResponseVO, Map.class);

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage("Login success!");
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "register", blockHandler = "registerBlockHelper")
    public ResponseVO register(@NotBlank @RequestParam("name") String userName, @NotBlank @RequestParam("secret") String password) {
        ResponseVO responseVO = new ResponseVO();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName(userName);
        commonLoginRequestVO.setSecret(password);
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        boolean registered = merchantAuthenticationService.register(commonLoginRequestVO);

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.name());

        if (registered) {
            Map<String, Object> resultMap = new HashMap<>();
            MerchantProfileEntity merchantProfile = merchantDetailsService.getMerchantByName(userName);

            if (merchantProfile != null) {
                MerchantUpdateSettlementSchedulerRequestVO createSchedulerRequestVO = new MerchantUpdateSettlementSchedulerRequestVO();

                createSchedulerRequestVO.setMerchantId(merchantProfile.getId());
                createSchedulerRequestVO.setHourOfDay(LocalDateTime.now().getHour());
                merchantOpenFeignClient.createSettlementSchedule(createSchedulerRequestVO);
            }

            resultMap.put("result", registered);
            responseVO.setData(resultMap);
        }

        return responseVO;
    }

    @GetMapping(value = "/balance/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getMerchantBalance", blockHandler = "getMerchantBalanceBlockHelper")
    public String getMerchantBalance(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId) {
        String balanceJson = merchantOpenFeignClient.getBalance(merchantId);

        return balanceJson;
    }

    @GetMapping(value = "/product/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getMerchantProduct", blockHandler = "getMerchantProductBlockHelper")
    public String getMerchantProduct(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                             @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer pageNumber,
                             @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer pageSize) {
        String productJson = merchantOpenFeignClient.getProduct(merchantId, pageNumber, pageSize);

        return productJson;
    }

    @PatchMapping(value = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "increaseProductQuantity", blockHandler = "increaseProductQuantityBlockHelper")
    public String increaseProductQuantity(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("productId") Integer productId,
                                          @Valid @NotNull @RequestBody MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO) {
        String resultJson = merchantOpenFeignClient.increaseQuantity(productId, increaseProductQuantityRequestVO);

        return resultJson;
    }

    @PostMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "addProduct", blockHandler = "addProductBlockHelper")
    public String addProduct(@Valid @NotNull @RequestBody MerchantAddProductRequestVO addProductRequestVO) {
        String resultJson = merchantOpenFeignClient.addProduct(addProductRequestVO);

        return resultJson;
    }

    @GetMapping(value = "/sales/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getMerchantSales", blockHandler = "getMerchantSalesBlockHelper")
    public String getMerchantSales(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer pageNumber,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer pageSize) {
        String salesJson = merchantOpenFeignClient.getSales(merchantId, pageNumber, pageSize);

        return salesJson;
    }

    @GetMapping(value = "/scheduler/settlement/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "getMerchantSettlement", blockHandler = "getMerchantSettlementBlockHelper")
    public String getMerchantSettlement(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                                        @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer pageNumber,
                                        @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer pageSize) {
        String settlementJson = merchantOpenFeignClient.getSettlement(merchantId, pageNumber, pageSize);

        return settlementJson;
    }

    @PostMapping(value = "/scheduler/settlement/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "runMerchantSettlementScheduler", blockHandler = "runMerchantSettlementSchedulerBlockHelper")
    public String runMerchantSettlementScheduler(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId) {
        String resultJson = merchantOpenFeignClient.runSettlementSchedule(merchantId);

        return resultJson;
    }

    @PatchMapping(value = "/scheduler/settlement", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @LoadBalanced
    @SentinelResource(value = "updateMerchantSettlementScheduler", blockHandler = "updateMerchantSettlementSchedulerBlockHelper")
    public String updateMerchantSettlementScheduler(@Valid @NotNull @RequestBody MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO) {
        String resultJson = merchantOpenFeignClient.updateSettlementSchedule(updateSettlementSchedulerRequestVO);

        return resultJson;
    }

    public ResponseVO loginBlockHelper(String userName, String password, HttpServletRequest request, HttpServletResponse response, BlockException exception) {
        ResponseVO responseVO = new ResponseVO();

        responseVO.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
        responseVO.setMessage("Login failed!");

        return responseVO;
    }

    public ResponseVO registerBlockHelper(String userName, String password, BlockException exception) {
        ResponseVO responseVO = new ResponseVO();

        responseVO.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
        responseVO.setMessage("Registry failed!");

        return responseVO;
    }

    public String getMerchantBalanceBlockHelper(Integer merchantId, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getMerchantProductBlockHelper(Integer merchantId, Integer pageNumber, Integer pageSize, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String increaseProductQuantityBlockHelper(Integer productId, MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String addProductBlockHelper(MerchantAddProductRequestVO addProductRequestVO, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getMerchantSalesBlockHelper(Integer merchantId, Integer pageNumber, Integer pageSize, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String getMerchantSettlementBlockHelper(Integer merchantId, Integer pageNumber, Integer pageSize, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String runMerchantSettlementSchedulerBlockHelper(Integer merchantId, BlockException exception) {
        return requestBlockHelper(exception);
    }

    public String updateMerchantSettlementSchedulerBlockHelper(MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO, BlockException exception) {
        return requestBlockHelper(exception);
    }

    private String requestBlockHelper(BlockException exception) {
        String blockHelperJson;

        if (exception instanceof DegradeException) {
            blockHelperJson = "{\"status\": 503, \"message\": \"Service down graded!\", \"data\": null}";
        } else {
            blockHelperJson = "{\"status\": 404, \"message\": \"Service out of range!\", \"data\": null}";
        }

        return blockHelperJson;
    }
}