package org.example.smallmall.merchant.client.service;

import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface MerchantDetailsService extends UserDetailsService {
    public MerchantProfileEntity getMerchantByName(String name);
}