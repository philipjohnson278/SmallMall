package org.example.smallmall.merchant.client.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.authenticate.service.MerchantProfileService;
import org.example.smallmall.merchant.client.service.MerchantAuthenticationService;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@Service
public class MerchantAuthenticationServiceImpl implements MerchantAuthenticationService {
    @Autowired
    private MerchantProfileService merchantProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public CommonLoginResponseVO createAuthenticationInfo(CommonLoginRequestVO loginRequestVO) {
        MerchantProfileEntity merchantProfile = merchantProfileService.getMerchant(loginRequestVO.getName());

        if (merchantProfile == null) {
            return null;
        }

        String authentication = loginRequestVO.getName() + ":" + loginRequestVO.getSecret();
        CommonLoginResponseVO loginResponseVO = new CommonLoginResponseVO();

        loginResponseVO.setId(merchantProfile.getId());
        loginResponseVO.setName(merchantProfile.getName());
        loginResponseVO.setState(merchantProfile.getState());
        loginResponseVO.setAuthen(Base64.getEncoder().encodeToString(authentication.getBytes(StandardCharsets.UTF_8)));

        return loginResponseVO;
    }

    @Override
    public boolean register(CommonLoginRequestVO loginRequestVO) {
        String password = loginRequestVO.getSecret();
        String secret = passwordEncoder.encode(password);

        loginRequestVO.setSecret(secret);

        boolean registered = merchantProfileService.merchantRegistry(loginRequestVO);

        return registered;
    }
}