window.addEventListener("load", () => {
    const signupForm = document.getElementById("form-signup");

    if (signupForm == null) {
        return;
    }

    function sendSignupData() {
        const XHR = new XMLHttpRequest();
        const FD = new FormData(signupForm);

        XHR.addEventListener("load", (event) => {
            let response = XHR.response;

            if (XHR.status == 200 && response != null && typeof response === "string" && response.length > 0) {
                try {
                    let responseJson = JSON.parse(response);

                    if (responseJson != null && typeof responseJson === "object"
                        && responseJson.status != null && typeof responseJson.status === "number" && responseJson.status == 200
                        && responseJson.data != null && typeof responseJson.data === "object") {
                        document.getElementById("check-password-tips").style.display = "none";
                        alert("Signup Success, Go To Signin Page ...");
                        window.location.href = "signin.html";
                    } else {
                        document.getElementById("check-password-tips").textContent = "Signup Failed, Please Retry Later!"
                        document.getElementById("check-password-tips").style.display = "inherit";
                    }
                } catch (e) {
                    document.getElementById("check-password-tips").textContent = "Signup Failed, Please Retry Later!"
                    document.getElementById("check-password-tips").style.display = "inherit";
                }
            } else {
                document.getElementById("check-password-tips").textContent = "Signup Failed, Please Retry Later!"
                document.getElementById("check-password-tips").style.display = "inherit";
            }
        });

        XHR.addEventListener("error", (event) => {
            ocument.getElementById("check-password-tips").textContent = "Signup Failed, Please Retry Later!"
            document.getElementById("check-password-tips").style.display = "inherit";
        });

        XHR.open("POST", signupForm.action);
        XHR.send(FD);
    }

    signupForm.addEventListener("submit", (event) => {
        event.preventDefault();
        sendSignupData();
    });
});

function checkPassword() {
    let password = document.getElementById("password").value;
    let confirm = document.getElementById("password-confirm").value;

    if (confirm == null || (confirm != null && confirm.length === 0) || password == confirm) {
        document.getElementById("check-password-tips").style.display = "none";
    } else {
        document.getElementById("check-password-tips").textContent = "Password Not Match!";
        document.getElementById("check-password-tips").style.display = "inherit";
    }
}