var Merchant;

async function httpRequestGet(url = "") {
    const response = await fetch(url, {
        method: "GET",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
    });
    return response.json();
}

async function httpRequestPost(url = "", data = {}) {
    const response = await fetch(url, {
        method: "POST",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });
    return response.json();
}

async function httpRequestPatch(url = "", data = {}) {
    const response = await fetch(url, {
        method: "PATCH",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json; charset=UTF-8",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });
    return response.json();
}

function increaseProductQuantity(quantityNode, productId, quantityOld, quantity) {
    let productData = {
        "merchantId": Merchant.id,
        "productId": productId,
        "count": quantity
    };

    httpRequestPatch("/merchant/product/" + productId, productData).then(data => {
        if (!data || !data.status || data.status != 200 || !data.data || !data.data.result) {
            quantityNode.value = quantityOld

            alert("Update Quantity Failed, Please Retry later!");

            return;
        }

        alert("Update Quantity Success!");
    });
}

function getProductList(merchantId, pageNumber, pageSize) {
    httpRequestGet("/merchant/product/merchant/" + merchantId + "?page=" + pageNumber + "&size=" + pageSize).then(data => {
        let productTableBody = document.querySelector("table#product-table > tbody")

        productTableBody.innerHTML = "";

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.list || data.data.list.length === 0) {
            return;
        }

        let productList = data.data.list;

        for (let i = 0; i < productList.length; i++) {
            let product = productList[i];

            if (!product) {
                continue;
            }

            let productId = product.id;
            let productName = product.productName;
            let productSKU = product.productSku;
            let productImage = product.productImage;
            let productDescription = product.productDescription;
            let productQuantity = product.productQuantity;
            let productPrice = product.productPrice;
            let status = product.state;
            let createTime = product.createTime;
            let updateTime = product.updateTime;

            productPrice = parseFloat(productPrice);
            productPrice = productPrice.toFixed(2);

            if (status === 1) {
                if (productQuantity == 0) {
                    status = "WARN";
                } else {
                    status = "OK";
                }
            } else {
                status = "ERROR";
            }

            if (!updateTime) {
                updateTime = "";
            }

            let productInfo = "<tr>\n" +
                "    <td>" + productId + "</td>\n" +
                "    <td>" + productName + "</td>\n" +
                "    <td>" + productSKU + "</td>\n" +
                "    <td>\n" +
                "        <img height=\"150\" src=\"" + productImage + "\">\n" +
                "    </td>\n" +
                "    <td>" + productDescription + "</td>\n" +
                "    <td>" + productPrice + "</td>\n" +
                "    <td>\n" +
                "        <input class=\"product-table-column-quantity-input\" type=\"text\" value=\"" + productQuantity + "\">\n" +
                "        <button id=\"productId-" + productId + "\" class=\"product-table-column-quantity-button\">Update</button>\n" +
                "    </td>\n" +
                "    <td>" + status + "</td>\n" +
                "    <td>" + createTime + "</td>\n" +
                "    <td>" + updateTime + "</td>\n" +
                "</tr>";

            productTableBody.insertAdjacentHTML("beforeend", productInfo);

            let updateQuantityButton = productTableBody.querySelector("td > button#productId-" + productId);

            updateQuantityButton.addEventListener("click", (event) => {
                event.preventDefault();

                let quantityNode = updateQuantityButton.parentNode.querySelector("input.product-table-column-quantity-input");
                let count = parseInt(quantityNode.value);

                if (count <= parseInt(productQuantity)) {
                    quantityNode.value = productQuantity;

                    alert("Please Check Product Quantity!");

                    return;
                }

                increaseProductQuantity(quantityNode, productId, productQuantity, count);
            });
        }
    });
}

function getSalesList(merchantId, pageNumber, pageSize) {
    httpRequestGet("/merchant/sales/merchant/" + merchantId + "?page=" + pageNumber + "&size=" + pageSize).then(data => {
        let salesTableBody = document.querySelector("table#sales-table > tbody")

        salesTableBody.innerHTML = "";

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.list || data.data.list.length === 0) {
            return;
        }

        let salesList = data.data.list;

        for (let i = 0; i < salesList.length; i++) {
            let sales = salesList[i];

            if (!sales) {
                continue;
            }

            let salesId = sales.id;
            let productName = sales.productName;
            let productSKU = sales.productSku;
            let productImage = sales.productImage;
            let productDescription = sales.productDescription;
            let productQuantity = sales.productQuantity;
            let productPrice = sales.productPrice;
            let productAmount = sales.productAmount;
            let remark = sales.remark;
            let createTime = sales.createTime;

            if (!remark) {
                remark = "";
            }

            productPrice = parseFloat(productPrice);
            productPrice = productPrice.toFixed(2);
            productAmount = parseFloat(productAmount);
            productAmount = productAmount.toFixed(2);

            let salesInfo = "<tr>\n" +
                "    <td>" + salesId + "</td>\n" +
                "    <td>" + productName + "</td>\n" +
                "    <td>" + productSKU + "</td>\n" +
                "    <td>\n" +
                "        <img height=\"150\" src=\"" + productImage + "\">\n" +
                "    </td>\n" +
                "    <td>" + productDescription + "</td>\n" +
                "    <td>" + productPrice + "</td>\n" +
                "    <td>" + productQuantity + "</td>\n" +
                "    <td>" + productAmount + "</td>\n" +
                "    <td>" + remark + "</td>\n" +
                "    <td>" + createTime + "</td>\n" +
                "</tr>";

            salesTableBody.insertAdjacentHTML("beforeend", salesInfo);
        }
    });
}

function getBalanceInfo(merchantId) {
    httpRequestGet("/merchant/balance/merchant/" + merchantId).then(data => {
        if (!data || !data.status || data.status != 200) {
            if (data.status == 404) {
                document.querySelector("#account-balance-total").innerHTML = "0.00";
                document.querySelector("#account-balance-update-time").innerHTML = "";
            }

            return;
        }

        if (!data.data) {
            document.querySelector("#account-balance-total").innerHTML = "0.00";
            document.querySelector("#account-balance-update-time").innerHTML = "";
        } else {
            document.querySelector("#account-balance-total").innerHTML = data.data.balance.toFixed(2);
            document.querySelector("#account-balance-update-time").innerHTML = data.data.updateTime;
        }
    });
}

function getSettlement(merchantId, pageNumber, pageSize) {
    httpRequestGet("/merchant/scheduler/settlement/" + merchantId + "?page=" + pageNumber + "&size=" + pageSize).then(data => {
        let settlementTableBody = document.querySelector("table#settlement-table > tbody")

        settlementTableBody.innerHTML = "";

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.list || data.data.list.length === 0) {
            return;
        }

        let settlementList = data.data.list;

        for (let i = 0; i < settlementList.length; i++) {
            let settlement = settlementList[i];

            if (!settlement) {
                continue;
            }

            let settlementId = settlement.id;
            let balance = settlement.balance;
            let salesAmount = settlement.salesAmount;
            let matched = settlement.matched;
            let createTime = settlement.createTime;

            balance = parseFloat(balance);
            balance = balance.toFixed(2);
            salesAmount = parseFloat(salesAmount);
            salesAmount = salesAmount.toFixed(2);

            if (i === 0) {
                let createDateTime = new Date(createTime);

                document.getElementById("update-settlement-scheduler-value").value = createDateTime.getHours();
            }

            if (matched === true) {
                matched = "True";
            } else {
                matched = "False";
            }

            let salesInfo = "<tr>\n" +
                "    <td>" + settlementId + "</td>\n" +
                "    <td>" + balance + "</td>\n" +
                "    <td>" + salesAmount + "</td>\n" +
                "    <td>" + matched + "</td>\n" +
                "    <td>" + createTime + "</td>\n" +
                "</tr>";

            settlementTableBody.insertAdjacentHTML("beforeend", salesInfo);
        }
    });
}

function updateSettlementScheduler(merchantId, hour) {
    let schedulerInfo = {
        "merchantId": merchantId,
        "hourOfDay": hour
    };

    httpRequestPatch("/merchant/scheduler/settlement", schedulerInfo).then(data => {
        if (!data || !data.status || data.status != 200 || !data.data || !data.data.result) {
            alert("Update Settlement Scheduler Failed, Please Retry Later!");

            return;
        }

        alert("Update Settlement Scheduler Success!");
    });
}

function executeSettlementSchedulerOnce(merchantId) {
    httpRequestPost("/merchant/scheduler/settlement/" + merchantId).then(data => {
        if (!data || !data.status || data.status != 200 || !data.data || !data.data.result) {
            alert("Execute Settlement Scheduler Failed, Please Retry Later!");

            return;
        }

        getBalanceInfo(Merchant.id);
        getSettlement(Merchant.id, 1, 10);

        alert("Execute Settlement Scheduler Success!");
    });
}

function handleLeftMenuItemClick(current) {
    if (current == null) {
        return;
    }

    let nodes = document.querySelectorAll("ul.sidebar-menu-table > li");
    let areas = document.querySelectorAll("#right-content > .content-area-display");
    let targetAreaId = current.getAttribute("targetId");

    if (nodes == null || nodes.length === 0) {
        return;
    }

    for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        let area = areas[i];

        if (node == null) {
            continue;
        }

        node.className = "sidebar-menu-click";

        if (area) {
            area.className = "content-area-display content-area-hidden";
        }
    }

    current.className = "sidebar-menu-click active";

    if (targetAreaId && targetAreaId.trim().length > 0) {
        document.getElementById(targetAreaId).className = "content-area-display";
    }
}

function showProductAddDialog() {
    document.getElementById("product-add-modal").className = "product-modal";
}

function hideProductAddDialog() {
    document.getElementById("product-add-modal").className = "product-modal modal-hidden";
}

function saveProductInfo() {
    let productName = document.querySelector("input[name = 'product-name']").value;
    let productSKU = document.querySelector("input[name = 'product-sku']").value;
    let productImage = document.querySelector("input[name = 'product-cover']").value;
    let productPrice = document.querySelector("input[name = 'product-price']").value;
    let productQuantity = document.querySelector("input[name = 'product-quantity']").value;
    let productDescription = document.querySelector("textarea[name = 'product-description']").value;

    if (!productName || productName.trim().length === 0
        || !productSKU || productSKU.trim().length === 0
        || !productImage || productImage.trim().length === 0
        || !productPrice || productPrice.trim().length === 0
        || !productQuantity || productQuantity.trim().length === 0 || parseInt(productQuantity) <= 0
        || !productDescription || productDescription.trim().length === 0) {
        alert("Please Check Product Detail!");

        return;
    }

    productPrice = parseFloat(productPrice);
    productPrice = productPrice.toFixed(2);

    let productInfo = {
        "merchantId": Merchant.id,
        "productName": productName,
        "productSku": productSKU,
        "productImage": productImage,
        "productDescription": productDescription,
        "productQuantity": parseInt(productQuantity),
        "productPrice": productPrice
    };

    httpRequestPost("/merchant/product", productInfo).then(data => {
        hideProductAddDialog();

        if (!data || !data.status || data.status != 200 || !data.data || !data.data.result) {
            alert("Add New Product Failed, Please Retry Later!");

            return;
        }

        getProductList(Merchant.id, 1, 1000);
        alert(""Add New Product Success!");
    });
}

function logout() {
    const XHR = new XMLHttpRequest();

    XHR.addEventListener("load", (event) => {
        if (XHR.status == 200) {
            sessionStorage.removeItem("Merchant");
            window.location.href = "signin.html";
        } else {
            alert("Logout Failed, Please Retry Later!");

            return;
        }
    });

    XHR.addEventListener("error", (event) => {
        alert("Logout Failed, Please Retry Later!");
    });

    XHR.open("POST", "/merchant/logout");
    XHR.send();
}

function handleMerchantMenuClick() {
    let className = document.getElementById("merchant-dropdown-menu-list").className;

    if (className.indexOf("merchant-menu-dropdown-list-open") >= 0) {
        document.getElementById("merchant-dropdown-menu-list").className = "merchant-menu-dropdown-list";
    } else {
        document.getElementById("merchant-dropdown-menu-list").className = "merchant-menu-dropdown-list merchant-menu-dropdown-list-open";
    }
}

window.addEventListener("load", () => {
    let merchant = sessionStorage.getItem("Merchant");

    if (merchant != null && typeof merchant === "string" && merchant.trim().length > 0) {
        try {
            let json = JSON.parse(merchant);

            if (json != null && typeof json === "object"
                && json.authen != null && typeof json.authen === "string" && json.authen.length > 0) {
                Merchant = json;

                document.getElementById("merchant-menu-display").innerHTML = "Welcome " + Merchant.name;
            } else {
                alert("Please Signin / Signup, Then Retry!");

                window.location.href = "signin.html";
            }
        } catch (e) {
            alert("Please Signin / Signup, Then Retry!");

            window.location.href = "signin.html";
        }
    } else {
        alert("Please Signin / Signup, Then Retry!");

        window.location.href = "signin.html";
    }

    const productMenu = document.getElementById("sidebar-menu-click-product");
    const salesMenu = document.getElementById("sidebar-menu-click-sales");
    const settlementMenu = document.getElementById("sidebar-menu-click-settlement");
    const addProductButton = document.getElementById("product-add-button");
    const saveProductButton = document.getElementById("product-save-button");
    const closeProductDialogButton = document.getElementById("product-cancel-button");
    const updateSettlementSchedulerButton = document.getElementById("update-settlement-scheduler-button");
    const executeSettlementSchedulerButton = document.getElementById("run-settlement-scheduler-button");
    const showMerchantMenuButton = document.getElementById("merchant-menu-dropdown-button");
    const logoutButton = document.getElementById("merchant-menu-dropdown-list-item-logout");

    productMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(productMenu.parentNode);
        getProductList(Merchant.id, 1, 1000);
    });

    salesMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(salesMenu.parentNode);
        getSalesList(Merchant.id, 1, 1000);
    });

    settlementMenu.addEventListener("click", (event) => {
        event.preventDefault();
        handleLeftMenuItemClick(settlementMenu.parentNode);
        getBalanceInfo(Merchant.id);
        getSettlement(Merchant.id, 1, 10);
    });

    addProductButton.addEventListener("click", (event) => {
        event.preventDefault();
        showProductAddDialog();
    });

    saveProductButton.addEventListener("click", (event) => {
        event.preventDefault();
        saveProductInfo();
    });

    closeProductDialogButton.addEventListener("click", (event) => {
        event.preventDefault();
        hideProductAddDialog();
    });

    updateSettlementSchedulerButton.addEventListener("click", (event) => {
        event.preventDefault();

        let hour = document.getElementById("update-settlement-scheduler-value").value;
        let hourOfDay = parseInt(hour);

        updateSettlementScheduler(Merchant.id, hourOfDay);
    });

    executeSettlementSchedulerButton.addEventListener("click", (event) => {
        event.preventDefault();
        executeSettlementSchedulerOnce(Merchant.id);
    });

    showMerchantMenuButton.addEventListener("click", (event) => {
        event.preventDefault();
        handleMerchantMenuClick();
    });

    logoutButton.addEventListener("click", (event) => {
        event.preventDefault();
        logout();
    });

    document.addEventListener("click", (event) => {
        let node = event.target;
        let subNode = node.querySelector(".merchant-menu-dropdown-title");

        if (!subNode) {
            let className = node.className;

            if (className == "merchant-menu-dropdown-title-text" || className == "merchant-menu-text" || className == "merchant-menu-dropdown-caret") {
                return;
            } else {
                document.getElementById("merchant-dropdown-menu-list").className = "merchant-menu-dropdown-list";
            }
        }  else {
            let className = subNode.className;

            if (className == "merchant-menu-dropdown-title-text" || className == "merchant-menu-text" || className == "merchant-menu-dropdown-caret") {
                return;
            } else {
                document.getElementById("merchant-dropdown-menu-list").className = "merchant-menu-dropdown-list";
            }
        }
    });

    productMenu.click();
});