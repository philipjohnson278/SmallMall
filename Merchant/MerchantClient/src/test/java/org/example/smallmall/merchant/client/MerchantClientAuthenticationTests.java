package org.example.smallmall.merchant.client;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.authenticate.service.MerchantProfileService;
import org.example.smallmall.merchant.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.merchant.client.service.MerchantAuthenticationService;
import org.example.smallmall.merchant.client.service.MerchantDetailsService;
import org.example.smallmall.merchant.client.service.impl.MerchantAuthenticationServiceImpl;
import org.example.smallmall.merchant.client.service.impl.MerchantDetailsServiceImpl;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.example.smallmall.vo.common.response.CommonLoginResponseVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchantClientAuthenticationTests {
    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private SecurityContextRepository securityContextRepository;

    @MockBean
    private SecurityContextHolderStrategy securityContextHolderStrategy;

    @MockBean
    private MerchantAuthenticationService merchantAuthenticationService;

    @MockBean
    private MerchantDetailsService merchantDetailsService;

    @MockBean
    private MerchantOpenFeignClient merchantOpenFeignClient;

    @MockBean
    private MerchantProfileService merchantProfileService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private SecurityContext securityContext = new SecurityContextImpl();

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        Mockito.doNothing().when(securityContextHolderStrategy).setContext(Mockito.any(SecurityContext.class));
        Mockito.doReturn(securityContext).when(securityContextHolderStrategy).getContext();
        Mockito.doReturn(securityContext).when(securityContextHolderStrategy).createEmptyContext();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testLogin() throws Exception {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken("test", "test");
        MockedStatic<SecurityContextHolder> securityContextHolderMockedStatic = Mockito.mockStatic(SecurityContextHolder.class);

        securityContextHolderMockedStatic.when(() -> {
            SecurityContextHolder.createEmptyContext();
        }).thenReturn(securityContext);
        securityContextHolderMockedStatic.when(() -> {
            SecurityContextHolder.getContext();
        }).thenReturn(securityContext);
        Mockito.doReturn(authenticationToken).when(authenticationManager).authenticate(authenticationToken);
        Mockito.doNothing().when(securityContextRepository).saveContext(Mockito.any(SecurityContext.class), Mockito.any(HttpServletRequest.class), Mockito.any(HttpServletResponse.class));

        MerchantProfileEntity merchantProfile = new MerchantProfileEntity();
        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        merchantProfile.setName("test");
        merchantProfile.setSecret("test");
        commonLoginRequestVO.setName("test");
        commonLoginRequestVO.setSecret("test");
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(merchantProfile);

        MerchantAuthenticationServiceImpl merchantAuthenticationServiceImpl = new MerchantAuthenticationServiceImpl();
        ReflectionTestUtils.setField(merchantAuthenticationServiceImpl, "merchantProfileService", merchantProfileService);
        MerchantAuthenticationService merchantAuthenticationServiceMock = Mockito.spy(merchantAuthenticationServiceImpl);

        CommonLoginResponseVO authenticationInfo = merchantAuthenticationServiceMock.createAuthenticationInfo(commonLoginRequestVO);
        Assertions.assertNotNull(authenticationInfo);

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(null);
        authenticationInfo = merchantAuthenticationServiceMock.createAuthenticationInfo(commonLoginRequestVO);
        Assertions.assertNull(authenticationInfo);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/login").param("name", "test").param("secret", "")).andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(merchantProfile);
        authenticationInfo = merchantAuthenticationServiceMock.createAuthenticationInfo(commonLoginRequestVO);
        Mockito.when(merchantAuthenticationService.createAuthenticationInfo(Mockito.any(CommonLoginRequestVO.class))).thenReturn(authenticationInfo);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/login").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());

        securityContextHolderMockedStatic.clearInvocations();
        securityContextHolderMockedStatic.reset();
        securityContextHolderMockedStatic.close();
    }

    @Test
    public void testRegister() throws Exception {
        MerchantProfileEntity merchantProfile = new MerchantProfileEntity();
        CommonLoginRequestVO loginRequestVO = new CommonLoginRequestVO();

        merchantProfile.setName("test");
        merchantProfile.setSecret("test");
        loginRequestVO.setName("test");
        loginRequestVO.setSecret("test");
        loginRequestVO.setRequestTime(LocalDateTime.now());

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(merchantProfile);
        Mockito.when(merchantProfileService.merchantRegistry(Mockito.any(CommonLoginRequestVO.class))).thenReturn(true);
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn("test");

        MerchantAuthenticationServiceImpl merchantAuthenticationServiceImpl = new MerchantAuthenticationServiceImpl();
        ReflectionTestUtils.setField(merchantAuthenticationServiceImpl, "merchantProfileService", merchantProfileService);
        ReflectionTestUtils.setField(merchantAuthenticationServiceImpl, "passwordEncoder", passwordEncoder);
        MerchantAuthenticationService merchantAuthenticationServiceMock = Mockito.spy(merchantAuthenticationServiceImpl);

        MerchantDetailsServiceImpl merchantDetailsServiceImpl = new MerchantDetailsServiceImpl();
        ReflectionTestUtils.setField(merchantDetailsServiceImpl, "merchantProfileService", merchantProfileService);
        MerchantDetailsService merchantDetailsServiceMock = Mockito.spy(merchantDetailsServiceImpl);

        boolean registered = merchantAuthenticationServiceMock.register(loginRequestVO);
        Assertions.assertTrue(registered);

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(null);
        MerchantProfileEntity merchantProfileInfo = merchantDetailsServiceMock.getMerchantByName("test");
        Assertions.assertNull(merchantProfileInfo);

        Mockito.when(merchantProfileService.getMerchant(Mockito.anyString())).thenReturn(merchantProfile);
        UserDetails userDetails = merchantDetailsServiceImpl.loadUserByUsername("test");
        Assertions.assertNotNull(userDetails);
        Assertions.assertNotNull(userDetails.getAuthorities());
        Assertions.assertNotNull(userDetails.getPassword());
        Assertions.assertNotNull(userDetails.getPassword());
        Assertions.assertFalse(userDetails.isEnabled());
        Assertions.assertTrue(userDetails.isAccountNonExpired());
        Assertions.assertTrue(userDetails.isAccountNonLocked());
        Assertions.assertTrue(userDetails.isCredentialsNonExpired());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "")).andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.when(merchantAuthenticationService.register(Mockito.any(CommonLoginRequestVO.class))).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.when(merchantDetailsService.getMerchantByName(Mockito.anyString())).thenReturn(merchantProfile);
        Mockito.when(merchantOpenFeignClient.createSettlementSchedule(Mockito.any(MerchantUpdateSettlementSchedulerRequestVO.class))).thenReturn("test");
        Mockito.when(merchantAuthenticationService.register(Mockito.any(CommonLoginRequestVO.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register").param("name", "test").param("secret", "test")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}