package org.example.smallmall.merchant.client;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.smallmall.merchant.client.controller.MerchantClientController;
import org.example.smallmall.merchant.client.feign.MerchantOpenFeignClient;
import org.example.smallmall.merchant.client.feign.fallback.MerchantOpenFeignClientFallback;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchantClientControllerTests {
    private static final String responseJson = "{\"test\": \"Test\"}";

    @MockBean
    private MerchantOpenFeignClient merchantOpenFeignClient;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetMerchantBalance() throws Exception {
        Mockito.when(merchantOpenFeignClient.getBalance(Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/balance/merchant/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/balance/merchant/1").with(SecurityMockMvcRequestPostProcessors.user("test").password("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetMerchantProduct() throws Exception {
        Mockito.when(merchantOpenFeignClient.getProduct(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/merchant/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/merchant/1").with(SecurityMockMvcRequestPostProcessors.user("test").password("test")).param("page", "1").param("size", "1")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testIncreaseProductQuantity() throws Exception {
        Mockito.when(merchantOpenFeignClient.increaseQuantity(Mockito.anyInt(), Mockito.any(MerchantIncreaseProductQuantityRequestVO.class))).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        ObjectMapper objectMapper = new ObjectMapper();
        MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO = new MerchantIncreaseProductQuantityRequestVO();

        increaseProductQuantityRequestVO.setMerchantId(1);
        increaseProductQuantityRequestVO.setProductId(1);
        increaseProductQuantityRequestVO.setCount(1);
        String requestJson = objectMapper.writeValueAsString(increaseProductQuantityRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test").password("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testAddProduct() throws Exception {
        Mockito.when(merchantOpenFeignClient.addProduct(Mockito.any(MerchantAddProductRequestVO.class))).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/product").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        ObjectMapper objectMapper = new ObjectMapper();
        MerchantAddProductRequestVO addProductRequestVO = new MerchantAddProductRequestVO();
        String requestJson = objectMapper.writeValueAsString(addProductRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/product").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        addProductRequestVO.setMerchantId(1);
        addProductRequestVO.setProductDescription("Test");
        addProductRequestVO.setProductImage("test");
        addProductRequestVO.setProductName("test");
        addProductRequestVO.setProductPrice(new BigDecimal("3.14"));
        addProductRequestVO.setProductQuantity(3);
        addProductRequestVO.setProductSku("test");
        requestJson = objectMapper.writeValueAsString(addProductRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/product").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetMerchantSales() throws Exception {
        Mockito.when(merchantOpenFeignClient.getSales(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/1").param("page", "1").param("size", "0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/1").param("page", "1").param("size", "1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testGetMerchantSettlement() throws Exception {
        Mockito.when(merchantOpenFeignClient.getSettlement(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/1").param("page", "1").param("size", "0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/1").param("page", "1").param("size", "1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testRunMerchantSettlementScheduler() throws Exception {
        Mockito.when(merchantOpenFeignClient.runSettlementSchedule(Mockito.anyInt())).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement/0").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement/1").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testUpdateMerchantSettlementScheduler() throws Exception {
        Mockito.when(merchantOpenFeignClient.updateSettlementSchedule(Mockito.any(MerchantUpdateSettlementSchedulerRequestVO.class))).thenReturn(responseJson);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        ObjectMapper objectMapper = new ObjectMapper();
        MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO = new MerchantUpdateSettlementSchedulerRequestVO();
        String requestJson = objectMapper.writeValueAsString(updateSettlementSchedulerRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        updateSettlementSchedulerRequestVO.setMerchantId(1);
        updateSettlementSchedulerRequestVO.setHourOfDay(24);
        requestJson = objectMapper.writeValueAsString(updateSettlementSchedulerRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isBadRequest());

        updateSettlementSchedulerRequestVO.setHourOfDay(1);
        requestJson = objectMapper.writeValueAsString(updateSettlementSchedulerRequestVO);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON).with(SecurityMockMvcRequestPostProcessors.user("test"))).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testSentinelBlockHandler() {
        MerchantClientController merchantClientController = new MerchantClientController();
        MerchantClientController merchantClientControllerMock = Mockito.spy(merchantClientController);
        BlockException blockException = new FlowException("Test");

        ResponseVO responseVO = merchantClientControllerMock.loginBlockHelper(null, null, null, null, blockException);
        Assertions.assertNotNull(responseVO);

        responseVO = merchantClientControllerMock.registerBlockHelper(null, null, blockException);
        Assertions.assertNotNull(responseVO);

        String blockJson = merchantClientControllerMock.getMerchantBalanceBlockHelper(null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.getMerchantProductBlockHelper(null, null, null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.increaseProductQuantityBlockHelper(null, null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.addProductBlockHelper(null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.getMerchantSalesBlockHelper(null, null, null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.getMerchantSettlementBlockHelper(null, null, null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.runMerchantSettlementSchedulerBlockHelper(null, blockException);
        Assertions.assertNotNull(blockJson);

        blockJson = merchantClientControllerMock.updateMerchantSettlementSchedulerBlockHelper(null, blockException);
        Assertions.assertNotNull(blockJson);
    }

    @Test
    public void testOpenFeignFallback() {
        MerchantOpenFeignClientFallback openFeignClientFallback = new MerchantOpenFeignClientFallback();
        MerchantOpenFeignClientFallback openFeignClientFallbackMock = Mockito.spy(openFeignClientFallback);

        String fallbackJson = openFeignClientFallbackMock.getSales(null, null, null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.getBalance(null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.getProduct(null, null, null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.increaseQuantity(null, null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.addProduct(null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.getSettlement(null, null, null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.runSettlementSchedule(null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.updateSettlementSchedule(null);
        Assertions.assertNotNull(fallbackJson);

        fallbackJson = openFeignClientFallbackMock.createSettlementSchedule(null);
        Assertions.assertNotNull(fallbackJson);
    }
}