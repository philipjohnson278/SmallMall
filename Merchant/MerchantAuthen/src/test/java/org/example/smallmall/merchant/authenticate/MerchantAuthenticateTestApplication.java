package org.example.smallmall.merchant.authenticate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantAuthenticateTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(MerchantAuthenticateTestApplication.class, args);
    }
}