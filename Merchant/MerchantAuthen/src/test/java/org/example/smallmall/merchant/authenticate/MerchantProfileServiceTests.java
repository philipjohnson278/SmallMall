package org.example.smallmall.merchant.authenticate;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.authenticate.service.MerchantProfileService;
import org.example.smallmall.merchant.authenticate.service.impl.MerchantProfileServiceImpl;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

@SpringBootTest(classes = {MerchantAuthenticateTestApplication.class})
@ActiveProfiles(profiles = {"common"})
public class MerchantProfileServiceTests {
    private MerchantProfileService merchantProfileService;

    @BeforeEach
    public void setup() {
        MerchantProfileServiceImpl merchantProfileServiceImpl = new MerchantProfileServiceImpl();
        merchantProfileService = Mockito.spy(merchantProfileServiceImpl);
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testGetMerchant() {
        MerchantProfileEntity merchantProfile = new MerchantProfileEntity();
        Mockito.doReturn(merchantProfile).when(merchantProfileService).getOne(Mockito.any(LambdaQueryWrapper.class));

        MerchantProfileEntity merchantProfileTest = merchantProfileService.getMerchant("test");
        Assertions.assertNotNull(merchantProfileTest);
        Assertions.assertNull(merchantProfileTest.getName());
        Assertions.assertNull(merchantProfileTest.getSecret());
        Assertions.assertNull(merchantProfileTest.getCreateTime());
        Assertions.assertNull(merchantProfileTest.getUpdateTime());
        Assertions.assertTrue(merchantProfileTest.getId() == 0);
        Assertions.assertTrue(merchantProfileTest.getState() == 0);
    }

    @Test
    public void testMerchantRegistry() {
        Mockito.doReturn(null).when(merchantProfileService).getMerchant(Mockito.anyString());
        Mockito.doReturn(true).when(merchantProfileService).save(Mockito.any(MerchantProfileEntity.class));

        CommonLoginRequestVO commonLoginRequestVO = new CommonLoginRequestVO();

        commonLoginRequestVO.setName("test");
        commonLoginRequestVO.setSecret("test");
        commonLoginRequestVO.setRequestTime(LocalDateTime.now());

        boolean success = merchantProfileService.merchantRegistry(commonLoginRequestVO);
        Assertions.assertTrue(success);

        MerchantProfileEntity merchantProfile = new MerchantProfileEntity();

        merchantProfile.setId(1);
        merchantProfile.setName("test");
        merchantProfile.setSecret("test");
        merchantProfile.setState((byte) 1);
        merchantProfile.setCreateTime(LocalDateTime.now());
        merchantProfile.setUpdateTime(LocalDateTime.now());
        Mockito.doReturn(merchantProfile).when(merchantProfileService).getMerchant(Mockito.anyString());
        success = merchantProfileService.merchantRegistry(commonLoginRequestVO);
        Assertions.assertFalse(success);
    }
}