package org.example.smallmall.merchant.authenticate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;


@Mapper
public interface MerchantProfileMapper extends BaseMapper<MerchantProfileEntity> {
}