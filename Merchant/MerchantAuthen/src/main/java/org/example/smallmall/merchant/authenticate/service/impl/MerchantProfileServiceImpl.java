package org.example.smallmall.merchant.authenticate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.merchant.authenticate.mapper.MerchantProfileMapper;
import org.example.smallmall.merchant.authenticate.service.MerchantProfileService;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MerchantProfileServiceImpl extends ServiceImpl<MerchantProfileMapper, MerchantProfileEntity> implements MerchantProfileService {
    @Override
    public MerchantProfileEntity getMerchant(String merchantName) {
        LambdaQueryWrapper<MerchantProfileEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(MerchantProfileEntity.class).eq(MerchantProfileEntity::getName, merchantName);
        MerchantProfileEntity merchantProfile = this.getOne(lambdaQueryWrapper);

        return merchantProfile;
    }

    @Override
    public boolean merchantRegistry(CommonLoginRequestVO commonLoginRequestVO) {
        MerchantProfileEntity merchantProfile = this.getMerchant(commonLoginRequestVO.getName());

        if (merchantProfile != null) {
            log.error("Can not registry a new merchant, already exists merchant name: {}", commonLoginRequestVO.getName());

            return false;
        }

        merchantProfile = new MerchantProfileEntity();
        merchantProfile.setName(commonLoginRequestVO.getName());
        merchantProfile.setSecret(commonLoginRequestVO.getSecret());
        merchantProfile.setState((byte) 1);
        merchantProfile.setCreateTime(commonLoginRequestVO.getRequestTime());

        try {
            boolean saved = this.save(merchantProfile);

            log.info("Registry a new merchant, name: {}, request time: {}, result: {}.", commonLoginRequestVO.getName(), commonLoginRequestVO.getRequestTime(), saved);

            return saved;
        } catch (Exception e) {
            log.error("Registry a new merchant failed by exception, name: {}, request time: {}, exception: {}", commonLoginRequestVO.getName(), commonLoginRequestVO.getRequestTime(), e.getMessage());
        }

        return false;
    }
}