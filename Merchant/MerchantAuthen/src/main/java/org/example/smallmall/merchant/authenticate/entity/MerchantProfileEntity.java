package org.example.smallmall.merchant.authenticate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("tbl_merchant_profile")
@Data
public class MerchantProfileEntity implements Serializable {
    private static final long serialVersionUID = -5028441949733061683L;

    private int id;
    private String name;
    private String secret;
    private byte state;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}