package org.example.smallmall.merchant.authenticate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.authenticate.entity.MerchantProfileEntity;
import org.example.smallmall.vo.common.request.CommonLoginRequestVO;

public interface MerchantProfileService extends IService<MerchantProfileEntity> {
    public MerchantProfileEntity getMerchant(String merchantName);
    public boolean merchantRegistry(CommonLoginRequestVO commonLoginRequestVO);
}