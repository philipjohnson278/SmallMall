package org.example.smallmall.merchant.scheduler.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.smallmall.merchant.scheduler.entity.SchedulerHistoryEntity;

import java.time.LocalDateTime;

@Mapper
public interface SchedulerHistoryMapper extends BaseMapper<SchedulerHistoryEntity> {
    public Integer settlementScheduler(@Param("merchantId") Integer merchantId, @Param("createTime") LocalDateTime createTime);
}