package org.example.smallmall.merchant.scheduler.dto;

import lombok.Data;

import java.util.Date;

@Data
public class XxlJobInfoDTO {
    private int jobGroup;
    private String jobDesc;
    private String author;
    private String scheduleType;
    private String scheduleConf;
    private String misfireStrategy;
    private String executorRouteStrategy;
    private String executorHandler;
    private String executorParam;
    private String executorBlockStrategy;
    private int executorFailRetryCount;
    private String glueType;
    private int triggerStatus;
    private int executorTimeout;
}