package org.example.smallmall.merchant.scheduler.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_scheduler_history")
@Data
public class SchedulerHistoryEntity implements Serializable {
    private static final long serialVersionUID = 5443095644937306182L;

    private int id;
    private int merchantId;
    private BigDecimal balance;
    private BigDecimal salesAmount;
    private Boolean matched;
    private LocalDateTime createTime;
}