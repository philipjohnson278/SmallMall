package org.example.smallmall.merchant.scheduler.dao;

import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.scheduler.mapper.SchedulerHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class SchedulerHistoryDAO {
    @Autowired
    private SchedulerHistoryMapper schedulerHistoryMapper;

    public boolean addSettlementSchedulerHistory(Integer merchantId, LocalDateTime createTime) {
        Integer addedRow = schedulerHistoryMapper.settlementScheduler(merchantId, createTime);

        if (addedRow == null || addedRow <= 0) {
            log.error("Add merchant settlement scheduler history failed, merchant ID: {}", merchantId);

            return false;
        }

        return true;
    }
}