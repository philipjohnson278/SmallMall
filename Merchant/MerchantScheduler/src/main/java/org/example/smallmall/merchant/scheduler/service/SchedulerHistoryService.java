package org.example.smallmall.merchant.scheduler.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.scheduler.entity.SchedulerHistoryEntity;

import java.time.LocalDateTime;
import java.util.Map;

public interface SchedulerHistoryService extends IService<SchedulerHistoryEntity> {
    public Map<String, Object> getSettlementSchedulerHistory(Integer merchantId, Integer pageNumber, Integer pageSize);
    public boolean startSettlementScheduledJob(Integer merchantId, LocalDateTime createTime);
    public boolean createSettlementScheduler(Integer merchantId, Integer hourOfDay);
    public boolean updateSettlementScheduler(Integer merchantId, Integer hourOfDay);
}