package org.example.smallmall.merchant.scheduler.scheduler;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.example.smallmall.merchant.scheduler.service.SchedulerHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class MerchantSettlementJobHandler extends IJobHandler {
    @Autowired
    private SchedulerHistoryService schedulerHistoryService;

    @Override
    public void execute() throws Exception {
        String parameter = XxlJobHelper.getJobParam();

        if (parameter == null || !StringUtils.isNumeric(parameter)) {
            log.error("Start settlement scheduled job failed, job parameter not correct, parameter: {}", parameter);

            return;
        }

        int merchantId = Integer.parseInt(parameter);
        boolean started = schedulerHistoryService.startSettlementScheduledJob(merchantId, LocalDateTime.now());

        log.info("Start settlement scheduled job for merchant ID: {}, result: {}.", merchantId, started);
    }
}