package org.example.smallmall.merchant.scheduler.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.example.smallmall.merchant.scheduler.dao.SchedulerHistoryDAO;
import org.example.smallmall.merchant.scheduler.dto.XxlJobInfoDTO;
import org.example.smallmall.merchant.scheduler.entity.SchedulerHistoryEntity;
import org.example.smallmall.merchant.scheduler.mapper.SchedulerHistoryMapper;
import org.example.smallmall.merchant.scheduler.service.SchedulerHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SchedulerHistoryServiceImpl extends ServiceImpl<SchedulerHistoryMapper, SchedulerHistoryEntity> implements SchedulerHistoryService {
    private static ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.appname}")
    private String appName;

    @Value("${xxl.job.accessToken}")
    private String accessToken;

    @Value("${merchant-scheduler.xxl-job.admin.username}")
    private String adminName;

    @Value("${merchant-scheduler.xxl-job.admin.password}")
    private String adminPassword;

    @Value("${merchant-scheduler.xxl-job.config.executor-block-strategy}")
    private String executorBlockStrategy;

    @Value("${merchant-scheduler.xxl-job.config.executor-fail-retry-count}")
    private Integer executorFailRetryCount;

    @Value("${merchant-scheduler.xxl-job.config.executor-handler}")
    private String executorHandler;

    @Value("${merchant-scheduler.xxl-job.config.executor-route-strategy}")
    private String executorRouteStrategy;

    @Value("${merchant-scheduler.xxl-job.config.glue-type}")
    private String glueType;

    @Value("${merchant-scheduler.xxl-job.config.job-author-prefix}")
    private String jobAuthorPrefix;

    @Value("${merchant-scheduler.xxl-job.config.job-description-prefix}")
    private String jobDescriptionPrefix;

    @Value("${merchant-scheduler.xxl-job.config.misfire-strategy}")
    private String misfireStrategy;

    @Value("${merchant-scheduler.xxl-job.config.schedule-type}")
    private String scheduleType;

    @Value("${merchant-scheduler.xxl-job.group-id}")
    private Integer groupId;

    @Value("${merchant-scheduler.xxl-job.restful.login}")
    private String loginPath;

    @Value("${merchant-scheduler.xxl-job.restful.logout}")
    private String logoutPath;

    @Value("${merchant-scheduler.xxl-job.restful.add-job}")
    private String addJobPath;

    @Value("${merchant-scheduler.xxl-job.restful.query-job}")
    private String queryJobPath;

    @Value("${merchant-scheduler.xxl-job.restful.start-job}")
    private String startJobPath;

    @Value("${merchant-scheduler.xxl-job.restful.trigger-job}")
    private String triggerJobPath;

    @Value("${merchant-scheduler.xxl-job.restful.update-job}")
    private String updateJobPath;

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private SchedulerHistoryDAO schedulerHistoryDAO;

    @Override
    public Map<String, Object> getSettlementSchedulerHistory(Integer merchantId, Integer pageNumber, Integer pageSize) {
        LambdaQueryWrapper<SchedulerHistoryEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(SchedulerHistoryEntity.class)
                .eq(SchedulerHistoryEntity::getMerchantId, merchantId)
                .orderByDesc(SchedulerHistoryEntity::getCreateTime);
        Page<SchedulerHistoryEntity> merchantHistoryPage = this.page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);

        if (merchantHistoryPage == null || merchantHistoryPage.getRecords() == null) {
            log.warn("No merchant history found for query argument: merchant ID: {}, pageNumber: {}, pageSize: {}", merchantId, pageNumber, pageSize);

            return null;
        }

        List<SchedulerHistoryEntity> historyList = merchantHistoryPage.getRecords();
        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("list", historyList);
        resultMap.put("totalPages", merchantHistoryPage.getPages() + 1);
        resultMap.put("currentPage", merchantHistoryPage.getCurrent());
        resultMap.put("pageSize", historyList.size());

        return resultMap;
    }

    @Override
    public boolean startSettlementScheduledJob(Integer merchantId, LocalDateTime createTime) {
        boolean added = schedulerHistoryDAO.addSettlementSchedulerHistory(merchantId, createTime);

        return added;
    }

    @Override
    public boolean createSettlementScheduler(Integer merchantId, Integer hourOfDay) {
        String cookie = loginXxl(adminAddresses + loginPath, adminName, adminPassword);

        if (StringUtils.isBlank(cookie)) {
            return false;
        }

        XxlJobInfoDTO xxlJobInfo = getXxlJobInfo(merchantId, hourOfDay);
        Map<String, Object> xxlJobInfoMap = objectMapper.convertValue(xxlJobInfo, Map.class);
        Map<String, Object> responseMap = this.updateJob(adminAddresses + addJobPath, cookie, xxlJobInfoMap);

        logoutXxl(adminAddresses + logoutPath, cookie);

        if (responseMap == null || responseMap.isEmpty() || ((Integer) responseMap.get("code")) != HttpStatus.OK.value()) {
            try {
                log.error("Create job scheduler info failed of merchant ID: {}, job info: {}", merchantId, objectMapper.writeValueAsString(xxlJobInfoMap));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            return false;
        }

        return true;
    }

    @Override
    public boolean updateSettlementScheduler(Integer merchantId, Integer hourOfDay) {
        String cookie = loginXxl(adminAddresses + loginPath, adminName, adminPassword);

        if (StringUtils.isBlank(cookie)) {
            return false;
        }

        Map<String, Object> jobInfoMap = this.queryJob(adminAddresses + queryJobPath, cookie, groupId, 1, jobDescriptionPrefix + merchantId, executorHandler, jobAuthorPrefix + merchantId);

        if (jobInfoMap == null || jobInfoMap.isEmpty()) {
            logoutXxl(adminAddresses + logoutPath, cookie);

            log.error("No scheduled job query result found of merchant ID: {}", merchantId);

            return false;
        }

        List<Object> jobInfoList = objectMapper.convertValue(jobInfoMap.get("data"), List.class);

        if (jobInfoList == null || jobInfoList.isEmpty() || jobInfoList.get(0) == null) {
            logoutXxl(adminAddresses + logoutPath, cookie);

            log.error("No scheduled job info found of merchant ID: {} for updating.", merchantId);

            return false;
        }

        LocalDateTime localDateTime = LocalDateTime.now();
        Map<String, Object> foundJobInfoMap = objectMapper.convertValue(jobInfoList.get(0), Map.class);

        foundJobInfoMap.remove("addTime");
        foundJobInfoMap.remove("updateTime");
        foundJobInfoMap.remove("glueUpdatetime");
        foundJobInfoMap.remove("triggerLastTime");
        foundJobInfoMap.remove("triggerNextTime");
        foundJobInfoMap.put("scheduleConf", localDateTime.getSecond() + " " + localDateTime.getMinute() + " " + hourOfDay + " * * ?");

        Map<String, Object> updateResultMap = this.updateJob(adminAddresses + updateJobPath, cookie, foundJobInfoMap);

        logoutXxl(adminAddresses + logoutPath, cookie);

        if (updateResultMap == null || updateResultMap.isEmpty() || ((Integer) updateResultMap.get("code")) != HttpStatus.OK.value()) {
            try {
                log.error("Update job scheduler info failed of merchant ID: {}, job info: {}", merchantId, objectMapper.writeValueAsString(foundJobInfoMap));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            return false;
        }

        return true;
    }

    private String loginXxl(String url, String userName, String password) {
        String targetUrl = url + "?userName=" + userName + "&password=" + password;
        ResponseEntity<Map> responseMap = restTemplate.postForEntity(targetUrl, new HashMap<String, String>(), Map.class);

        if (responseMap == null || responseMap.getStatusCode() != HttpStatus.OK) {
            log.error("Login XXL-Job by Restful API failed, login URL: {}.", url);

            return null;
        }

        HttpHeaders header = responseMap.getHeaders();

        if (header == null || header.get(HttpHeaders.SET_COOKIE) == null || header.get(HttpHeaders.SET_COOKIE).isEmpty()) {
            log.error("Login XXL-Job by Restful API failed, no response header found, login URL: {}.", url);

            return null;
        }

        String cookie = header.get(HttpHeaders.SET_COOKIE).stream().collect(Collectors.joining(";"));

        return cookie;
    }

    private Map<String, Object> logoutXxl(String url, String cookie) {
        HttpHeaders header = new HttpHeaders();

        header.put(HttpHeaders.COOKIE, Arrays.asList(cookie));

        HttpEntity<Object> request = new HttpEntity<>(header);
        ResponseEntity<Map> responseMap = restTemplate.postForEntity(url, request, Map.class);

        if (responseMap == null || responseMap.getStatusCode() != HttpStatus.OK) {
            log.error("Logout XXL-Job by Restful API failed, request URL: {}.", url);

            return null;
        }

        Map<String, Object> resultMap = responseMap.getBody();

        return resultMap;
    }

    private Map<String, Object> queryJob(String url, String cookie, int jobGroup, int triggerStatus, String jobDesc, String executorHandler, String author) {
        String targetUrl = url + "?jobGroup=" + jobGroup + "&triggerStatus=" + triggerStatus + "&jobDesc=" + jobDesc + "&executorHandler=" + executorHandler + "&author=" + author;
        HttpHeaders header = new HttpHeaders();

        header.put(HttpHeaders.COOKIE, Arrays.asList(cookie));

        HttpEntity<Object> request = new HttpEntity<>(header);
        ResponseEntity<Map> responseMap = restTemplate.postForEntity(targetUrl, request, Map.class);

        if (responseMap == null || responseMap.getStatusCode() != HttpStatus.OK) {
            log.error("Query XXL-Job by Restful API failed, request URL: {}.", url);

            return null;
        }

        Map<String, Object> resultMap = responseMap.getBody();

        return resultMap;
    }

    private Map<String, Object> updateJob(String url, String cookie, Map<String, Object> xxlJobInfoMap) {
        HttpHeaders header = new HttpHeaders();
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(xxlJobInfoMap, header);
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        header.setContentType(MediaType.APPLICATION_JSON);
        header.put(HttpHeaders.COOKIE, Arrays.asList(cookie));
        xxlJobInfoMap.forEach((k, v) -> {
            if (v == null) {
                return;
            }

            multiValueMap.put(k, Arrays.asList(v.toString()));
        });

        String targetUrl = UriComponentsBuilder.fromHttpUrl(url).queryParams(multiValueMap).build().toString();
        ResponseEntity<Map> responseMap = restTemplate.postForEntity(targetUrl, request, Map.class);

        if (responseMap == null || responseMap.getStatusCode() != HttpStatus.OK) {
            try {
                log.error("Add XXL-Job job handler by Restful API failed, request URL: {}, job info: {}.", targetUrl, objectMapper.writeValueAsString(xxlJobInfoMap));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            return null;
        }

        Map<String, Object> resultMap = responseMap.getBody();

        return resultMap;
    }

    private XxlJobInfoDTO getXxlJobInfo(Integer merchantId, Integer hourOfDay) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        XxlJobInfoDTO xxlJobInfo = new XxlJobInfoDTO();

        xxlJobInfo.setAuthor(jobAuthorPrefix + merchantId);
        xxlJobInfo.setExecutorBlockStrategy(executorBlockStrategy);
        xxlJobInfo.setExecutorFailRetryCount(executorFailRetryCount);
        xxlJobInfo.setExecutorHandler(executorHandler);
        xxlJobInfo.setExecutorParam(merchantId.toString());
        xxlJobInfo.setExecutorRouteStrategy(executorRouteStrategy);
        xxlJobInfo.setExecutorTimeout(0);
        xxlJobInfo.setGlueType(glueType);
        xxlJobInfo.setJobDesc(jobDescriptionPrefix + merchantId);
        xxlJobInfo.setJobGroup(groupId);
        xxlJobInfo.setMisfireStrategy(misfireStrategy);
        xxlJobInfo.setScheduleConf(currentDateTime.getSecond() + " " + currentDateTime.getMinute() + " " + hourOfDay + " * * ?");
        xxlJobInfo.setScheduleType(scheduleType);
        xxlJobInfo.setTriggerStatus(1);

        return xxlJobInfo;
    }
}