package org.example.smallmall.merchant.scheduler;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxl.job.core.context.XxlJobHelper;
import org.example.smallmall.merchant.scheduler.dao.SchedulerHistoryDAO;
import org.example.smallmall.merchant.scheduler.dto.XxlJobInfoDTO;
import org.example.smallmall.merchant.scheduler.entity.SchedulerHistoryEntity;
import org.example.smallmall.merchant.scheduler.mapper.SchedulerHistoryMapper;
import org.example.smallmall.merchant.scheduler.scheduler.MerchantSettlementJobHandler;
import org.example.smallmall.merchant.scheduler.service.SchedulerHistoryService;
import org.example.smallmall.merchant.scheduler.service.impl.SchedulerHistoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = {MerchantSchedulerTestApplication.class})
@ActiveProfiles(profiles = {"common", "merchant-scheduler"})
class MerchantSchedulerApplicationTests {
    @Value("${merchant-scheduler.xxl-job.config.executor-block-strategy}")
    private String executorBlockStrategy;

    @Value("${merchant-scheduler.xxl-job.config.executor-fail-retry-count}")
    private Integer executorFailRetryCount;

    @Value("${merchant-scheduler.xxl-job.config.executor-handler}")
    private String executorHandler;

    @Value("${merchant-scheduler.xxl-job.config.executor-route-strategy}")
    private String executorRouteStrategy;

    @Value("${merchant-scheduler.xxl-job.config.glue-type}")
    private String glueType;

    @Value("${merchant-scheduler.xxl-job.config.job-author-prefix}")
    private String jobAuthorPrefix;

    @Value("${merchant-scheduler.xxl-job.config.job-description-prefix}")
    private String jobDescriptionPrefix;

    @Value("${merchant-scheduler.xxl-job.config.misfire-strategy}")
    private String misfireStrategy;

    @Value("${merchant-scheduler.xxl-job.config.schedule-type}")
    private String scheduleType;

    @Value("${merchant-scheduler.xxl-job.group-id}")
    private Integer groupId;

    @Mock
    private UriComponents uriComponents;

    @MockBean
    private SchedulerHistoryMapper schedulerHistoryMapper;

    @MockBean
    private SchedulerHistoryService schedulerHistoryService;

    @Test
    void contextLoads() {
    }

    @Test
    public void testGetSettlementSchedulerHistory() {
        SchedulerHistoryDAO schedulerHistoryDAO = new SchedulerHistoryDAO();
        ReflectionTestUtils.setField(schedulerHistoryDAO, "schedulerHistoryMapper", schedulerHistoryMapper);
        SchedulerHistoryDAO schedulerHistoryDAOMock = Mockito.spy(schedulerHistoryDAO);
        SchedulerHistoryServiceImpl schedulerHistoryServiceImpl = new SchedulerHistoryServiceImpl();
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "schedulerHistoryDAO", schedulerHistoryDAOMock);
        SchedulerHistoryService schedulerHistoryServiceMock = Mockito.spy(schedulerHistoryServiceImpl);

        Mockito.doReturn(null).when(schedulerHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        Map<String, Object> settlementSchedulerHistoryMap = schedulerHistoryServiceMock.getSettlementSchedulerHistory(1, 1, 1);
        Assertions.assertNull(settlementSchedulerHistoryMap);

        SchedulerHistoryEntity schedulerHistory = new SchedulerHistoryEntity();
        List<SchedulerHistoryEntity> historyList = new ArrayList<>();
        Page<SchedulerHistoryEntity> schedulerHistoryPage = new Page<>();

        schedulerHistoryPage.setCurrent(1);
        schedulerHistoryPage.setPages(1);
        schedulerHistoryPage.setSize(1);
        schedulerHistoryPage.setTotal(1);
        historyList.add(schedulerHistory);
        schedulerHistoryPage.setRecords(historyList);

        Mockito.doReturn(schedulerHistoryPage).when(schedulerHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        settlementSchedulerHistoryMap = schedulerHistoryServiceMock.getSettlementSchedulerHistory(1, 1, 1);
        Assertions.assertFalse(settlementSchedulerHistoryMap.isEmpty());
    }

    @Test
    public void testStartSettlementScheduledJob() {
        Mockito.doReturn(null).when(schedulerHistoryMapper).settlementScheduler(Mockito.anyInt(), Mockito.any(LocalDateTime.class));

        SchedulerHistoryDAO schedulerHistoryDAO = new SchedulerHistoryDAO();
        ReflectionTestUtils.setField(schedulerHistoryDAO, "schedulerHistoryMapper", schedulerHistoryMapper);
        SchedulerHistoryDAO schedulerHistoryDAOMock = Mockito.spy(schedulerHistoryDAO);
        SchedulerHistoryServiceImpl schedulerHistoryServiceImpl = new SchedulerHistoryServiceImpl();
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "schedulerHistoryDAO", schedulerHistoryDAOMock);
        SchedulerHistoryService schedulerHistoryServiceMock = Mockito.spy(schedulerHistoryServiceImpl);

        boolean started = schedulerHistoryServiceMock.startSettlementScheduledJob(1, LocalDateTime.now());
        Assertions.assertFalse(started);

        Mockito.doReturn(1).when(schedulerHistoryMapper).settlementScheduler(Mockito.anyInt(), Mockito.any(LocalDateTime.class));
        started = schedulerHistoryServiceMock.startSettlementScheduledJob(1, LocalDateTime.now());
        Assertions.assertTrue(started);
    }

    @Test
    public void testCreateSettlementScheduler() {
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);
        SchedulerHistoryServiceImpl schedulerHistoryServiceImpl = new SchedulerHistoryServiceImpl();
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "restTemplate", restTemplateMock);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "jobAuthorPrefix", jobAuthorPrefix);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorBlockStrategy", executorBlockStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorFailRetryCount", executorFailRetryCount);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorHandler", executorHandler);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorRouteStrategy", executorRouteStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "glueType", glueType);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "jobDescriptionPrefix", jobDescriptionPrefix);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "groupId", groupId);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "misfireStrategy", misfireStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "scheduleType", scheduleType);
        SchedulerHistoryService schedulerHistoryServiceMock = Mockito.spy(schedulerHistoryServiceImpl);

        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.anyMap().getClass());
        boolean created = schedulerHistoryServiceMock.createSettlementScheduler(1, 1);
        Assertions.assertFalse(created);

        HttpHeaders header = new HttpHeaders();
        header.put(HttpHeaders.SET_COOKIE, Arrays.asList("test=test"));
        ResponseEntity<Map> responseMap = new ResponseEntity<>(HttpStatusCode.valueOf(HttpStatus.OK.value()));

        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.eq(Map.class));
        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        created = schedulerHistoryServiceMock.createSettlementScheduler(1, 1);
        Assertions.assertFalse(created);

        UriComponentsBuilder uriComponentsBuilder = Mockito.mock(UriComponentsBuilder.class);
        MockedStatic<UriComponentsBuilder> uriComponentsBuilderMockedStatic = Mockito.mockStatic(UriComponentsBuilder.class);

        uriComponentsBuilderMockedStatic.when(() -> {
            UriComponentsBuilder.fromHttpUrl(Mockito.anyString());
        }).thenReturn(uriComponentsBuilder);
        Mockito.doReturn(uriComponentsBuilder).when(uriComponentsBuilder).queryParams(Mockito.any(MultiValueMap.class));
        Mockito.doReturn(uriComponents).when(uriComponentsBuilder).build();
        Mockito.doReturn("test").when(uriComponents).toString();

        responseMap = new ResponseEntity<>(header, HttpStatusCode.valueOf(HttpStatus.OK.value()));
        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.eq(Map.class));
        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        created = schedulerHistoryServiceMock.createSettlementScheduler(1, 1);
        Assertions.assertFalse(created);

        Map<String, Object> bodyMap = new HashMap<>();

        bodyMap.put("code", 200);
        responseMap = new ResponseEntity<>(bodyMap, header, HttpStatusCode.valueOf(HttpStatus.OK.value()));
        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        created = schedulerHistoryServiceMock.createSettlementScheduler(1, 1);
        Assertions.assertTrue(created);

        uriComponentsBuilderMockedStatic.clearInvocations();
        uriComponentsBuilderMockedStatic.reset();
        uriComponentsBuilderMockedStatic.close();
    }

    @Test
    public void testUpdateSettlementScheduler() {
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);
        SchedulerHistoryServiceImpl schedulerHistoryServiceImpl = new SchedulerHistoryServiceImpl();
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "restTemplate", restTemplateMock);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "jobAuthorPrefix", jobAuthorPrefix);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorBlockStrategy", executorBlockStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorFailRetryCount", executorFailRetryCount);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorHandler", executorHandler);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "executorRouteStrategy", executorRouteStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "glueType", glueType);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "jobDescriptionPrefix", jobDescriptionPrefix);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "groupId", groupId);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "misfireStrategy", misfireStrategy);
        ReflectionTestUtils.setField(schedulerHistoryServiceImpl, "scheduleType", scheduleType);
        SchedulerHistoryService schedulerHistoryServiceMock = Mockito.spy(schedulerHistoryServiceImpl);

        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.anyMap().getClass());
        boolean updated = schedulerHistoryServiceMock.updateSettlementScheduler(1, 1);
        Assertions.assertFalse(updated);

        HttpHeaders header = new HttpHeaders();
        header.put(HttpHeaders.SET_COOKIE, Arrays.asList("test=test"));
        ResponseEntity<Map> responseMap = new ResponseEntity<>(HttpStatusCode.valueOf(HttpStatus.OK.value()));

        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.eq(Map.class));
        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        updated = schedulerHistoryServiceMock.updateSettlementScheduler(1, 1);
        Assertions.assertFalse(updated);

        UriComponentsBuilder uriComponentsBuilder = Mockito.mock(UriComponentsBuilder.class);
        MockedStatic<UriComponentsBuilder> uriComponentsBuilderMockedStatic = Mockito.mockStatic(UriComponentsBuilder.class);

        uriComponentsBuilderMockedStatic.when(() -> {
            UriComponentsBuilder.fromHttpUrl(Mockito.anyString());
        }).thenReturn(uriComponentsBuilder);
        Mockito.doReturn(uriComponentsBuilder).when(uriComponentsBuilder).queryParams(Mockito.any(MultiValueMap.class));
        Mockito.doReturn(uriComponents).when(uriComponentsBuilder).build();
        Mockito.doReturn("test").when(uriComponents).toString();

        responseMap = new ResponseEntity<>(header, HttpStatusCode.valueOf(HttpStatus.OK.value()));
        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.anyMap(), Mockito.eq(Map.class));
        Mockito.doReturn(null).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        updated = schedulerHistoryServiceMock.updateSettlementScheduler(1, 1);
        Assertions.assertFalse(updated);

        Map<String, Object> bodyMap = new HashMap<>();
        LocalDateTime currentDateTime = LocalDateTime.now();
        XxlJobInfoDTO xxlJobInfo = new XxlJobInfoDTO();

        xxlJobInfo.setAuthor(jobAuthorPrefix + 1);
        xxlJobInfo.setExecutorBlockStrategy(executorBlockStrategy);
        xxlJobInfo.setExecutorFailRetryCount(executorFailRetryCount);
        xxlJobInfo.setExecutorHandler(executorHandler);
        xxlJobInfo.setExecutorParam("1");
        xxlJobInfo.setExecutorRouteStrategy(executorRouteStrategy);
        xxlJobInfo.setExecutorTimeout(0);
        xxlJobInfo.setGlueType(glueType);
        xxlJobInfo.setJobDesc(jobDescriptionPrefix + 1);
        xxlJobInfo.setJobGroup(groupId);
        xxlJobInfo.setMisfireStrategy(misfireStrategy);
        xxlJobInfo.setScheduleConf(currentDateTime.getSecond() + " " + currentDateTime.getMinute() + " " + 1 + " * * ?");
        xxlJobInfo.setScheduleType(scheduleType);
        xxlJobInfo.setTriggerStatus(1);

        bodyMap.put("code", 200);
        bodyMap.put("data", Arrays.asList(xxlJobInfo));
        responseMap = new ResponseEntity<>(bodyMap, header, HttpStatusCode.valueOf(HttpStatus.OK.value()));
        Mockito.doReturn(responseMap).when(restTemplateMock).postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(Map.class));
        updated = schedulerHistoryServiceMock.updateSettlementScheduler(1, 1);
        Assertions.assertTrue(updated);

        uriComponentsBuilderMockedStatic.clearInvocations();
        uriComponentsBuilderMockedStatic.reset();
        uriComponentsBuilderMockedStatic.close();
    }

    @Test
    public void testExecuteJobHandler() throws Exception {
        MerchantSettlementJobHandler merchantSettlementJobHandler = new MerchantSettlementJobHandler();
        ReflectionTestUtils.setField(merchantSettlementJobHandler, "schedulerHistoryService", schedulerHistoryService);
        MerchantSettlementJobHandler merchantSettlementJobHandlerMock = Mockito.spy(merchantSettlementJobHandler);
        MockedStatic<XxlJobHelper> xxlJobHelperMockedStatic = Mockito.mockStatic(XxlJobHelper.class);

        xxlJobHelperMockedStatic.when(() -> {
            XxlJobHelper.getJobParam();
        }).thenReturn("1");
        Mockito.when(schedulerHistoryService.startSettlementScheduledJob(Mockito.anyInt(), Mockito.any(LocalDateTime.class))).thenReturn(true);
        merchantSettlementJobHandlerMock.execute();

        xxlJobHelperMockedStatic.when(() -> {
            XxlJobHelper.getJobParam();
        }).thenReturn(null);
        merchantSettlementJobHandlerMock.execute();

        xxlJobHelperMockedStatic.clearInvocations();
        xxlJobHelperMockedStatic.reset();
        xxlJobHelperMockedStatic.close();
    }
}
