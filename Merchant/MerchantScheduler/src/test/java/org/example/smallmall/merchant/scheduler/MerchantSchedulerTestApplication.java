package org.example.smallmall.merchant.scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantSchedulerTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(MerchantSchedulerTestApplication.class, args);
    }
}