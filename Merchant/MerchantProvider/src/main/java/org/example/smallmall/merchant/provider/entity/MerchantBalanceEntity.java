package org.example.smallmall.merchant.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_merchant_balance")
@Data
public class MerchantBalanceEntity implements Serializable {
    private static final long serialVersionUID = -309141737708171992L;

    private int id;
    private int merchantId;
    private BigDecimal balance;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}