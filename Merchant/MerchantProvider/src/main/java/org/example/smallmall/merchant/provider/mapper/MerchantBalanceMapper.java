package org.example.smallmall.merchant.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.merchant.provider.entity.MerchantBalanceEntity;

@Mapper
public interface MerchantBalanceMapper extends BaseMapper<MerchantBalanceEntity> {
}