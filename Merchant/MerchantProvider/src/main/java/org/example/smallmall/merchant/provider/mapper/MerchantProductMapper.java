package org.example.smallmall.merchant.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface MerchantProductMapper extends BaseMapper<MerchantProductEntity> {
    public Integer addNewProduct(MerchantProductEntity product);
    public Integer updateProductQuantity(@Param("merchantId") Integer merchantId, @Param("productId") Integer productId, @Param("count") Integer count, @Param("updateTime") LocalDateTime updateTime);
    public IPage<Map<String, Object>> getProductList(@Param("productId") Integer productId, @Param("merchantId") Integer merchantId, IPage<Map<String, Object>> page);
}