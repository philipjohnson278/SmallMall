package org.example.smallmall.merchant.provider.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantProductMapper;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class MerchantProductDAO {
    @Autowired
    private MerchantProductMapper productMapper;

    public boolean addProduct(MerchantProductEntity merchantProduct) {
        Integer addedRow = productMapper.addNewProduct(merchantProduct);
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();

        if (addedRow == null || addedRow <= 0) {
            try {
                log.error("Add a new product failed, product info: {}", objectMapper.writeValueAsString(merchantProduct));
            } catch (JsonProcessingException e) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            return false;
        }

        try {
            log.info("Add a new product of merchant, product ino: {}, affected row: {}", objectMapper.writeValueAsString(merchantProduct), addedRow);
        } catch (JsonProcessingException e) {
            log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
        }

        return true;
    }

    public boolean increaseQuantity(MerchantIncreaseProductQuantityRequestVO increaseProductRequestVO, LocalDateTime updateTime) {
        Integer merchantId = increaseProductRequestVO.getMerchantId();
        Integer productId = increaseProductRequestVO.getProductId();
        Integer count = increaseProductRequestVO.getCount();
        Integer updatedRow = productMapper.updateProductQuantity(merchantId, productId, count, updateTime);

        if (updatedRow == null || updatedRow <= 0) {
            log.error("Increase product quantity failed, merchant ID: {}, product ID: {}, count: {}", merchantId, productId, count);

            return false;
        }

        log.info("Update quantity of product, merchant ID: {}, product ID: {}, count: {}, affected row: {}", merchantId, productId, count, updatedRow);

        return true;
    }

    public Map<String, Object> getProduct(Integer productId, Integer merchantId, Integer pageNumber, Integer pageSize) {
        IPage<Map<String, Object>> page = new Page<>(pageNumber, pageSize);
        IPage<Map<String, Object>> productInfoPage = productMapper.getProductList(productId, merchantId, page);

        if (productInfoPage == null || productInfoPage.getRecords() == null) {
            log.error("No merchant product found for query argument: product ID: {}, merchant ID: {}, pageNumber: {}, pageSize: {}", productId, merchantId, pageNumber, pageSize);

            return null;
        }

        Map<String, Object> resultMap = new HashMap<>();
        List<Map<String, Object>> productInfoList = productInfoPage.getRecords();

        resultMap.put("list", productInfoList);
        resultMap.put("totalPages", productInfoPage.getPages());
        resultMap.put("currentPage", productInfoPage.getCurrent());
        resultMap.put("pageSize", productInfoList.size());

        return resultMap;
    }
}