package org.example.smallmall.merchant.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.provider.entity.MerchantHistoryEntity;

import java.util.Map;

public interface MerchantHistoryService extends IService<MerchantHistoryEntity> {
    public Map<String, Object> getMerchantHistory(Integer merchantId, Integer pageNumber, Integer pageSize);
}