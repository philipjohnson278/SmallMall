package org.example.smallmall.merchant.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.merchant.provider.entity.MerchantHistoryEntity;

@Mapper
public interface MerchantHistoryMapper extends BaseMapper<MerchantHistoryEntity> {
}