package org.example.smallmall.merchant.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.provider.entity.MerchantSalesEntity;

import java.util.Map;

public interface MerchantSalesService extends IService<MerchantSalesEntity> {
    public Map<String, Object> getSales(Integer merchantId, Integer pageNumber, Integer pageSize);
}