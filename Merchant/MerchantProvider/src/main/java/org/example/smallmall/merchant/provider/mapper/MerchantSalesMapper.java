package org.example.smallmall.merchant.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.smallmall.merchant.provider.entity.MerchantSalesEntity;

@Mapper
public interface MerchantSalesMapper extends BaseMapper<MerchantSalesEntity> {
}