package org.example.smallmall.merchant.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_merchant_history")
@Data
public class MerchantHistoryEntity implements Serializable {
    private static final long serialVersionUID = 2611865270097054480L;

    private int id;
    private int merchantId;
    private String operation;
    private int productId;
    private String productName;
    private String productSku;
    private String productImage;
    private String productDescription;
    private int productQuantity;
    private BigDecimal productPrice;
    private String remark;
    private LocalDateTime createTime;
}