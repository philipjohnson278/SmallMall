package org.example.smallmall.merchant.provider.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.example.smallmall.merchant.provider.entity.MerchantBalanceEntity;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;
import org.example.smallmall.merchant.provider.service.MerchantBalanceService;
import org.example.smallmall.merchant.provider.service.MerchantHistoryService;
import org.example.smallmall.merchant.provider.service.MerchantProductService;
import org.example.smallmall.merchant.provider.service.MerchantSalesService;
import org.example.smallmall.merchant.scheduler.service.SchedulerHistoryService;
import org.example.smallmall.vo.ResponseVO;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MerchantProviderController {
    @Value("${query-config.paging.default.number}")
    private Integer defaultPageNumber;

    @Value("${query-config.paging.default.size}")
    private Integer defaultPageSize;

    @Autowired
    private MerchantBalanceService merchantBalanceService;

    @Autowired
    private MerchantHistoryService merchantHistoryService;

    @Autowired
    private MerchantProductService merchantProductService;

    @Autowired
    private MerchantSalesService merchantSalesService;

    @Autowired
    private SchedulerHistoryService schedulerHistoryService;

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity<String> index() {
        String hello = "<html><head><title>Merchant Service Provider</title></head><body><h1>Merchant Service Provider</h1></body></html>";

        return new ResponseEntity<>(hello, HttpStatus.OK);
    }

    @GetMapping(value = "/sales/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getSales(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                               @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> resultMap = merchantSalesService.getSales(merchantId, pageNumber, pageSize);

        if (resultMap == null || resultMap.size() == 0) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @GetMapping(value = "/history/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getOperationHistory(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                                          @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                          @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> resultMap = merchantHistoryService.getMerchantHistory(merchantId, pageNumber, pageSize);

        if (resultMap == null || resultMap.size() == 0) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @GetMapping(value = "/balance/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getBalance(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId) {
        ResponseVO responseVO = new ResponseVO();
        MerchantBalanceEntity merchantBalance = merchantBalanceService.getMerchantBalance(merchantId);

        if (merchantBalance == null) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage(HttpStatus.NOT_FOUND.toString());
        } else {
            Map<String, Object> resultMap = new HashMap<>();

            resultMap.put("balance", merchantBalance.getBalance());
            resultMap.put("updateTime", merchantBalance.getUpdateTime());
            responseVO.setData(resultMap);
            responseVO.setStatus(HttpStatus.OK.value());
            responseVO.setMessage(HttpStatus.OK.toString());
        }

        return responseVO;
    }

    @GetMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getAllProduct(@Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                    @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> resultMap = merchantProductService.getProduct(null, null, pageNumber, pageSize);

        if (resultMap == null || resultMap.size() == 0) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @GetMapping(value = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getProduct(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("productId") Integer productId,
                                 @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                 @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = 1;
        Integer pageSize = 1;
        ResponseVO responseVO = new ResponseVO();

        Map<String, Object> resultMap = merchantProductService.getProduct(productId, null, pageNumber, pageSize);

        if (resultMap == null || resultMap.size() == 0) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        List<MerchantProductEntity> productList = (List<MerchantProductEntity>) resultMap.get("list");

        resultMap.clear();
        resultMap.put("product", productList.get(0));
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @GetMapping(value = "/product/merchant/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getMerchantProduct(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                                         @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                         @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> resultMap = merchantProductService.getProduct(null, merchantId, pageNumber, pageSize);

        if (resultMap == null || resultMap.size() == 0) {
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());
            responseVO.setMessage("No query result found!");

            return responseVO;
        }

        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PostMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO addProduct(@Valid @NotNull @RequestBody MerchantAddProductRequestVO addProductRequestVO) {
        ResponseVO responseVO = new ResponseVO();
        boolean added = merchantProductService.addNewProduct(addProductRequestVO);

        if (!added) {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Add a new product failed!");

            return responseVO;
        }

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("result", added);
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PatchMapping(value = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO updateProduct(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("productId") Integer productId,
                                    @Valid @NotNull @RequestBody MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO) {
        ResponseVO responseVO = new ResponseVO();

        if (productId != increaseProductQuantityRequestVO.getProductId()) {
            responseVO.setStatus(HttpStatus.FORBIDDEN.value());
            responseVO.setMessage("Product ID not matched!");

            return responseVO;
        }

        boolean increased = merchantProductService.increaseProductQuantity(increaseProductQuantityRequestVO, LocalDateTime.now());

        if (!increased) {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Update product detail failed!");

            return responseVO;
        }

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("result", increased);
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @GetMapping(value = "/scheduler/settlement/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO getMerchantSettlement(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId,
                                            @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "page", required = false) Integer page,
                                            @Min(1) @Max(Integer.MAX_VALUE) @RequestParam(value = "size", required = false) Integer size) {
        Integer pageNumber = defaultPageNumber;
        Integer pageSize = defaultPageSize;
        ResponseVO responseVO = new ResponseVO();

        if (page != null) {
            pageNumber = page;
        }

        if (size != null) {
            pageSize = size;
        }

        Map<String, Object> schedulerHistoryMap = schedulerHistoryService.getSettlementSchedulerHistory(merchantId, pageNumber, pageSize);

        if (schedulerHistoryMap == null || schedulerHistoryMap.isEmpty()) {
            responseVO.setMessage("No query result found!");
            responseVO.setStatus(HttpStatus.NOT_FOUND.value());

            return responseVO;
        }

        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(schedulerHistoryMap);
        responseVO.setStatus(HttpStatus.OK.value());

        return responseVO;
    }

    @PostMapping(value = "/scheduler/settlement/{merchantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO runMerchantSettlementScheduler(@NotNull @Min(1) @Max(Integer.MAX_VALUE) @PathVariable("merchantId") Integer merchantId) {
        ResponseVO responseVO = new ResponseVO();
        boolean executed = schedulerHistoryService.startSettlementScheduledJob(merchantId, LocalDateTime.now());

        if (!executed) {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Run settlement job failed!");

            return responseVO;
        }

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("result", executed);
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PostMapping(value = "/scheduler/settlement", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO createMerchantSettlementScheduler(@Valid @NotNull @RequestBody MerchantUpdateSettlementSchedulerRequestVO createSettlementSchedulerRequestVO) {
        ResponseVO responseVO = new ResponseVO();
        boolean created = schedulerHistoryService.createSettlementScheduler(createSettlementSchedulerRequestVO.getMerchantId(), createSettlementSchedulerRequestVO.getHourOfDay());

        if (!created) {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Create settlement job for merchant failed!");

            return responseVO;
        }

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("result", created);
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }

    @PatchMapping(value = "/scheduler/settlement", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVO updateMerchantSettlementScheduler(@Valid @NotNull @RequestBody MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO) {
        Integer merchantId = updateSettlementSchedulerRequestVO.getMerchantId();
        Integer hourOfDay = updateSettlementSchedulerRequestVO.getHourOfDay();
        ResponseVO responseVO = new ResponseVO();
        boolean updated = schedulerHistoryService.updateSettlementScheduler(merchantId, hourOfDay);

        if (!updated) {
            responseVO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            responseVO.setMessage("Update settlement job failed!");

            return responseVO;
        }

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("result", updated);
        responseVO.setStatus(HttpStatus.OK.value());
        responseVO.setMessage(HttpStatus.OK.toString());
        responseVO.setData(resultMap);

        return responseVO;
    }
}