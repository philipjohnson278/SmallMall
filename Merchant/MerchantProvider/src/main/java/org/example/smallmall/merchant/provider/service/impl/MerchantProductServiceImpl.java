package org.example.smallmall.merchant.provider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.provider.dao.MerchantProductDAO;
import org.example.smallmall.merchant.provider.entity.MerchantHistoryEntity;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantProductMapper;
import org.example.smallmall.merchant.provider.service.MerchantHistoryService;
import org.example.smallmall.merchant.provider.service.MerchantProductService;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@Service
public class MerchantProductServiceImpl extends ServiceImpl<MerchantProductMapper, MerchantProductEntity> implements MerchantProductService {
    @Autowired
    private MerchantProductDAO merchantProductDAO;

    @Autowired
    private MerchantHistoryService merchantHistoryService;

    @Override
    public boolean addNewProduct(MerchantAddProductRequestVO addProductRequestVO) {
        boolean added = false;
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        MerchantProductEntity merchantProduct = null;

        try {
            merchantProduct = objectMapper.convertValue(addProductRequestVO, MerchantProductEntity.class);
            merchantProduct.setState((byte) 1);
            merchantProduct.setCreateTime(LocalDateTime.now());

            added = merchantProductDAO.addProduct(merchantProduct);
        } catch (Exception e) {
            try {
                log.error("Add a new product failed by exception, product info: {}, exception: {}", objectMapper.writeValueAsString(merchantProduct), e.getMessage());
            } catch (JsonProcessingException ex) {
                log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", e.getMessage());
            }

            return added;
        }

        try {
            log.info("Add a new product, product info: {}, result: {}", objectMapper.writeValueAsString(merchantProduct), added);
        } catch (JsonProcessingException ex) {
            log.error("Jackson ObjectMapper writeValueAsString failed by exception: {}", ex.getMessage());
        }

        if (added) {
            MerchantHistoryEntity merchantHistory = new MerchantHistoryEntity();

            merchantHistory.setMerchantId(merchantProduct.getMerchantId());
            merchantHistory.setProductId(merchantProduct.getId());
            merchantHistory.setProductName(merchantProduct.getProductName());
            merchantHistory.setProductImage(merchantProduct.getProductImage());
            merchantHistory.setProductDescription(merchantProduct.getProductDescription());
            merchantHistory.setProductPrice(merchantProduct.getProductPrice());
            merchantHistory.setProductQuantity(merchantProduct.getProductQuantity());
            merchantHistory.setProductSku(merchantProduct.getProductSku());
            merchantHistory.setOperation("ADD_PRODUCT");
            merchantHistory.setCreateTime(merchantProduct.getCreateTime());
            merchantHistoryService.save(merchantHistory);
        }

        return added;
    }

    @Override
    public boolean increaseProductQuantity(MerchantIncreaseProductQuantityRequestVO increaseProductRequestVO, LocalDateTime updateTime) {
        MerchantProductEntity productInfo = this.getById(increaseProductRequestVO.getProductId());

        if (productInfo == null) {
            log.error("Increase product failed, product not found, request info: merchant ID: {}, product ID: {}, count: {}", increaseProductRequestVO.getMerchantId(), increaseProductRequestVO.getProductId(), increaseProductRequestVO.getCount());

            return false;
        }

        int merchantId = productInfo.getMerchantId();
        int productQuantity = productInfo.getProductQuantity();
        int productId = productInfo.getId();

        if (productInfo.getProductQuantity() >= increaseProductRequestVO.getCount()) {
            log.error("Increase product failed, requested quantity <= exists quantity, exists quantity: {}, merchant ID: {}, product ID: {}, new quantity: {}", productQuantity, merchantId, productId, increaseProductRequestVO.getCount());

            return false;
        }

        boolean increased = merchantProductDAO.increaseQuantity(increaseProductRequestVO, updateTime);

        if (increased) {
            MerchantHistoryEntity merchantHistory = new MerchantHistoryEntity();

            merchantHistory.setProductImage(productInfo.getProductImage());
            merchantHistory.setProductDescription(productInfo.getProductDescription());
            merchantHistory.setMerchantId(productInfo.getMerchantId());
            merchantHistory.setProductId(productInfo.getId());
            merchantHistory.setProductName(productInfo.getProductName());
            merchantHistory.setProductPrice(productInfo.getProductPrice());
            merchantHistory.setProductQuantity(increaseProductRequestVO.getCount());
            merchantHistory.setProductSku(productInfo.getProductSku());
            merchantHistory.setOperation("UPDATE_PRODUCT");
            merchantHistory.setCreateTime(updateTime);
            merchantHistoryService.save(merchantHistory);
        }

        return increased;
    }

    @Override
    public Map<String, Object> getProduct(Integer productId, Integer merchantId, Integer pageNumber, Integer pageSize) {
        Map<String, Object> resultMap = merchantProductDAO.getProduct(productId, merchantId, pageNumber, pageSize);

        return resultMap;
    }
}