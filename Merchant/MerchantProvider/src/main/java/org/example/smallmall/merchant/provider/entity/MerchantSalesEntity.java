package org.example.smallmall.merchant.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_merchant_sales")
@Data
public class MerchantSalesEntity implements Serializable {
    private static final long serialVersionUID = 6580102692298643639L;

    private int id;
    private int merchantId;
    private int customerId;
    private int productId;
    private String productName;
    private String productSku;
    private String productImage;
    private String productDescription;
    private int productQuantity;
    private BigDecimal productPrice;
    private BigDecimal productAmount;
    private byte state;
    private String remark;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}