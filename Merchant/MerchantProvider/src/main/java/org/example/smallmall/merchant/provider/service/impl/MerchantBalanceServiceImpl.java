package org.example.smallmall.merchant.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.smallmall.merchant.provider.entity.MerchantBalanceEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantBalanceMapper;
import org.example.smallmall.merchant.provider.service.MerchantBalanceService;
import org.springframework.stereotype.Service;

@Service
public class MerchantBalanceServiceImpl extends ServiceImpl<MerchantBalanceMapper, MerchantBalanceEntity> implements MerchantBalanceService {
    @Override
    public MerchantBalanceEntity getMerchantBalance(int merchantId) {
        LambdaQueryWrapper<MerchantBalanceEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(MerchantBalanceEntity.class).eq(MerchantBalanceEntity::getMerchantId, merchantId);
        MerchantBalanceEntity merchantBalance = this.getOne(lambdaQueryWrapper);

        return merchantBalance;
    }
}