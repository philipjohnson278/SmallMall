package org.example.smallmall.merchant.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.provider.entity.MerchantHistoryEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantHistoryMapper;
import org.example.smallmall.merchant.provider.service.MerchantHistoryService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MerchantHistoryServiceImpl extends ServiceImpl<MerchantHistoryMapper, MerchantHistoryEntity> implements MerchantHistoryService {
    @Override
    public Map<String, Object> getMerchantHistory(Integer merchantId, Integer pageNumber, Integer pageSize) {
        LambdaQueryWrapper<MerchantHistoryEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(MerchantHistoryEntity.class)
                                                                               .eq(MerchantHistoryEntity::getMerchantId, merchantId)
                                                                               .orderByDesc(MerchantHistoryEntity::getCreateTime);
        Page<MerchantHistoryEntity> merchantHistoryEntityPage = this.page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);

        if (merchantHistoryEntityPage == null || merchantHistoryEntityPage.getRecords() == null) {
            log.warn("No merchant history found for query argument: merchant ID: {}, pageNumber: {}, pageSize: {}", merchantId, pageNumber, pageSize);

            return null;
        }

        List<MerchantHistoryEntity> historyList = merchantHistoryEntityPage.getRecords();
        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("list", historyList);
        resultMap.put("totalPages", merchantHistoryEntityPage.getPages() + 1);
        resultMap.put("currentPage", merchantHistoryEntityPage.getCurrent());
        resultMap.put("pageSize", historyList.size());

        return resultMap;
    }
}