package org.example.smallmall.merchant.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.smallmall.merchant.provider.entity.MerchantSalesEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantSalesMapper;
import org.example.smallmall.merchant.provider.service.MerchantSalesService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MerchantSalesServiceImpl extends ServiceImpl<MerchantSalesMapper, MerchantSalesEntity> implements MerchantSalesService {
    @Override
    public Map<String, Object> getSales(Integer merchantId, Integer pageNumber, Integer pageSize) {
        LambdaQueryWrapper<MerchantSalesEntity> lambdaQueryWrapper = Wrappers.lambdaQuery(MerchantSalesEntity.class)
                                                                             .eq(MerchantSalesEntity::getMerchantId, merchantId)
                                                                             .orderByDesc(MerchantSalesEntity::getCreateTime);
        Page<MerchantSalesEntity> productSalesyPage = this.page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);

        if (productSalesyPage == null || productSalesyPage.getRecords() == null) {
            log.error("No merchant sales found for query argument: merchant ID: {}, pageNumber: {}, pageSize: {}", merchantId, pageNumber, pageSize);

            return null;
        }

        Map<String, Object> resultMap = new HashMap<>();
        List<MerchantSalesEntity> orderList = productSalesyPage.getRecords();

        resultMap.put("list", orderList);
        resultMap.put("totalPages", productSalesyPage.getPages() + 1);
        resultMap.put("currentPage", productSalesyPage.getCurrent());
        resultMap.put("pageSize", orderList.size());

        return resultMap;
    }
}