package org.example.smallmall.merchant.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"org.example.smallmall.merchant"})
@MapperScan(basePackages = {"org.example.smallmall.merchant.authenticate.mapper", "org.example.smallmall.merchant.provider.mapper", "org.example.smallmall.merchant.scheduler.mapper"})
public class MerchantProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(MerchantProviderApplication.class, args);
    }
}