package org.example.smallmall.merchant.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@TableName("tbl_merchant_product")
@Data
public class MerchantProductEntity implements Serializable {
    private static final long serialVersionUID = 3922620880013213115L;

    private int id;
    private int merchantId;
    private String productName;
    private String productSku;
    private String productImage;
    private String productDescription;
    private int productQuantity;
    private BigDecimal productPrice;
    private byte state;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}