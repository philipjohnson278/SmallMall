package org.example.smallmall.merchant.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;

import java.time.LocalDateTime;
import java.util.Map;

public interface MerchantProductService extends IService<MerchantProductEntity> {
    public boolean addNewProduct(MerchantAddProductRequestVO addProductRequestVO);
    public boolean increaseProductQuantity(MerchantIncreaseProductQuantityRequestVO increaseProductRequestVO, LocalDateTime updateTime);
    public Map<String, Object> getProduct(Integer productId, Integer merchantId, Integer pageNumber, Integer pageSize);
}