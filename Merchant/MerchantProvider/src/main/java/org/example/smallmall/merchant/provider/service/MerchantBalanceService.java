package org.example.smallmall.merchant.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.smallmall.merchant.provider.entity.MerchantBalanceEntity;

public interface MerchantBalanceService extends IService<MerchantBalanceEntity> {
    public MerchantBalanceEntity getMerchantBalance(int merchantId);
}