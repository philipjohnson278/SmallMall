#!/bin/bash

cd `dirname $0`

BIN_DIR=`pwd`

cd ..

DEPLOY_DIR=`pwd`
CONF_DIR=$DEPLOY_DIR/config

LOGS_DIR=$DEPLOY_DIR/logs

if [ ! -d $LOGS_DIR ]; then
    mkdir $LOGS_DIR
fi

LIB_DIR=$DEPLOY_DIR/lib
LIB_JARS=`ls $LIB_DIR|grep .jar|awk '{print "'$LIB_DIR'/"$0}'|tr "\n" ":"`

JAVA_OPTS="-Duser.timezone=Asia/Shanghai"
JAVA_DEBUG_OPTS=""

if [ "$1" = "debug" ]; then
    JAVA_DEBUG_OPTS=" -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"
fi

JAVA_JMX_OPTS=""

if [ "$1" = "jmx" ]; then
    JAVA_JMX_OPTS=" -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
fi

JAVA_MEM_OPTS="-server -Xms512m -Xmx512m -XX:-OmitStackTraceInFastThrow -XX:+UseG1GC -XX:ConcGCThreads=2 -XX:ParallelGCThreads=4 -XX:+UnlockExperimentalVMOptions -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90 -Dsun.zip.disableMemoryMapping=true"

echo -e "Starting the application ...\c"
nohup java $JAVA_OPTS $JAVA_MEM_OPTS $JAVA_DEBUG_OPTS $JAVA_JMX_OPTS -classpath $CONF_DIR:$LIB_JARS -jar "${1}" & > /dev/null

COUNT=0

while [ $COUNT -lt 1 ]; do
    echo -e ".\c"
    sleep 1

    COUNT=`ps -ef | grep java | grep "$DEPLOY_DIR" | awk '{print $2}' | wc -l`

    if [ $COUNT -gt 0 ]; then
        break
    else
    	echo "Failed!"
    	exit -1
    fi
done

echo "OK!"

PIDS=`ps -ef | grep java | grep "$DEPLOY_DIR" | awk '{print $2}'`

echo "PID: $PIDS"