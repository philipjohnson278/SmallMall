package org.example.smallmall.merchant.provider;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.smallmall.merchant.provider.dao.MerchantProductDAO;
import org.example.smallmall.merchant.provider.entity.MerchantBalanceEntity;
import org.example.smallmall.merchant.provider.entity.MerchantHistoryEntity;
import org.example.smallmall.merchant.provider.entity.MerchantProductEntity;
import org.example.smallmall.merchant.provider.entity.MerchantSalesEntity;
import org.example.smallmall.merchant.provider.mapper.MerchantProductMapper;
import org.example.smallmall.merchant.provider.service.MerchantBalanceService;
import org.example.smallmall.merchant.provider.service.MerchantHistoryService;
import org.example.smallmall.merchant.provider.service.MerchantProductService;
import org.example.smallmall.merchant.provider.service.MerchantSalesService;
import org.example.smallmall.merchant.provider.service.impl.MerchantBalanceServiceImpl;
import org.example.smallmall.merchant.provider.service.impl.MerchantHistoryServiceImpl;
import org.example.smallmall.merchant.provider.service.impl.MerchantProductServiceImpl;
import org.example.smallmall.merchant.provider.service.impl.MerchantSalesServiceImpl;
import org.example.smallmall.merchant.scheduler.service.SchedulerHistoryService;
import org.example.smallmall.vo.merchant.request.MerchantAddProductRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantIncreaseProductQuantityRequestVO;
import org.example.smallmall.vo.merchant.request.MerchantUpdateSettlementSchedulerRequestVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchantProviderApplicationTests {
    @Value("${query-config.paging.default.number}")
    private Integer defaultPageNumber;

    @Value("${query-config.paging.default.size}")
    private Integer defaultPageSize;

    @Mock
    BaseMapper<MerchantProductEntity> baseProductMapper;

    @MockBean
    private MerchantProductMapper productMapper;

    @MockBean
    private MerchantBalanceService merchantBalanceService;

    @MockBean
    private MerchantHistoryService merchantHistoryService;

    @MockBean
    private MerchantProductService merchantProductService;

    @MockBean
    private MerchantSalesService merchantSalesService;

    @MockBean
    private SchedulerHistoryService schedulerHistoryService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void testGetSales() throws Exception {
        MerchantSalesEntity merchantSales = new MerchantSalesEntity();
        List<MerchantSalesEntity> merchantSalesList = new ArrayList<>();
        Page<MerchantSalesEntity> merchantSalesPage = new Page<>();

        merchantSalesPage.setCurrent(1);
        merchantSalesPage.setPages(1);
        merchantSalesPage.setSize(1);
        merchantSalesPage.setTotal(1);
        merchantSalesList.add(merchantSales);
        merchantSalesPage.setRecords(merchantSalesList);

        MerchantSalesServiceImpl merchantSalesServiceImpl = new MerchantSalesServiceImpl();
        MerchantSalesService merchantSalesServiceMock = Mockito.spy(merchantSalesServiceImpl);
        Mockito.doReturn(merchantSalesPage).when(merchantSalesServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        Map<String, Object> salesMap = merchantSalesServiceMock.getSales(1, 1, 1);
        Assertions.assertFalse(salesMap.isEmpty());

        Mockito.doReturn(null).when(merchantSalesServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        salesMap = merchantSalesServiceMock.getSales(1, 1, 1);
        Assertions.assertNull(salesMap);

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);

        Mockito.when(merchantSalesService.getSales(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/0")).andExpect(status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/1")).andExpect(status().isOk());

        Mockito.when(merchantSalesService.getSales(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sales/merchant/1")).andExpect(status().isOk());
    }

    @Test
    public void testGetOperationHistory() throws Exception {
        MerchantHistoryServiceImpl merchantHistoryServiceImpl = new MerchantHistoryServiceImpl();
        MerchantHistoryService merchantHistoryServiceMock = Mockito.spy(merchantHistoryServiceImpl);

        Mockito.doReturn(null).when(merchantHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        Map<String, Object> merchantHistoryMap = merchantHistoryServiceMock.getMerchantHistory(1, 1, 1);
        Assertions.assertNull(merchantHistoryMap);

        MerchantHistoryEntity merchantHistory = new MerchantHistoryEntity();
        List<MerchantHistoryEntity> merchantHistoryList = new ArrayList<>();
        Page<MerchantHistoryEntity> merchantHistoryPage = new Page<>();

        merchantHistoryPage.setCurrent(1);
        merchantHistoryPage.setPages(1);
        merchantHistoryPage.setSize(1);
        merchantHistoryPage.setTotal(1);
        merchantHistoryList.add(merchantHistory);
        merchantHistoryPage.setRecords(merchantHistoryList);

        Mockito.doReturn(merchantHistoryPage).when(merchantHistoryServiceMock).page(Mockito.any(Page.class), Mockito.any(LambdaQueryWrapper.class));
        merchantHistoryMap = merchantHistoryServiceMock.getMerchantHistory(1, 1, 1);
        Assertions.assertFalse(merchantHistoryMap.isEmpty());

        Mockito.when(merchantHistoryService.getMerchantHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/merchant/1")).andExpect(status().isOk());

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);

        Mockito.when(merchantHistoryService.getMerchantHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/merchant/1")).andExpect(status().isOk());
    }

    @Test
    public void testGetBalance() throws Exception {
        MerchantBalanceEntity merchantBalance = null;
        MerchantBalanceServiceImpl merchantBalanceServiceImpl = new MerchantBalanceServiceImpl();
        MerchantBalanceService merchantBalanceServiceMock = Mockito.spy(merchantBalanceServiceImpl);

        Mockito.doReturn(merchantBalance).when(merchantBalanceServiceMock).getOne(Mockito.any(LambdaQueryWrapper.class));
        MerchantBalanceEntity merchantBalanceMock = merchantBalanceServiceMock.getMerchantBalance(1);
        Assertions.assertNull(merchantBalanceMock);

        Mockito.when(merchantBalanceService.getMerchantBalance(Mockito.anyInt())).thenReturn(merchantBalance);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/balance/merchant/0")).andExpect(status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/balance/merchant/1")).andExpect(status().isOk());

        merchantBalance = new MerchantBalanceEntity();
        merchantBalance.setId(1);
        merchantBalance.setMerchantId(1);
        merchantBalance.setBalance(new BigDecimal("3.14"));
        merchantBalance.setCreateTime(LocalDateTime.now());
        Mockito.when(merchantBalanceService.getMerchantBalance(Mockito.anyInt())).thenReturn(merchantBalance);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/balance/merchant/1")).andExpect(status().isOk());

        Mockito.doReturn(merchantBalance).when(merchantBalanceServiceMock).getOne(Mockito.any(LambdaQueryWrapper.class));
        merchantBalanceMock = merchantBalanceServiceMock.getMerchantBalance(1);
        Assertions.assertNotNull(merchantBalanceMock);
    }

    @Test
    public void testGetAllProduct() throws Exception {
        Mockito.doReturn(null).when(productMapper).getProductList(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(Page.class));

        MerchantProductDAO merchantProductDAO = new MerchantProductDAO();
        ReflectionTestUtils.setField(merchantProductDAO, "productMapper",  productMapper);
        MerchantProductDAO merchantProductDAOMock = Mockito.spy(merchantProductDAO);
        MerchantProductServiceImpl merchantProductServiceImpl = new MerchantProductServiceImpl();

        ReflectionTestUtils.setField(merchantProductServiceImpl, "merchantProductDAO",  merchantProductDAOMock);
        MerchantProductService merchantProductServiceMock = Mockito.spy(merchantProductServiceImpl);

        Map<String, Object> productMap = merchantProductServiceMock.getProduct(1, 1, 1, 1);
        Assertions.assertNull(productMap);

        Map<String, Object> productDetailMap = new HashMap<>();
        List<Map<String, Object>> merchantProductList = new ArrayList<>();
        Page<Map<String, Object>> merchantProductPage = new Page<>();

        merchantProductPage.setCurrent(1);
        merchantProductPage.setPages(1);
        merchantProductPage.setSize(1);
        merchantProductPage.setTotal(1);
        merchantProductList.add(productDetailMap);
        merchantProductPage.setRecords(merchantProductList);

        Mockito.doReturn(merchantProductPage).when(productMapper).getProductList(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(Page.class));
        productMap = merchantProductServiceMock.getProduct(1, 1, 1, 1);
        Assertions.assertFalse(productMap.isEmpty());

        Map<String, Object> resultMap = new HashMap<>();
        MerchantProductEntity merchantProduct = new MerchantProductEntity();
        List<MerchantProductEntity> productList = new ArrayList<>();

        productList.add(merchantProduct);
        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);
        resultMap.put("list", productList);

        Mockito.when(merchantProductService.getProduct(null, null, defaultPageNumber, defaultPageSize)).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product")).andExpect(status().isOk());

        Mockito.when(merchantProductService.getProduct(1, null, 1, 1)).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/0")).andExpect(status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/1")).andExpect(status().isOk());

        Mockito.when(merchantProductService.getProduct(null, 1, defaultPageNumber, defaultPageSize)).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/merchant/0")).andExpect(status().isBadRequest());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/merchant/1")).andExpect(status().isOk());

        Mockito.when(merchantProductService.getProduct(null, null, defaultPageNumber, defaultPageSize)).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product")).andExpect(status().isOk());

        Mockito.when(merchantProductService.getProduct(1, null, 1, 1)).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/1")).andExpect(status().isOk());

        Mockito.when(merchantProductService.getProduct(null, 1, defaultPageNumber, defaultPageSize)).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/merchant/1")).andExpect(status().isOk());
    }

    @Test
    public void testAddProduct() throws Exception {
        Mockito.doReturn(null).when(productMapper).addNewProduct(Mockito.any(MerchantProductEntity.class));

        MerchantProductDAO merchantProductDAO = new MerchantProductDAO();
        ReflectionTestUtils.setField(merchantProductDAO, "productMapper",  productMapper);
        MerchantProductDAO merchantProductDAOMock = Mockito.spy(merchantProductDAO);
        MerchantProductServiceImpl merchantProductServiceImpl = new MerchantProductServiceImpl();

        ReflectionTestUtils.setField(merchantProductServiceImpl, "merchantProductDAO",  merchantProductDAOMock);
        ReflectionTestUtils.setField(merchantProductServiceImpl, "merchantHistoryService",  merchantHistoryService);
        MerchantProductService merchantProductServiceMock = Mockito.spy(merchantProductServiceImpl);

        Mockito.when(merchantHistoryService.save(Mockito.any(MerchantHistoryEntity.class))).thenReturn(true);

        MerchantAddProductRequestVO addProductRequestVO = new MerchantAddProductRequestVO();
        boolean added = merchantProductServiceMock.addNewProduct(addProductRequestVO);
        Assertions.assertFalse(added);

        Mockito.doReturn(1).when(productMapper).addNewProduct(Mockito.any(MerchantProductEntity.class));
        added = merchantProductServiceMock.addNewProduct(addProductRequestVO);
        Assertions.assertTrue(added);

        Mockito.when(merchantProductService.addNewProduct(Mockito.any(MerchantAddProductRequestVO.class))).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/product")).andExpect(status().isBadRequest());

        addProductRequestVO.setMerchantId(1);
        addProductRequestVO.setProductImage("test");
        addProductRequestVO.setProductDescription("Test");
        addProductRequestVO.setProductName("Test");
        addProductRequestVO.setProductPrice(new BigDecimal("3.14"));
        addProductRequestVO.setProductQuantity(3);
        addProductRequestVO.setProductSku("test");

        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(addProductRequestVO);

        Mockito.when(merchantProductService.addNewProduct(addProductRequestVO)).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/product").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(merchantProductService.addNewProduct(addProductRequestVO)).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/product").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testUpdateProduct() throws Exception {
        Mockito.doReturn(null).when(baseProductMapper).selectById(Mockito.anyInt());

        MerchantProductDAO merchantProductDAO = new MerchantProductDAO();
        ReflectionTestUtils.setField(merchantProductDAO, "productMapper",  productMapper);
        MerchantProductDAO merchantProductDAOMock = Mockito.spy(merchantProductDAO);
        MerchantProductServiceImpl merchantProductServiceImpl = new MerchantProductServiceImpl();

        ReflectionTestUtils.setField(merchantProductServiceImpl, "merchantProductDAO",  merchantProductDAOMock);
        ReflectionTestUtils.setField(merchantProductServiceImpl, "merchantHistoryService",  merchantHistoryService);
        MerchantProductService merchantProductServiceMock = Mockito.spy(merchantProductServiceImpl);
        Mockito.doReturn(baseProductMapper).when(merchantProductServiceMock).getBaseMapper();
        Mockito.doReturn(null).when(merchantProductServiceMock).getById(Mockito.anyInt());

        Mockito.when(merchantHistoryService.save(Mockito.any(MerchantHistoryEntity.class))).thenReturn(true);

        MerchantIncreaseProductQuantityRequestVO increaseProductRequestVO = new MerchantIncreaseProductQuantityRequestVO();
        boolean added = merchantProductServiceMock.increaseProductQuantity(increaseProductRequestVO, LocalDateTime.now());
        Assertions.assertFalse(added);

        MerchantProductEntity merchantProduct = new MerchantProductEntity();

        merchantProduct.setMerchantId(1);
        merchantProduct.setId(1);
        merchantProduct.setProductQuantity(1);
        increaseProductRequestVO.setCount(1);
        increaseProductRequestVO.setMerchantId(1);
        increaseProductRequestVO.setProductId(1);

        Mockito.doReturn(merchantProduct).when(baseProductMapper).selectById(Mockito.anyInt());
        Mockito.doReturn(merchantProduct).when(merchantProductServiceMock).getById(Mockito.anyInt());
        added = merchantProductServiceMock.increaseProductQuantity(increaseProductRequestVO, LocalDateTime.now());
        Assertions.assertFalse(added);

        increaseProductRequestVO.setCount(2);
        Mockito.doReturn(null).when(productMapper).updateProductQuantity(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(LocalDateTime.class));
        added = merchantProductServiceMock.increaseProductQuantity(increaseProductRequestVO, LocalDateTime.now());
        Assertions.assertFalse(added);

        Mockito.doReturn(1).when(productMapper).updateProductQuantity(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(LocalDateTime.class));
        added = merchantProductServiceMock.increaseProductQuantity(increaseProductRequestVO, LocalDateTime.now());
        Assertions.assertTrue(added);

        MerchantIncreaseProductQuantityRequestVO increaseProductQuantityRequestVO  = new MerchantIncreaseProductQuantityRequestVO();
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1")).andExpect(status().isBadRequest());

        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(increaseProductQuantityRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        increaseProductQuantityRequestVO.setMerchantId(1);
        increaseProductQuantityRequestVO.setProductId(2);
        increaseProductQuantityRequestVO.setCount(1);
        requestJson = objectMapper.writeValueAsString(increaseProductQuantityRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        increaseProductQuantityRequestVO.setProductId(1);
        requestJson = objectMapper.writeValueAsString(increaseProductQuantityRequestVO);
        Mockito.when(merchantProductService.increaseProductQuantity(Mockito.any(MerchantIncreaseProductQuantityRequestVO.class), Mockito.any(LocalDateTime.class))).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(merchantProductService.increaseProductQuantity(Mockito.any(MerchantIncreaseProductQuantityRequestVO.class), Mockito.any(LocalDateTime.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/product/1").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetMerchantSettlement() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/0")).andExpect(status().isBadRequest());

        Mockito.when(schedulerHistoryService.getSettlementSchedulerHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/1")).andExpect(status().isOk());

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("totalPages", 1);
        resultMap.put("currentPage", 1);
        resultMap.put("pageSize", 1);

        Mockito.when(schedulerHistoryService.getSettlementSchedulerHistory(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(resultMap);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/scheduler/settlement/1")).andExpect(status().isOk());
    }

    @Test
    public void testRunMerchantSettlementScheduler() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement/0")).andExpect(status().isBadRequest());

        Mockito.when(schedulerHistoryService.startSettlementScheduledJob(Mockito.anyInt(), Mockito.any(LocalDateTime.class))).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement/1")).andExpect(status().isOk());

        Mockito.when(schedulerHistoryService.startSettlementScheduledJob(Mockito.anyInt(), Mockito.any(LocalDateTime.class))).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement/1")).andExpect(status().isOk());
    }

    @Test
    public void testCreateMerchantSettlementScheduler() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement")).andExpect(status().isBadRequest());

        MerchantUpdateSettlementSchedulerRequestVO createSettlementSchedulerRequestVO = new MerchantUpdateSettlementSchedulerRequestVO();
        createSettlementSchedulerRequestVO.setMerchantId(0);

        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(createSettlementSchedulerRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        createSettlementSchedulerRequestVO.setMerchantId(1);
        createSettlementSchedulerRequestVO.setHourOfDay(1);
        requestJson = objectMapper.writeValueAsString(createSettlementSchedulerRequestVO);

        Mockito.when(schedulerHistoryService.createSettlementScheduler(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(schedulerHistoryService.createSettlementScheduler(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testUpdateMerchantSettlementScheduler() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement")).andExpect(status().isBadRequest());

        MerchantUpdateSettlementSchedulerRequestVO updateSettlementSchedulerRequestVO = new MerchantUpdateSettlementSchedulerRequestVO();
        updateSettlementSchedulerRequestVO.setMerchantId(0);

        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(updateSettlementSchedulerRequestVO);

        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

        updateSettlementSchedulerRequestVO.setMerchantId(1);
        updateSettlementSchedulerRequestVO.setHourOfDay(1);
        requestJson = objectMapper.writeValueAsString(updateSettlementSchedulerRequestVO);

        Mockito.when(schedulerHistoryService.updateSettlementScheduler(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        Mockito.when(schedulerHistoryService.updateSettlementScheduler(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders.patch("/scheduler/settlement").content(requestJson).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}